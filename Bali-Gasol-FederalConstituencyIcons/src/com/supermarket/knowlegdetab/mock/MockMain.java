package com.supermarket.knowlegdetab.mock;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.supermarket.knowledgetab.R;

public class MockMain  extends Activity implements OnClickListener{
	
	LinearLayout jamb,waec;
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.mockmain);

jamb = (LinearLayout)findViewById(R.id.jamb_linear);
waec = (LinearLayout)findViewById(R.id.waec_linear);

jamb.setOnClickListener(this);
waec.setOnClickListener(this);
}
@Override
public void onClick(View v) {

	switch (v.getId()) {
	case R.id.jamb_linear:
		Intent in = new Intent(MockMain.this,JambMain.class);
		startActivity(in);
		break;
	case R.id.waec_linear:
		Intent in1 = new Intent(MockMain.this,JambMain.class);
		startActivity(in1);
		break;
	default:
		break;
	}
}
}
