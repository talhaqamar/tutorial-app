	package com.supermarket.knowlegdetab.mock;
	
	import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.supermarket.knowledgetab.R;
	
	public class Questions1 extends Activity implements OnClickListener {
		TextView questionmain,questionno,answertextview,questiontypevalue;
		ImageView questionnext,questionend;
		RadioButton radio1,radio2,radio3,radio4,radio5;
		public static int check = 0;
		String biology_1_1[] = {
				"1.Which of the following body parts secretes a hormone which helps a person to react in dangerous situation?",
				"2. What happens to a seedling when an aluminium foil cap is placed on the top of its plumule and it is subjected to light from one side?",
				"3. The liver performs all of the following functions except the",
				"4. The part of the skin that helps in excretion is the",
				"5. The kidney helps to prevent the loss of useful substances from the body through"};
		
		String biology_1_1no[]= {"1 Of 5","2 Of 5","3 Of 5","4 Of 5","5 Of 5"};
		String biology_1_1_options[] = {"A.Pituary gland","B.Adrenal gland","C.Thyroid gland","D.Spleen","E.Pancrease"
				,"A. No further growth takes place.","B. The shoot bends over towards the light.","C. The root bends away from the light.","D. The internodes show a marked elongation","",
				
			"A. manufacture of urea.","B. manufacture of bile.","C. manufacture of proteins.","D. manufacture of adrenaline.","",
			"A. hair.","B. sweat gland.","C. sebaceous gland.","D. nerve ending.","",
			"A. pressure filtration.","B. active diffusion.","C. selective reabsorption.","D. active transportation",""	
		};
		int biology_1_1_answers[] = {2,2,4,2,1};
		/****************/
		
		/// biology_1_2
		
		String biology_1_2[] = { "1. The basic functional unit of the nervous system is the","2. All the following structures are parts of a neurone except the","3. The main function of myelin sheath in a neurone is to","4. The part of the mammalian brain that controls body posture is the","5. The kidney helps to prevent the loss of useful substances from the body through" };
		String biology_1_2no[]= {"1 Of 5","2 Of 5","3 Of 5","4 Of 5","5 Of 5"};
		String biology_1_2_options[] = {"A. dendrite.","B. dendron","C. neurone","D. nerve fibre","","A. soma","B. grey matter","C. axon","D. dendrite","","A. slow down the speed of transmission of impulses","B. retard the speed of transmission of impulses","C. accelerate the speed of transmission of impulses","D. depolarize the axon","","A. medulla oblongata","B. cerebellum","C. cerebrum","D. cerebral cortex","","A. walking","B. running","C. sneezing","D. reading",""};
		int biology_1_2_answers[] = {3,2,3,2,3};
		RadioGroup questionradioGroup=null;
		String subject= "";
		String chapter = "";
		
		// biology_1_3
		String biology_1_3[] = {"1. Which of the following structures is not a sense organ?","2. Which of the following statements is false?","3. The parts of the mammalian eye that strongly bend light rays are the","4. Which of the following statements about the optic nerve is correct?","5. Which of the following statements is false?"};
		String biology_1_3no[]= {"1 Of 5","2 Of 5","3 Of 5","4 Of 5","5 Of 5"};
		String biology_1_3_options[] = { "A. Mouth","B. Skin","C. Eye","D. Tongue","","A. The skin can perceive the sensations of touch, pain, pressure and temperature.","B. Sensory nerve endings receive stimuli and relay the messages to the brain","C. Taste buds and olfactory cells are active when wet as well as when dry.","D. Senses of smell and taste are weakened by cold, catarrh or blocked nose.","","A. cornea and the lens.","B. lens and aqueous humour.","C. cornea and aqueous humour","D. lens and vitreous humour.","","A. It relays images to the brain from the ear.","B. It is the third cranial nerve","C. It is the second nerve from the spinal cord.","D. It transmits impulses from the eye to the brain.","","A. Short-sightedness and long-sightedness are two common eye defects.","B. Convex lenses are used for the correction of long-sightedness.","C. The correct order of the ear ossicles from the ear drum is, malleus, incus and stapes.","D. The ear is an organ for balancing and hearing.",""};
		int biology_1_3_answers[] = {1,3,4,4,4};
		// biology_1_4
		String biology_1_4[] = {"1. Which of the following structures is absent in the reproductive system of a male mammal?","2. Sperm cells in mammals are stored in the","3. The female of all the following organisms have two ovaries except that of a","4. Which of the following structures are not usually found in an insect-pollinated flower?","5. Which of the following statements about the structures and functions of parts of a flower is false?"};
		String biology_1_4no[]= {"1 Of 5","2 Of 5","3 Of 5","4 Of 5","5 Of 5"};
		String biology_1_4_options[] = {"A. Testes.","B. Claspers.","C. Prostate gland.","D. Sperm ducts.","","A. epididymis.","B. testes","C. scrotum.","D. seminal vesicle.","","A. frog.","B. toad","C. bird","D. reptile","","A. Pollen grains.","B. Lodicules.","C. Stamens.","D. Pistils.","","A. The pedicel surrounds the ovary.","B. The calyx is often green in colour.","C. The corolla is the most prominent.","D. The stigmas receive pollen grains.","" };
		int biology_1_4_answers[] = {2,4,3,2,1};
		// biology_1_5
		String biology_1_5[] = {"1. Reproductive behaviours in animals include all the following except","2. Which of the following statements is not true?","3. All the following features ensure cross fertilization except","4. Which of the following statements is not true of self pollination?","5. Which of the following statements is not shown by wind-pollinated flowers?"};
		String biology_1_5no[]= {"1 Of 5","2 Of 5","3 Of 5","4 Of 5","5 Of 5"};		
		String biology_1_5_options[] = {"A. pairing","B. display","C. territoriality","D. seasonal migration","","A. Male crickets attract females by making shrill noises.","B. Male Tilapia often bites, struggles or fights for the possession of the female","C. There is only one type of pollination","D. The agents of pollination are wind, insects, water and some animals","","A. Cleistogamy in certain closed flowers.","B. Some plants bear only male or female flowers.","C. In some flowers, the styles are longer than the filaments making them coil back.","D. Filaments are longer than styles.","","A. No scent and nectar","B. Flowers are usually small and inconspicuous.","C. Flowers are borne on large inflorescence","D. Pollen grains are heavy, rough-edged and sticky.","" };		
		int biology_1_5_answers[] = {4,3,1,4,4};		
		// biology_1_6
		String biology_1_6[] = {"1. Which of the following animals shows internal fertilization?","2. Which of the following statements is incorrect?","3. A seed requires oxygen in order to","4. Which of the following describes the sequence of development of an insect with complete metamorphosis?","5. In the frog or toad, fertilization takes place"};
		String biology_1_6no[]= {"1 Of 5","2 Of 5","3 Of 5","4 Of 5","5 Of 5"};	
		String biology_1_6_options[] = { "A. Toad","B. Lizard","C. Fish","D. Hydra","","A. The testa is the integument which develops from the ovary wall.","B. The plumule is the leafy part of the embryonic shoot.","C. The hilum is the scar left by the stalk which attaches the ovule to the ovary wall.","D. The micropyle is the opening of the integument.","","A. release energy to geminate","B. synthesise food.","C. release food from the cotyledons.","D. break down the testa.","","A. Egg -> pupa  ->  larva  ->  imago","B. Egg  ->  larva  ->  pupa  ->  nymph  ->  imago","C. Egg  ->  larva  ->  nymph  ->  pupa  ->  imago","D. Egg  ->  larva  ->  pupa  ->  imago","","A. in the water where both eggs and sperm meet.","B. in the cloaca of the female.","C. a few days after both eggs and sperm are liberated in water.","D. internally or externally, depending on the species",""};
		int biology_1_6_answers[] = {4,1,1,4,1};
		// biology_1_7
				String biology_1_7[] = {"1. The maize grain is regarded not as a seed but a fruit because","2. The following statements are correct except","A. Seeds have two natural scars.B. A fruit consists of a pericarp and seeds.C. Most fruits have seeds.D. Fruits develop from fertilized ovaries","3. Which of the following statements is false?","4. Which of the following statements is true?","5. Which of the following features is not correct?" };
				String biology_1_7no[]= {"1 Of 5","2 Of 5","3 Of 5","4 Of 5","5 Of 5"};	
				String biology_1_7_options[] = {"A. it has the remains of a style.","B. of the relatively small scutellum.","C. it has a coleoptile.","D. it has a coleorhiza","","A. Multiple fruits are always false fruits.","B. Aggregate fruits are formed from free and close carpels.","C. True fruits are formed from either the whole inflorescence or other parts of the flower.","D. Drupes have hard and stony endocarp.","","A. Follicles split along one side.","B. Legumes split along one side.","C. Coconut is dry and indehiscent.","D. Capsules split into several, separate small units.","","A. Dispersal prevents overcrowding.","B. Dispersal encourages colonization of other areas.","C. Lightness in weight aids wind dispersal of fruits.","D. The water-proof nature of some fruits ensures wind dispersal.","" };
				int biology_1_7_answers[] = {1,1,3,1,4};
				// biology_1_8
				String biology_1_8[] = {"1. Which of these statements about variation is false?","2. Which of the following is not a continuous variation?","3. All the following are discontinuous variations except","4. Which of the following is not one of the applications of the knowledge of variation?","5. Which of the following is a discontinuous variation in plants?"	};
				String biology_1_8no[]= {"1 Of 5","2 Of 5","3 Of 5","4 Of 5","5 Of 5"};	
				String biology_1_8_options[] = {"A. It is the difference found among members of the same species.","B. Variations among members of the same species are usually great.","C. Variations can be inherited.","D. Variations can be continuous or discontinuous.","","A. Height","B. Weight","C. Skin colour","D. Ability to smell","","A. tongue rolling","B. left-handedness","C. ability to taste","D. eye colour","", "A. Determination of paternity.","B. Crime detection.","C. Blood transfusion.","D. Determination of genotype","","A. Height","B. Shape of body parts","C. Root size","D. Size of petals",""};
				int biology_1_8_answers[] = {2,4,4,4,4};		
		// biology_1_9
		String biology_1_9[] = {"1. Which of the following people proposed the theory of acquired characteristics?","2. Which of the following lines of enquiry is not generally used to provide evidence to support the scientific theory of evolution?","3. The possession of thick and succulent stems by plants growing in dry conditions is an adaptation for","4. Which of the following is a social insect?","5. The chemical contained in a dog�s urine which enables other dogs to show that a territory has been occupied is called"	 };			
		String biology_1_9no[]= {"1 Of 5","2 Of 5","3 Of 5","4 Of 5","5 Of 5"};	
		String biology_1_9_options[] = {"A. De Vries.","B. Lamarck.","C. Darwin","D. Leuwenhoek.","","A. Analysis of fossil records.","B. Analysis of mutation","C. Comparative embryology.","D. Comparative anatomy.","","A. the reduction of photosynthesis.","B. water conservation.","C. an increase in the transpiratory surface.","D. exposing the axillary buds to more light.","","A. Butterfly.","B. Housefly","C. Mosquito","D. Honey bee","","A. thyroxin.","B. pheromone.","C. adrenalin.","D. auxin.",""};			
		int biology_1_9_answers[] = {2,2,2,4,2};				
		// biology_1_10
		String biology_1_10[] = {"1. Which of the following statements is not true?", "2. Who of the following, gave us the knowledge that hereditary material is the gene and not blood or any body fluid?","3. An individual is homozygous for a trait when the genes","4. If a tall woman (TT) is married to a short man (tt) and they have four male children, the offspring will be","5. Which of the following statements is false?"};			
		String biology_1_10no[]= {"1 Of 5","2 Of 5","3 Of 5","4 Of 5","5 Of 5"};	
		String biology_1_10_options[] = {"A. Genetics is the study of heredity and variation.","B. Hereditary variations refer to the influence of environments on each organism.","C. Blood groups, sickle-cell anaemia, and flower colour, are examples of hereditary variations.","D. Hereditary variation may be continuous or discontinuous.","","A. Morgan.","B. Crick.","C. Hugo de Vries.","D. Mendel.","","A. controlling his traits are identical.","B. are inherited from the father only.","C. are inherited from the mother only.","D. are inherited from both parents","","A. three short and one tall","B. two short and two tall.","C. all short","D. one short and three tall","","A. Mendel gave two laws of inheritance","B. Nuclear division in gametes produces four haploid cells.","C. DNA consists of four nitrogenous bases, deoxyribose sugars and phosphoric acid molecules in a double helix shape.","D. Genes have specific locations on chromosomes and they can replicate themselves.",""};		
		int biology_1_10_answers[] = {2,4,1,4,4};		
						
		//////////////////////////////////////////////////////////////////
		//	Commerce
		///////////////////////////////////////////////
		String commerce_1_1[] = {"1.  Commerce means","2.  e-Commerce includes the use of the following except","3. The exchange of goods in home trade is known as","4. Which of the following ancillary services makes it possible for goods to be where they are required","5. e-Commerce means"};String commerce_1_1no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String commerce_1_1_options[] = {"A. buying and selling of  good and services.","B.exchange of goods and service","C.   buying and selling","D.  production, distribution and exchange of goods and services","","A. internet","B. website","C. facebook","D. manual typewriter","","A. entreport  trade","B. trade by barter","C. commodity trade","D.  distributive trade","","A. communication","B.transportation","C.  banking","D. insurance","","A. electric - commerce","B. electrical-commerce","C. electronic-commerce","D.  elective-commerce",""};int commerce_1_1_answers[] = {4,4,2,2,3};			
		String commerce_1_2[] = {"1.   Exchange of goods for goods and services for services is","2.  The growth of commerce in Nigeria was affected by which of the following?","3. Trading in Nigeria got a boost with contact with European in","4.  Articles of trade exchanged by Nigerians with the Europeans include the following except","5. Early men were engaged in _______ production"};String commerce_1_2no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String commerce_1_2_options[] = {"A.  trade","B.barter","C.  aids to trade","D. commerce","","A. insufficient capital","B. employment generation","C.  indirect services","D. direct services","","A. 14th century","B. 17th century","C.19th century","D.  15th century","","A. palm-oil","B.pepper","C. elephants teeth","D. Tobacco","","A. peasant","B. industrial","C. mechanical","D.  elective-commerce",""};int commerce_1_2_answers[] = {3,2,4,4,2};				
		String commerce_1_3[] = {"1.   What one does for most of his productive time to earn a living is","2.   Industrial occupation includes the following, except","3. Climatic difference, natural resources, salary and wages, Government  polices are factors","4.  Teachers, Doctors, Engineers working in public are in","5. Teachers, Doctors, Engineers working in private establishments are in"};String commerce_1_3no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String commerce_1_3_options[] = {"A. occupation","B.contract","C.  job","D. trade","","A. extractive","B. services","C.  manufacturing","D. constructive","","A.of production","B.determining types of occupation","C.classifying occupation","D.  of commerce","","A. commercial occupation","B.direct service occupation","C. industrial occupation","D. indirect service occupation","","A.commercial occupation","B. direct service occupation","C.industrial occupation","D.   indirect service occupation",""};int commerce_1_3_answers[] = {2,3,3,4,3};				
		String commerce_1_4[] = {"1.  Making goods and services available to the people is","2.  The following are factors of production except","3.  ____________ does not belong to the group","4.  Permanent engagement in one type of occupation is known as","5.  _____________ is the basis for exchange"};String commerce_1_4no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String commerce_1_4_options[] = {"A. trade","B.production","C.  specialization","D. exchange","","A. warehousing","B. enterprise","C.  labour","D. land","","A. banking","B.insurance","C.occupation","D.  advertising","","A. exchange","B.barter","C. enterprise","D. specialization","","A. cash","B. labour","C.money","D.  barter",""};int commerce_1_4_answers[] = {3,2,4,4,4};
		String commerce_1_5[] = {"1.  The use of coin operated machines to sell goods is a form of","2.  Many stores under one roof are called","3. Quotation is usually a reply to:","4.  If a seller discovers that he had under charged a buyer, which document will he use to correct the error?","5.  The main function of the wholesaler to the manufacturer is"};String commerce_1_5no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String commerce_1_5_options[] = {"A. Personal selling","B.Retailing","C.  Wholesaling","D. Mail order selling","","A. Multiple stores","B. Departmental stores","C.  Supermarkets","D. Co-operative store","","A. Letter of inquiry","B.Bank statement","C.I owe you","D.  Consular invoice","","A. Promissory note","B.Credit note","C. Advise note","D. Debit note","","A.packaging","B. branding","C.lending","D.  warehousing",""};int commerce_1_5_answers[] = {3,3,2,4,4};
		String commerce_1_6[] = {"1.  The use of coin operated machines to sell goods is a form of","2.  Many stores under one roof are called","3. Quotation is usually a reply to:","4.  If a seller discovers that he had under charged a buyer, which document will he use to correct the error?","5.  The main function of the wholesaler to the manufacturer is"};String commerce_1_6no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String commerce_1_6_options[] = {"A. Personal selling","B.Retailing","C.  Wholesaling","D. Mail order selling","","A. Multiple stores","B. Departmental stores","C.  Supermarkets","D. Co-operative store","","A. Letter of inquiry","B.Bank statement","C.I owe you","D.  Consular invoice","","A. Promissory note","B.Credit note","C. Advise note","D. Debit note","","A.packaging","B. branding","C.lending","D.  warehousing",""};int commerce_1_6_answers[] = {3,3,2,4,4};
		String commerce_1_7[] = {"1. The use of coin operated machines to sell goods is a form of","2.  Many stores under one roof are called","3. Quotation is usually a reply to:","4.  If a seller discovers that he had under charged a buyer, which document will he use to correct the error?","5.  The main function of the wholesaler to the manufacturer is"};String commerce_1_7no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String commerce_1_7_options[] = {"A. Personal selling","B.Retailing","C.  Wholesaling","D. Mail order selling","","A. Multiple stores","B. Departmental stores","C.  Supermarkets","D. Co-operative store","","A. Letter of inquiry","B.Bank statement","C.I owe you","D.  Consular invoice","","A. Promissory note","B.Credit note","C. Advise note","D. Debit note","","A.packaging","B. branding","C.lending","D.  warehousing",""};int commerce_1_7_answers[] = {3,3,2,4,4};
		String commerce_1_8[] = {"1. The rate at which the country's import exchanges for its export is known as;","2.  in foreign trade, document of title is known as the","3. Entreport trade is said to have taken place when goods are","4.  A trade document signed by a representative of the country to which goods are being sent is referred to","5.  A trade deficit means that"};String commerce_1_8no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String commerce_1_8_options[] = {"A. terms of trade.","B.balance of trade","C.  Rate of exchange","D. terms of exchange","","A. Bill of lading","B. Bill of sight.","C.  Certificate of origin.","D. Declaration form.","","A. Held at the port pending payments of customs duty.","B.sold in the ship under special regulation","C.Imported and then re-exported","D.   kept for further processing before sales.","","A. Export invoice","B.Consignment","C. Consular invoice","D. pro-forms invoice.","","A.the balance of trade is favorable.","B. exports are less than imports","C.imports are less than exports","D.   The balance of payments is favorable",""};int commerce_1_8_answers[] = {3,2,4,4,3};
		String commerce_1_9[] = {"1. The most common form of business unit in West Africa is","2. Sole enterprise may flourish best in","3.  The main advantage of a sole trader is the freedom to","4. Sources of finance to a business include personal savings, shares, debentures and","5.  Sole proprietorship business is owned by"};String commerce_1_9no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String commerce_1_9_options[] = {"A. co-operative society","B.joint stock company","C.  partnership","D. sole proprietorship","","A. mining business","B. a retailing business","C.  an oil exploration business.","D. a car assembly business","","A.employ anyone he likes","B.seek advice from any source","C.take quick decision","D.   plough all the profits back into the business","","A. loans from IMF","B.Bank overdrafts","C. Central bank loans","D. Money from political parties","","A.One man","B. two men","C.three men","D.   many people",""};int commerce_1_9_answers[] = {4,3,4,3,2};
		String commerce_1_10[] = {"1. The partnership agreement contains the","2.  A partnership cannot raise its capital by","3. Partnership are most suitable where","4.  Where a limited partner exists in a partnership","5.  A partnership formed for banking business is made up of"};String commerce_1_10no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String commerce_1_10_options[] = {"A. duration of the partnership","B. goodwill and credibility of partners","C. relationship among the partners and customers","D. size of the partnership","","A. selling shares to the public","B. personal contribution of partners","C.  admission of new partners","D. undistributed profits","","A. The partners are family friends","B.members can easily raise enough capital","C.government regulations are favorable","D.","","A.no active partner is needed","B.other partners must be nominal","C. at least one ordinary partner must be there","D.at least one member must be a legal practitioner","","A.2-10 members","B. 2-20 members","C.2-40 members","D.   2-50 members",""};int commerce_1_10_answers[] = {2,2,3,4,2};	
		String commerce_1_11[] = {"1. Portability as a quality of money implies that it must be","2.   an exchange economy where goods are directly exchanged for goods is called:","3. Which of the following is not a function of money?","4.  Which of the following is not a legal tender?","5.  Which of the following is not a means of making payment to a businessman?"};String commerce_1_11no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String commerce_1_11_options[] = {"A.divisible into various denominations","B. easily carried about","C.  easily identified","D. generally accepted by all","","A. import trade","B.export trade","C.  home trade","D. undistributed profits","","A. Provision of double coincidence of wants*C*","B.Durability","C. standard for deferred payment.","D. Portability","","A. Bank notes","B.Bank drafts","C. Biro","D.Traveler's cheque.","","A.Bank draft","B. Cash","C.Cheque","D.   Statements",""};int commerce_1_11_answers[] = {3,4,2,4,4};
		String commerce_1_12[] = {"1. A cheque made payable to bearer.","2. When a customer writes a cheque to withdraw cash from his current account","3. A bank may dishonour a cheque if:","4. Which of the following is not a function of development bank","5.  The authority given to a bank to make regular payments on behalf of a customer for a specific purpose known as"};String commerce_1_12no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String commerce_1_12_options[] = {"A. Can be negotiable for cash by the drawer only.","B.Can only be paid into an account","C.  Can be negotiated for cash by any holder*C*","D. Is not regarded as a negotiable instrument","","A.  he is both the","B. Drawer and the payee","C.  Drawee and the payee","D. Drawer and the debtor","","A. it is not crossed","B.an alteration on the cheque is initiated","C.the amount on the cheque is less than the amount in the account","D.   The amount on the cheque is more than the amount in the account","","A. Give long- term loan","B. Invest directly in agriculture","C. Issue currency notes","D. Give technical advice to investors","","A.Bank draft","B. Credit transfer","C.Standing order","D.   Certified cheque",""};int commerce_1_12_answers[] = {4,3,4,4,4};
		String commerce_1_13[] = {"1.  A cheque made payable to bearer.","2.   When a customer writes a cheque to withdraw cash from his current account, he is both the","3. A bank may dishonour a cheque if:","4.  Which of the following is not a function of development bank","5. The authority given to a bank to make regular payments on behalf of a customer for a specific purpose known as"};String commerce_1_13no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String commerce_1_13_options[] = {"A. Can be negotiable for cash by the drawer only.","B.Can only be paid into an account","C. Can be negotiated for cash by any holder","D. Is not regarded as a negotiable instrument","","A. Drawer and the payee","B. Drawee and the payee","C.  Drawer and the debtor","D. Drawer and creditor","","A. it is not crossed","B.an alteration on the cheque is initiated","C.the amount on the cheque is less than the amount in the account","D.    The amount on the cheque is more than the amount in the account","","A. Give long- term loan","B. Invest directly in agriculture","C. Issue currency notes","D. Give technical advice to investors","","A.Bank draft","B. Credit transfer","C.Standing order","D.   Certified cheque",""};int commerce_1_13_answers[] = {4,2,4,4,4};
		String commerce_1_14[] = {"1.  All the following are means of water transportation except.","2.  Which one of these is not a means of land transportation","3.  All the following are the advantages of air transportation except","4.  All the following are the advantages of road transportation except","5. All the following are means of transportation except"};String commerce_1_14no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String commerce_1_14_options[] = {"A. Canoes","B.Ships","C. Footbridge","D. Launch","","A. Donkey","B.Okada","C.  Rail","D. Steamer","","A. It carries passengers to only designated airports.","B. It is fastest means of transportation.","C.It is most suitable means for carrying fragile and perishable goods","D.    it is suitable for carrying mail and expensive goods","","A. It is very convenient to travel by road","B. passengers pay cheaper fares.","C. It encounters traffic congestions other form of transportation.","D. An individual can own his own means of road transportation","","A.Road transportation","B. Water transportation","C.Net transportation","D.   Air transportation",""};int commerce_1_14_answers[] = {4,4,2,4,4};
		String commerce_1_15[] = {};String commerce_1_15no[] = {};String commerce_1_15_options[] = {};int commerce_1_15_answers[] = {};
		String commerce_1_16[] = {"1.   A bank s guarantee to honour the exporter s invoice when presented for payment is called","2.     Which is the safest of all documentary credits?","3.    A bonded warehouse is used to store goods which are","4. Which of the following provides harbours, berth and navigational aids to ships?","5.     Which of the following is not performed by the Airports Authority?","6.  The basic function of the Nigerian Ports Authority is to","7.   One of the functions of seaports is the provision of","8.   A clean bill of lading is a bill that","9.  A bill of lading is said to be foul if","10.    Which of the following services is not provided by the Customs and Excise Authority?"};String commerce_1_16no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String commerce_1_16_options[] = {"A.    irrevocable letter of credit","B.  documentary credit","C.    unconfirmed credit","D.    letter of hypothecation","","A.   Revocable letter of credit","B.   Unconfirmed credit","C.   Irrevocable letter of credit","D.    Confirmed irrevocable letter of credit","","A.   manufactured in a country","B.   to be exported","C.awaiting payment of duties","D. seized as contrabands","","A. Export Promotion Council","B.   Shipping, Clearing and Forwarding Agents","C. Ports Authority","D.    Customs and Excise Authority","","A.  Providing loading facilities","B.  Issuing travelling documents","C.  Providing warehouses for cargo","D.   Providing security for passengers","","A.   coordinate the activities of all the seaports in the country","B. facilitate and control the movement of goods and services into and out of the country","C.  ensure that the right caliber of personnel is employed at the ports","D.    coordinate and regulate the activities of shipping lines","","A.    loading and offloading facilities","B.   control room for relaying radio messages","C.   warehouses for storage of cargo","D.   berthing and landing facilities","","A.  is clean and without stain","B. shows ownership of goods","C. is issued by customs to show that the importer has paid all charges","D.  contains no damaged goods","","A.  it has to do with shipment of chickens","B.   the goods are in a damaged state","C.  the goods are rejected by the shipping company","D.   the goods are impounded by the Customs Authority","","A.  Collection of import duties","B. Improvement of ports facilities","C.  Checking of smuggling","D. Control of goods in bonded warehouse or retirement",""};int commerce_1_16_answers[] = {1,4,3,3,2,4,2,3,2,2};
		String commerce_1_17[] = {"1.   Which of the following is a feature of sole proprietorship?","2.        Furniture and fittings used in a business are classified as","3.  Capital plus profit minus drawings is equal to","4.   Which of the following is a source of capital to a sole proprietor?","5.     Capital Owned  by a sole proprietor is the difference between","6. Malam Soho started a business with a capital of N20,000 and spent Nl5,000 of it to buy machines and equipment. The remaining N5,000 is his","7.     Which of the following is an internal source of capital to a business concern?","8.     In what service will a sole trader NOT be found?","9.One of these is NOT an item of current assets"};String commerce_1_17no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5"};String commerce_1_17_options[] = {"A.  Business name must be registered","B.   Liability of the owner is limited","C.    It is a legal entity","D.  Decisions are promptly made","","A. fixed asset","B.   circulating capital","C.  current asset","D.  working capital","","A.   fixed capital","B.  liquid capital","C.   capital owned","D.  capital employed","","A.  shares","B.  savings","C. debentures","D.   bonds","","A.   sundry creditors and sundry debtors","B. current assets and current liabilitiess","C.   total assets and current liabilities","D.   fixed and current assets","","A.  capital owned","B.  nominal capital","C.  working capital","D.   fixed capital","","A. Trade credit","B. Debenture","C.   Overdraft","D.  Retained profit","","A. business consultant","B. electrician","C. decorator","D.  national electric power authority","","A.  Creditors","B. Debtors","C.  Pre-payment","D.    Stock of good",""};int commerce_1_17_answers[] = {2,1,3,2,3,3,4,4,1};
		String commerce_1_18[] = {"1.    An agreement between two or more persons which is intended to be enforceable by law is termed:","2.      An existing partnership is dissolved if it","3.   A partnership business will not raise capital through","4.  Which of the following does not lead to the dissolution of a deed of partnership?","5.    Which body of these people has NOT been granted an exception to have as many partners as are necessary by the company s Act of 1967?","6. In partnership business the maximum number of person for its formation is","7.    A partnership formed for banking business is made up of","8.   Which of the following statements is true of limited partnership?","9.  Which of the following is not required for the dissolution of a partnership?","10.   One of these points may not necessarily be carried in partnership deeds"};String commerce_1_18no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String commerce_1_18_options[] = {"A.   Agency","B.  Contract","C.    Hire-purchase","D.    Partnership","","A.   is registered under a trade name","B.    has been registered for over ten years","C.  has no limited partner","D.   admits a new partner","","A.   contributions","B.  credit purchases","C.  debentures","D.    retained earnings","","A.  Absence of a deed of partnership","B.  Withdrawal of a partner","C.Incapacity of any of the partners","D.    Completion of contractual term","","A.   Accountants","B.  Bankers","C.   Brokers","D.   Jobbers","","A.   five","B. fifteen","C. ten","D.   twenty","","A.    2 10 members","B.   2 20 members","C.   2 30 members","D.    2 40 members","","A. Every member is a limited partner","B.   All members are general partners","C.  All members have limited liability","D.   There is at least one ordinary partner","","A.  Order from the Registrar of Companies","B.  Mutual consent of the partners","C.   One partner giving intention to dissolve","D.     Termination of venture","","A.  Capital contribution per partner","B.  Partners  salaries (if any)","C. Name of successor in case of a partner s death","D. Ascertaining partner s business share at death or retirement",""};int commerce_1_18_answers[] = {4,4,3,1,2,4,1,4,2,3};
		String commerce_1_19[] = {};String commerce_1_19no[] = {};String commerce_1_19_options[] = {};int commerce_1_19_answers[] = {};
		String commerce_1_20[] = {"1.   When a customer writes a cheque in his own name and withdraws cash with it from his account, he is both the","2.     Which of the following services is not rendered by Commercial Banks?","3.  Which of the following is a creditor legally bound to accept for settlement of a debt?","4. If a customer is allowed 1,000 overdraft and he received a bank statement showing an overdraft 100, this means that he","5.    Which of the following cheques does not require endorsement?","6.  In foreign or international trade, which of the following services is rendered by the banks?","7.  A cheque which a bank draws on itself is known as a","8.  A cheque which is payable to anyone who presents it across the counter is known as","9.  In opening a current account the following are required except","10. Building societies and mortgage banks encourage people to save and borrow later principally"};String commerce_1_20no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String commerce_1_20_options[] = {"A.  drawee and payee","B. drawer and payer","C.  drawer and drawee","D.  drawer and payee","","A. Business advice","B.Currency notes issue","C.  Accepting deposits","D.   Credit transfers","","A. Bank draft","B.  Bank note","C. Money orders","D.  Cheque","","A.   cannot draw more cheques","B.  is owed 100 by the bank","C. owes the bank at least 900","D.   owes the bank 100 only","","A.   Specially crossed cheque","B.  Open cheque","C. Bearer cheque","D. Generally crossed cheque","","A.  Provision of treasury bills","B.    Collection of customas duties","C. Facilitation of cash on delivery","D.  Provision of documentary credits","","A. banker's order","B.   bank teller","C.   crossed cheque","D.   bank draft","","A.   Crossed cheque","B. Bearer cheque","C.  Order cheque","D.  Specially crossed cheque","","A.   a refree","B. initial deposit","C. specimen signature","D.   a cheque book","","A. to build their own factories","B.   to build their own houses","C.  to start agricultural production","D.  to develop co-operative ventures",""};int commerce_1_20_answers[] = {4,2,2,3,3,4,4,2,4,2};
		String commerce_1_21[] = {"1.   When a customer writes a cheque in his own name and withdraws cash with it from his account, he is both the","2.     Which of the following services is not rendered by Commercial Banks?","3.  Which of the following is a creditor legally bound to accept for settlement of a debt?","4. If a customer is allowed 1,000 overdraft and he received a bank statement showing an overdraft 100, this means that he","5.    Which of the following cheques does not require endorsement?","6.  In foreign or international trade, which of the following services is rendered by the banks?","7.  A cheque which a bank draws on itself is known as a","8.  A cheque which is payable to anyone who presents it across the counter is known as","9.  In opening a current account the following are required except","10. Building societies and mortgage banks encourage people to save and borrow later principally"};String commerce_1_21no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String commerce_1_21_options[] = {"A.  drawee and payee","B. drawer and payer","C.  drawer and drawee","D.  drawer and payee","","A. Business advice","B.Currency notes issue","C.  Accepting deposits","D.   Credit transfers","","A. Bank draft","B.  Bank note","C. Money orders","D.  Cheque","","A.   cannot draw more cheques","B.  is owed 100 by the bank","C. owes the bank at least 900","D.   owes the bank 100 only","","A.   Specially crossed cheque","B.  Open cheque","C. Bearer cheque","D. Generally crossed cheque","","A.  Provision of treasury bills","B.    Collection of customas duties","C. Facilitation of cash on delivery","D.  Provision of documentary credits","","A. banker's order","B.   bank teller","C.   crossed cheque","D.   bank draft","","A.   Crossed cheque","B. Bearer cheque","C.  Order cheque","D.  Specially crossed cheque","","A.   a refree","B. initial deposit","C. specimen signature","D.   a cheque book","","A. to build their own factories","B.   to build their own houses","C.  to start agricultural production","D.  to develop co-operative ventures",""};int commerce_1_21_answers[] = {4,2,2,3,3,4,4,2,4,2};
		String commerce_1_22[] = {"1.   Promissory notes, bank drafts and cheques are classified asss","2.    Which of the following grants loan to members without stringent collateral security?","3.     Government raises funds usually through the sale of","4.     An example of a secured loan is","5.  A collective name for money market and capital market is","6.   The means through which the government borrows from the public on short term is"};String commerce_1_22no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5"};String commerce_1_22_options[] = {"A.   bank depositss","B.  documents of title","C.  documents of title","D. credit instruments","","A.   Commercial banks","B.  Mortgage banks","C.   Saving banks","D.  Thrift societies","","A. bill of exchange","B.  debentures","C. premium certificates","D. treasury bills","","A.   trade credit","B.   mortgage","C. co-operative loan","D.   overdrafty","","A.    financial market","B.   financial institutions","C.  insurance market","D.   foreign exchange market","","A.  treasury bill","B. exchange rate","C.  bill of exchange","D.  bank draft",""};int commerce_1_22_answers[] = {4,4,3,2,2,1};
		String commerce_1_23[] = {"1. The following are functions of customs authority Except","2.    Which of the following is not performed by the airport authority?","3.   All of these function except one are performed by the Nigerian Airports Authority.","4.   These are the advantages of air transport","5.  One of these is odd in the group of advantages of using the road over the railway","6.  Factors affecting the railway rates","7.  Consignment note is known as","8.  One of these is not necessarily on the consignment note","9.The following are advantages of walking as a means of transport except"};String commerce_1_23no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5"};String commerce_1_23_options[] = {"A.   rail","B.    trailers","C.   luxury buses","D.  ferry","","A.  providing loading facilities","B. issuing travelling documents","C.    providing warehouses for cargo","D. providing security for passengers","","A.  developing and maintaining airports in the country","B.  responsibility for flight delays","C. providing information bureau in each of the airport","D. keeping the airport free of touts.","","A.  expensive cost of travel","B. saves time","C.  loss boring","D.  handy for emergencies","","A.   more readily available","B.  boarding procedure more informal","C. penetrates more into inferiors","D.   ownership more wide spread","","A.   weight of goods","B.   value of goods","C.   status of sender","D.  length of journey","","A.consignor","B.  consignee","C.   receipt given to a consignor","D.   counter form","","A.  description and weight of goods","B. name of consignee","C. at whose risk is the transportation","D.   passport photograph of the consignor","","A. unsuitable for long journeys","B. very handy to rural settlers","C. not badly affected by transport jams","D. quicker for short journeys",""};int commerce_1_23_answers[] = {1,2,2,1,4,3,3,4,1};
		String commerce_1_24[] = {"1.     The capacity to carry bulky goods and passengers is a distinctive advantage of transport by:","2.     Which of the following is not performed by the airport authority?","3.     All of these function except one are performed by the Nigerian Airports Authority.","4.     These are the advantages of air transport","5.     One of these is odd in the group of advantages of using the road over the railway","6.     Factors affecting the railway rates","7.     Consignment note is known as","8.     One of these is not necessarily on the consignment note","9.     The following are advantages of walking as a means of transport except"};String commerce_1_24no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5"};String commerce_1_24_options[] = {"A.     rail","B.     trailers","C.     luxury buses","D.     ferry","","A.     providing loading facilities","B.     issuing travelling documents","C.     providing warehouses for cargo","D.     providing security for passengers","","A.     developing and maintaining airports in the country","B.     responsibility for flight delays","C.     providing information bureau in each of the airport","D.     keeping the airport free of touts.","","A.     expensive cost of travel","B.     saves time","C.     loss boring","D.     handy for emergencies","","A.     more readily available","B.     boarding procedure more informal","C.     penetrates more into inferiors","D.     ownership more wide spread","","A.     weight of goods","B.     value of goods","C.     status of sender","D.     length of journey","","A.     consignor","B.     consignee","C.     receipt given to a consignor","D.     counter form","","A.     description and weight of goods","B.     name of consignee","C.     at whose risk is the transportation","D.     passport photograph of the consignor","","A.     unsuitable for long journeys","B.     very handy to rural settlers","C.     not badly affected by transport jams","D.     quicker for short journeys",""};int commerce_1_24_answers[] = {1,2,2,1,4,3,3,4,1};
		
		////////////////////////////////////////////////////////////
		// CRS
		/////////////////////////////////////////////////////////////
		String crs_1_1[] = {"There Are NO QUESTIONS Loaded Here"};String crs_1_1no[] = {"1 of 5"};String crs_1_1_options[] = {"Empty Answer","Empty Answer","Empty Answer","Empty Answer",""};int crs_1_1_answers[] = {4};
		
		
		//////////////////////////////////////////////////////////
		// Economics
		/////////////////////////////////////////////////////////
		String economics_1_1[] = {"1. Which of the following is an economic activity.","2. The reward for land as a factor of production is","3. The three broad categories of production are","4. A carpenter s consumer goods include; I.A kilogram of rice II.3 pairs of shoes, III.4 pairs of socks IV.3 screw-drivers","5. In a capitalist system, the means of production is owned and controlled by","6. Mixed economy refers to a system in which the means of production are controlled by","7. An economy good is described as a good which","8. The concept of opportunity cost is important to the firm because it","9. As a firm expands, it enjoys some advantages called","10. Division of labour is limited by","11. An entrepreneur is likely to make more profits when quantity of output reduces","12. Natural growth rate is"};String economics_1_1no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5","11 of 5","12 of 5"};String economics_1_1_options[] = {"A. Attending a town's meeting","B.  A visit to the stadium","C. Payment of school fees","D.  Arresting a petty thief","","A. interest","B. rent","C. dividend","D. wages","","A. direct, secondary and extractive","B. primary, tertiary and direct","C. primary, secondary and tertiary","D. secondary, primary and indirect","","A. 1 and 4 only","B.  I, II, III and IV","C.  I, II and IV only","D.  I, II and III only","","A. the government","B. politicians","C.  private individuals","D.  the workers","","A.  private enterprise and the government","B.  private individuals only","C.  the government only","D. the workers and businessmen","","A. yields utility and commands a price","B.  is useful and occupies a space","C.  is in high demand and transferable","D.   has unlimited supply","","A. determines the prices of the firm s products","B.  increases the level of output of the firm","C.  leads to maximum satisfaction of the consumers","D.  guides firms in allocating scarce resources","","A. variable proportion","B.  diminishing marginal returns","C.  internal economies of scale","D.  decreasing returns to scale","","A. the size of the market","B.  cost of production","C.  the productivity of capital","D.  the factors of production","","A. expenditure is more than revenue","B. competitors charge lower","C.  cost per unit output falls","D. quantity of output reduces","","A.  birth rate less death rate","B.  death rate less migration","C.  death rate plus birth rate","D.  birth rate plus migration",""};int economics_1_1_answers[] = {3,2,3,1,3,1,1,4,3,1,3,1};
		String economics_1_2[] = {"1.  An economic system where all resources are allocated through the price mechanism is a","2.  Scarcity implies that","3. An arrangement of the individual s unsatisfied desires in their order of importance is called","4. The problem of how goods shall be produced in a society implies choice between","5. An example of a primary producer is the"};String economics_1_2no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String economics_1_2_options[] = {"A.  socialist economy","B.   command economy","C. market economy","D.  mixed economy","","A.  a commodity is not available","B. the price of a commodity is low","C. demand exceeds supply.","D. supply exceeds demand","","A. Scale of preference","B.  money cost","C. opportunity cost","D.  demand schedule","","A.  labour and capital intensive method","B.   capital and consumer goods","C.   civilian and military goods","D.   necessities and luxuries","","A. manufacturer","B. school teacher","C.  Fisherman","D. wholesaler",""};int economics_1_2_answers[] = {3,3,1,1,3};
		String economics_1_3[] = {"1. A major effect of ageing population is","2. One of the problems associated with the middleman in the distribution of goods is that he","3. The production of goods is said to be completed when it reaches the","4. The gap between demand and supply curves below the equilibrium price indicates","5. Demand for inferior goods is an example of","6. When a change in price does not affect the quantity demanded of a commodity, the price elasticity of demand is"};String economics_1_3no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5"};String economics_1_3_options[] = {"A. reduced labour force","B.  rise in prices of commodities","C.  reduced infrastructure","D.  neglect of agriculture.","","A.  buys in large quantities","B.  hoards goods","C.  grants credit to the retailer","D.  sells varieties of goods.","","A. wholesalers","B. consumers","C. retailers","D. manufacturers","","A. excess demand","B.   excess supply","C.  equilibrium quantity","D.   equilibrium price","","A. expansion of demand","B.  contraction of demand","C.   individual demand","D.  abnormal demand","","A.  fairly inelastic","B.  infinitely elastic","C.  perfectly inelastic","D.  unitary elastic",""};int economics_1_3_answers[] = {1,2,2,1,4,3};
		String economics_1_4[] = {"1. The Economic Community of West African States (ECOWAS) has been slow in achieving its objectives because of","2. A major achievement of the Organization of Petroleum Exporting Countries (OPEC) is that","3. The elasticity of supply is","4. Which of the following is excluded when estimating national income?","5. For both the monopolist and the perfectly competitive firm, profit maximizing output occurs at the point where the"};String economics_1_4no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String economics_1_4_options[] = {"A. political instability in member states","B.  The activities of multinationals","C. Inadequate personnel at the secretariat","D.   Inadequate international support","","A. crude oil price has increased tremendously","B.  petroleum products prices have remained low","C. producing countries are highly industrialized","D.  price of crude oil has remained relatively stable","","A. infinite","B.  equal to one","C. less than one","D.  equal to zero","","A. dividends","B.  wages and salaries","C.  transfer payments","D. Profits","","A.  marginal cost curve cuts the marginal revenue curve from below","B.  marginal revenue curve cuts the marginal output from above","C.   marginal cost curve intersects the X-axis","D.   marginal revenue curve intersects the Y-axis",""};int economics_1_4_answers[] = {1,4,2,3,1};
		String economics_1_5[] = {"1. One main benefit of partnerships is","2. Joint ventures are partnerships involving","3. The value of money is best determined by","4.  wage earners will gain","5. Government can control inflation by"};String economics_1_5no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String economics_1_5_options[] = {"A. the possibility of raising funds on the stock exchange","B.  the possibility of attracting twenty one or more members","C.  that members can specialize in various functions","D.   that it enjoys its own separate legal entity","","A.  the poor and the rich","B.  employers and workers","C. government and private investors","D.  multinationals and individuals","","A.  input-output ratio","B.  the purchasing power","C.  the important people attach to it","D. economies of scale","","A.  wage earners will gain","B.  borrowing of money will be restricted","C.  money lenders will gain","D. borrowers of money will gain","","A.  printing more money","B. reducing the rate of taxes","C.  reducing the level of expenditure","D.   establishing more banks",""};int economics_1_5_answers[] = {3,3,2,4,3};
		String economics_1_6[] = {"1. The market consisting of a network of dealers where currencies can be bought and sold is known as","2. Open Market Operation (OMO) means the","3. Which of the following is an advantage of localization of industry?","4. Which of the following will ensure efficiency in the industrial sector of your country?","5. The numbers of people who are qualified to work and who offer themselves for employment are called"};String economics_1_6no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String economics_1_6_options[] = {"A.   capital market","B.  foreign exchange market","C.  commodity market","D.  the stock market","","A. provision of credit facilities by commercial banks","B.  provision of credit facilities by the mortgage banks","C. buying and selling of government securities by the central bank","D.   procedure for the establishment of commercial banks","","A.  development of subsidiary firms","B.  creation of parallel markets","C. development of slums","D.  attraction of foreign capital","","A. indigenization","B. privatization","C. nationalization","D. Liquidation","","A.  migrant labour","B.   working population","C.   labour turnover","D.   mobility of labour",""};int economics_1_6_answers[] = {2,3,1,2,2};
		String economics_1_7[] = {"1. When the general price level persistently falls, the rate of unemployment","2. Which of the following is not a direct effort to increase agricultural production?","3. To ensure high employment rates, developing countries should","4. The act of selling goods in foreign markets at prices below those charged at home markets is called","5. Taxes levied on commodities are"};String economics_1_7no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String economics_1_7_options[] = {"A. rises","B. stagnates","C.   rapidly reduces","D.   equals natural growth rate","","A.   operation feed the nation","B.  use of improved seedlings","C.  increased loans to farmers and co-operative societies","D.  National Youth Service Corps","","A. build more universities.","B.  protect infant industries","C. organize trade fairs","D.   prevent rural-urban drift","","A. exchange","B. specialization","C. dumping","D. exporting","","A. direct taxes","B.  indirect taxes","C.  poll taxes","D.  investment taxes",""};int economics_1_7_answers[] = {1,4,2,3,2};
		String economics_1_8[] = {"1. One disadvantage of direct taxes is that","2. International trade is an application of the principle of","3. Trade among West African countries is poor because the","4. A policy by which governments restrict the amount of foreign currencies bought and sold is known as","5. Which of the following items is under the capital account of a balance of payments?"};String economics_1_8no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String economics_1_8_options[] = {"A.  government s revenue is reduced","B.  price of essential commodities fall","C.   people are discouraged from additional work","D.  firms make more profits","","A.  industrial production","B. mass production","C. regional co-operation","D. comparative cost advantage","","A.  countries are self-sufficient","B.  communication links are weak","C.  number of banks is insufficient","D. people are not enterprising","","A. devaluation","B.  credit creation","C. exchange control","D.  export promotion","","A. repayments of foreign loans","B.  visible imports","C.  invisible exports","D.  cocoa exports",""};int economics_1_8_answers[] = {3,4,2,3,1};
		String economics_1_9[] = {"1. A surplus in the balance of payments should be use to","2. Development planning which takes an overall view of the economy is described as","3. The national income of a country can be estimated by the","4. Which of the following is a major foreign exchange earner for West African countries?"};String economics_1_9no[] = {"1 of 5","2 of 5","3 of 5","4 of 5"};String economics_1_9_options[] = {"A. subsidize multinational companies","B. build infrastructure for friendly nations","C.  make donation to developed countries","D. buy investments overseas","","A. aggregate economic planning","B.   disaggregated economic planning","C.  sectorial economic planning","D.  system economic planning","","A.  cost-benefit method","B. distribution method","C.  expenditure method","D.   consumption method","","A.   tea","B. cassava","C.  cocoa","D. rice",""};int economics_1_9_answers[] = {4,1,3,3};
		String economics_1_10[] = {"1. The most frequently occurring number in a set of data is referred to as the","2. Land refers to all forms of natural resources that can attract the payment of","3. A major function of the entrepreneur is to","4.  Which of the following production costs of a carpenter is fixed in the short run?","5. Total cost can be defined as"};String economics_1_10no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String economics_1_10_options[] = {"A.  mode","B.  median","C.  mean","D. range","","A. wages","B.  royalties","C.  interest","D.  profit","","A. finance public projects","B.  donate to the needy.","C.  bear risks","D.  discourage investors","","A. Cost of planks","B.  Expenditure on nails","C. Overtime payment to casual workers","D.  Rent paid yearly on his premises","","A. Fixed cost plus variable cost","B.  total variable cost divided by total output","C.  total output divided by variable cost","D. fixed cost plus marginal cost",""};int economics_1_10_answers[] = {1,2,3,4,1};
		String economics_1_11[] = {"1. Utility refers to the","2. Demand schedule is","3. Which of the following is true of a normal demand curve?","4. When the demand for a commodity is inelastic,total revenue will fall if","5. Given the supply function Q= 2p+1, the quantity supply function, Q, at the price of N4 is"};String economics_1_11no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String economics_1_11_options[] = {"A. importation of capital goods and raw materials","B. production of goods and services for export","C.  satisfaction derived from consuming goods and services","D. money spent on buying goods and services","","A.   Purchases at different prices","B. the nature of a demand curve","C.  elasticity of demand","D. the demand for composite goods.","","A. Slopes upwards, from left to right","B.   Slope downward, from left to right","C.  Parallel to X - axis","D.   Parallel to Y - axis","","A.  Price is increased","B.  price is reduced","C.  price remains constant","D.  cost of production is reduced.","","A. 7 units","B. 8 units","C. 9 units","D. 10 units",""};int economics_1_11_answers[] = {3,1,2,2,3};
		String economics_1_12[] = {"1. The major reason for changes in quantity supplied is","2. Elasticity of supply can be expressed as","3. If the government takes over a business organization from its original owners, the business is said to have been","4. A partnership is usually governed by a written agreement called","5. Membership of a private limited company consist of between two and"};String economics_1_12no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String economics_1_12_options[] = {"A.  government policy","B.   weather","C.  taxation","D.   price","","A. A percentage change in supply   percentage change in price","B.   original supply   original price","C.  change in supply   change in price","D. percentage change in supply   original supply","","A.  privatized","B.  liquidated","C.  indigenized","D. nationalized","","A. certificate of partnership","B.  deed of partnership","C.  document of partnership","D.   partnership trust","","A. ten","B.  twenty","C. fifty","D.  infinity",""};int economics_1_12_answers[] = {4,1,4,2,3};
		String economics_1_13[] = {"1. Wholesalers assist producers by","2. Emigration rate of a country increases as a result of","3. Changes in age distribution of a given population are primarily cause by","4. Which of the following is a problem of population census in West Africa?","5. Labour efficiency can be improved by all the following except"};String economics_1_13no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String economics_1_13_options[] = {"A. preventing retailers from purchasing goods on credit","B.  informing manufacturers about retailers' complaints","C. granting retailers warehousing facilities","D.  providing after sales service to consumers","","A. Inadequate employment and health facilities","B. explosion in educational facilities","C.  improvement in living standards","D.  improvement in pre and post-natal care","","A.  government population policies","B.  birth and death rates.","C.  poverty and famine","D. technological development","","A.    High cost","B. Availability of electricity","C.   Poor waterways","D. Unemployment","","A. Prompt payment of wages","B.  Technological improvement","C.  Job security","D.   Increase in population",""};int economics_1_13_answers[] = {2,1,1,1,4};
		String economics_1_14[] = {"1. Which of the following is not a reason for wage differential among occupations?","2. A market characterized by only one seller but many buyers is known as","3.  Which of the following is a feature of a perfect market?","4. Which of the following can lead to increase in the general price level?","5. To deflate an economy, government should"};String economics_1_14no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String economics_1_14_options[] = {"A.  Length and cost of training","B.  Supply of labour","C. Influence of trade unions","D.  Age of the worker","","A.   a perfect market","B.  a monopoly market","C.  an oligopoly market","D.  a duopoly market","","A.   Factors of production are immobile","B.  Entry and exit are restricted","C.  There is a large number of buyers and sellers","D.  There is no perfect knowledge in the market.","","A.  Expenditure persistently exceeding taxation","B. Reduction in the external reserves","C.  Increase in the level of production","D.  Increased investments from abroad","","A.  reduce bank lending","B.  increase expenditure","C.   print more money","D.  license more banks",""};int economics_1_14_answers[] = {4,2,3,1,1};
		String economics_1_15[] = {"1. Which of the following financial institution can not provide direct loans to individual?","2. An economy in which both roadside shoe manufacturer and the government owned shoe factory co-exist, is","3. The economic resource of capitalist countries are controlled by","4. One basic feature of every state-owned enterprise, is that, it is","5. An important source of revenue for government of West Africa countries is"};String economics_1_15no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String economics_1_15_options[] = {"A. pThe Agricultural bank","B.  The Industrial bank","C.  The Central bank","D.   The Co - operative Bank","","A.  socialist","B. capitalist","C. barter","D.  Mixed","","A. the state","B. private individuals","C.  the workers","D. foreigners","","A.  established by a decree or an act of parliament","B.   registered by the Registrar General of companies","C.    run by a number of private shareholders","D.   run with funds freely subscribed by its employees","","A. sale of lottery tickets","B. donations from friendly government","C. donations from wealthy citizens","D. Royalties from minerals",""};int economics_1_15_answers[] = {3,4,2,1,4};
		String economics_1_16[] = {"1. Which of the following items can not be classified as essential government expenditure?","2. A tax system in which all the payees are taxed the same percentage of their incomes is referred to as","3. Which of the following will lead to a higher national income per head?","4. Countries impose custom duties to protect","5. The economist associated with the law of comparative advantage is"};String economics_1_16no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String economics_1_16_options[] = {"A.  Construction of roads","B. Servicing of external debts","C.  Maintenance of public hospital","D.  Importation of luxury consumer goods","","A. regressive.","B.  progressive","C. proportional","D.   flexible","","A. reduction in total population","B. fall in house hold expenditure","C. balance of payment deficit","D. rise in the prices of essential commodities","","A. multinational companies","B.  public corporations","C.  Indigenous enterprises","D.  military installations","","A.   David Ricardo","B.  Adam Smith","C.    Alfred Marshall","D.  Lionel Robbins",""};int economics_1_16_answers[] = {4,3,1,3,1};
		String economics_1_17[] = {"1. A Major objective of international financial institution is to","2. Trade among West African countries is poor because","3. A major negative effect of the exploitation and  export of crude oil on Nigeria s economy is the","4. To enhance rapid economic development West African country should","5. A major aim of the European Economic Community (EEC) is the"};String economics_1_17no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String economics_1_17_options[] = {"A.  Aid countries facing severe economic problems","B. control the spending habits of nations","C. enforce the principle of comparative advantage","D.  build industries in developing nations","","A.   price are  generally high in the region","B. the countries produce similar commodities","C. transport cost is quite prohibitive","D. storage facilities are virtually non-existent","","A. Growing unemployment of school leavers","B.  Neglect of other sources of revenue","C. Collapse of infrastructural facilities","D. Rural - urban drift","","A. continue to export their raw materials","B.  encourage foreigners to exploit their mineral resources","C.   develop technology to transform their raw materials","D.   promote the assemblage of knocked down parts","","A. promotion of a strong defence force.","B. establishment of free trade among member countries","C.   development of English speaking West African countries.","D.  establishment of industries in major cities of the world.",""};int economics_1_17_answers[] = {1,3,2,3,2};
		String economics_1_18[] = {"1. A major reason for the establishment of the Economic Community of West African States (ECOWAS) was to","2. The international bank for reconstruction and development (IBRD) is also known as the"};String economics_1_18no[] = {"1 of 5","2 of 5"};String economics_1_18_options[] = {"A. compete with other international agencies","B.  develop the domestic trade of member countries","C.   control the outflow of foreign exchange in the region","D.   promote the economic progress of member nations","","A.   World Bank","B. Bank for International Bank","C.   African Development Bank","D. European Economic commission.",""};int economics_1_18_answers[] = {4,1};
		String economics_1_19[] = {"1. Which of the following is an economic activity.","2. The reward for land as a factor of production is","3. The three broad categories of production are","4. A carpenter s consumer goods include; I.A kilogram of rice II.3 pairs of shoes, III.4 pairs of socks IV.3 screw-drivers","5. In a capitalist system, the means of production is owned and controlled by"};String economics_1_19no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String economics_1_19_options[] = {"A. Attending a town's meeting","B.  A visit to the stadium","C. Payment of school fees","D.  Arresting a petty thief","","A. interest","B. rent","C. dividend","D. wages","","A. direct, secondary and extractive","B. primary, tertiary and direct","C. primary, secondary and tertiary","D. secondary, primary and indirect","","A. 1 and 4 only","B.  I, II, III and IV","C.  I, II and IV only","D.  I, II and III only","","A. the government","B. politicians","C.  private individuals","D.  the workers",""};int economics_1_19_answers[] = {3,2,3,1,3};
		
		//////////////////////////////////////////
		// English language
		/////////////////////////////////
		String english_language_1_1[] = {};String english_language_1_1no[] = {};String english_language_1_1_options[] = {};int english_language_1_1_answers[] = {};
		
		//////////////////////////////////////////
		// Geography
		///////////////////////////////////////
		String geography_1_1[] = {"1. Geography can be defined as the","2. Which of the following is not a physical feature?","3. An example of human (man-made) feature is","4. Which of the following is not studied by geographers?","5. Human activities include all except"};String geography_1_1no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String geography_1_1_options[] = {"A. description of the earth.","B. study of plants and animals.","C. study of places.","D. study of physical features.","","A.  Hills","B. Vegetation","C. Soil","D.  Animal","","A. roads.","B.  rivers.","C.  seas.","D. vegetation.","","A. Man-environment interaction","B. Distribution of physical features","C. Distribution of cultural features","D. Mars and gravity","","A. mining.","B. farming.","C. marketing.","D. None of the above.",""};int geography_1_1_answers[] = {1,5,1,4,4};
		String geography_1_2[] = {"1. The study of our locality is called","2. Which of these is not a cultural feature found in our locality?","3. Which of these functions is not performed by both a village and a town?","4.  A village can be differentiated from a town in terms of","5. The direction of a feature relative to other features can be determined by","6. Which of these is not a characteristic of a village?","7. Which of these is not a function of a village?","8. Which of these can be found in a village?","9. Which of these functions is performed by both village and town?","10. Which of these do not influence agricultural activities?"};String geography_1_2no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String geography_1_2_options[] = {"A. Physical Geography.","B. Human Geography.","C. Map Reading.","D. Local Geography.","","A.  Mountain","B.  School","C. Markets","D.  Hospital","","A.  Educational","B.  Residential","C.  Marketing","D. Administrative.","","A.  population size.","B. agricultural activities.","C. presence of facilities.","D. climate.","","A. measurement with a tape.","B. cardinal points.","C. using a ruler.","D. looking up in the sky","","A. Homogenous population","B. Mostly primary activities are carried out","C.Heterogeneous population","D. Small area size","","A.  Residual function","B. Marketing function","C.Religion function","D. Administrative function","","A.  University","B. Supermarket","C.Research Institution","D.  Market","","A.   Residential function","B. Administrative function","C. Research","D. Industrial function","","A.  Fertile soil","B.  Settlement","C. Topography","D. Rainfall",""};int geography_1_2_answers[] = {4,1,4,1,2,3,4,4,1,2};
		String geography_1_3[] = {"1. Which of the following statements is not true about the solar system?","2. Which of the following is not a proof that the earth is spherical?","3. Which of these planets has the largest size?","4.  The earth is unique because it","5. Which of the planets has the highest number of satellites?","6. Which of these is a dwarf planet?","7. The ecliptic is defined by the plane of the ____________ orbit.","8. Satellites are also called ____________","9. The closest planet to the sun is called __________","10. The planet with the largest number of satellites is called ________"};String geography_1_3no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String geography_1_3_options[] = {"A.  The rays of the sun give energy to the system","B. All the planets rotate around the sun.","C. All the planets have their orbits round the sun.","D.  The planets revolve around the sun.","","A.  The earth  s horizon as seen from a ship","B.  Planetary observed as observed from a telescope","C.  The earth  s shadow on the moon during an eclipse","D.  The four seasons","","A. Earth","B. Neptune","C. Jupiter","D. Mars","","A.  has a large circumference and diameter.","B. contains large amount of gases.","C. contains gases and water which can support life.","D. contains plants and animals.","","A. Mercury","B. Venus","C. Saturn","D.  Jupiter","","A.  Neptune","B. Pluto","C.Earth","D. Saturn","","A.   Earth  s","B. Pluto  s","C. Jupiter  s","D.  Uranus  s","","A.   Moons","B. Planets","C.Comet","D. Meteors","","A.   Mars","B. Earth","C. Neptune","D. Mercury","","A.  Jupiter","B.  Mercury","C. Saturn","D.  Jupiter",""};int geography_1_3_answers[] = {2,4,3,3,4,2,1,1,4,4};
		String geography_1_4[] = {"1. The rotation of the Earth on its axis is","2.  Each revolution of the Earth around the sun takes","3. Which of the following phenomena is due entirely to the Earth rotating on its axis?","4.  The revolution of the Earth around the sun gives rise to","5.  The tilt of the Earth axis from the vertical is","6. On June 21st the sun is overhead at","7.  The overhead sun is to be seen at the Tropic of Capricorn on","8. In the Northern hemisphere, the summer solstice occurs in","9. Twilight is the period between","10. Mid-winters day in the northern hemisphere is"};String geography_1_4no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String geography_1_4_options[] = {"A.  faster in winter.","B. faster in summer.","C. from west to east.","D. from east to west.","","A. 24 hours.","B. 365 years.","C.  one leap year.","D.  365 days.","","A.The rising and setting of the sun","B. The different angles of the sun rays throughout the year","C. The seasonal differences and changes","D. The eclipse of the sun","","A.  different seasons.","B. day and night.","C. the earth tilt on its axis.","D.  half of the earth being in darkness.","","A. 23.","B. 66.","C. 90.","D.   156.","","A. the Equator.","B. the Tropic of Capricorn.","C.the North Pole.","D. the Tropic of Cancer.","","A.   March 21st","B.  July 19th","C. August 17th","D.  December 22nd","","A. June","B. July","C. September","D. December","","A.  sunset and sunrise.","B. the setting of the sun and the rising of the moon.","C. the rising of the sun and daybreak.","D. daylight and midnight.","","A.  December 21st.","B.  March 21st.","C. September 21st.","D.  June 21st.",""};int geography_1_4_answers[] = {3,4,1,1,1,4,4,1,4,1};
		String geography_1_5[] = {"1. Which of the following is the best definition of latitudes?","2. The longest latitude is","3. The length of 10 of latitude on the earth surface is","4.  The shortest latitude is","5. The latitude of the Tropic of Cancer is","6. 660N is to the Arctic circle as 660S is to","7. The name given to the 00 line of longitude is","8.  What is the time at 750W if it is midnight at 1200W?","9. When the time is 10 pm at 900E, what time is it as 150E?","10. By crossing the International Date Line from East to West, the traveller"};String geography_1_5no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String geography_1_5_options[] = {"A.  Distance from the equator","B.  Angular distance north or south of equator","C.  A line on a map","D.   Lines to show places","","A.  any great circle.","B.   the Equator.","C.  the North Pole.","D.   the Tropic of Cancer.","","A.  32km","B.  76km","C. 98.5km","D. about 11km","","A.   the North or South Polediameter.","B.  Lat 700N","C. the Equator","D.  Arctic Circle","","A.  900N","B. 550N","C. 450N","D.  230N","","A.  the Equator","B. the Tropic of Cancer","C. the South Pole","D.  the Antarctic Circle","","A.   the Equator.","B.  the Great Circle.","C. the International Date Line.","D.  Greenwich Meridian.","","A.   9 pm","B. 3 pm","C.9 am","D.  3 am","","A. 5 pm","B. Midday","C. 2 am","D. 3 am","","A.  gains a day.","B.  loses a day.","C. loses 2hours.","D.   gains 1hour.",""};int geography_1_5_answers[] = {2,2,4,1,4,4,4,1,1,2};
		String geography_1_6[] = {"1. The Earth is made of _____________ structures.","2.  The inner structure of the Earth is made up of all but one of the following:","3. Which of the following outer structure is not physical in its characteristics?","4.  Which of the following gases is the least abundant in total volume within the atmosphere?","5.  Which of the following is not a source of fresh water in the hydrosphere?","6. The biosphere is important to life as a source of primary support for each of the following except","7.  Which of the following is not a constituent material of the biosphere?","8. The portion of the Earth that contains life is the","9.  The earth is enveloped by a mixture of gases referred to as the","10. Another name for the core of the Earth is"};String geography_1_6no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String geography_1_6_options[] = {"A.  1","B. 2","C. 3","D. 4","","A. Core.","B. Mantle.","C. Crust.","D.  Stratosphere.","","A.Biosphere","B. Hydrosphere","C. Atmosphere","D.  Lithosphere","","A.   carbon(IV) oxide","B.  oxygen","C. Nitrogen","D.  Argon","","A. Glacier","B.  Rain","C. Seas","D. Groundwater","","A.  water.","B.  food.","C. shelter.","D. energy.","","A.   Man","B.  plants","C. Animals","D.   Rocks","","A. atmosphere.","B.  hydrosphere.","C.  lithosphere.","D.  biosphere.","","A.   biosphere.","B.  lithosphere.","C.  mesosphere.","D. atmosphere.","","A.   lithosphere.","B.   biosphere.","C. barysphere.","D.   mesosphere.",""};int geography_1_6_answers[] = {2,4,1,4,3,1,4,4,4,3};
		String geography_1_7[] = {"1. Which of the following most accurately describe the layers of rock close to the Earth surface?","2.   Rocks on or near the surface are classified into 3 groups according to","3. Which one of the following rocks is igneous?","4.  One of the most common metamorphic rock is","5.  Which two of these rocks is classified as sedimentary?I. Granite II. Sandstone III. Limestone IV. Marble V. Basalt","6. Graphite is a metamorphic rock which has been produced form","7.  Plutonic rocks are igneous rocks that have hardened","8.  Metamorphic rocks are formed by","9.  Which of the following pairs do not go together?","10. If metamorphic rocks are the result of heat or pressure which of the following statements is unlikely to be true?"};String geography_1_7no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String geography_1_7_options[] = {"A. Rock layer","B. hydrosphere","C. Earth crust","D. Atmosphere","","A.  their age alone.","B.  their colour.","C.  their mode of origin.","D. their hardness.","","A.clay","B.  gneiss","C. loess","D.   basalt","","A.    lava.","B. slate.","C.  sandstone.","D. chalk.","","A.  I and IV","B.   II and V","C. IV and III","D. II and III","","A.   limestone.","B.  coral.","C. coal.","D. sand.","","A.  beneath the Earths crust.","B. on the Earths surface.","C.  inside volcanoes.","D.  in the Earths crust.","","A. the decay of plants","B. the accumulation of skeletons","C.  wave action","D.  pressure or heat","","A.  volcanic obsidian","B.  metamorphic marble","C.  plutonic granite","D. sedimentary gneiss","","A.   They often form the axes of great mountain chains","B.   They are associated with the deparition of muds and silts in tropical areas","C. They frequently occur on the periphery of volcanic areas","D.  They are generally crystalline but the crystals are arranged in layers.",""};int geography_1_7_answers[] = {3,3,4,2,4,3,1,4,4,2};
		String geography_1_8[] = {"1.  Which of the following is a Volcanic plateau?","2.   Depositional Plains refer to all the following except","3. When a plateau is enclosed by Fold Mountains it is called","4.  Mountains formed as a result of compressional forces in the earth crust are","5.  Which of the following is responsible for the formation of block mountains?","6. Residual mountains occur when","7.  A large expanse of low, level land, formed due to an uplift of part of the sea floor bordering a continent is called","8. Which of the following is not an example of a fold mountain?","9.  The plains formed when the slopes of mountains are worn back in arid and semi-arid regions by mechanical weathering","10. Which of the following is not a type of mountain?"};String geography_1_8no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String geography_1_8_options[] = {"A. kukuruku","B. Terra Rosa","C. Deccan","D. Graben","","A. alluvial plains","B. structural plains","C. deltaic plains","D. flood plains","","A. a volcanic plateau","B. a tectonic plateau","C. an intermont plateau","D.  a dissected plateau","","A.  block Mountains","B.  fold mountains","C. volcanic Mountains","D. dissected mountains","","A.  Faulting","B.  Sedimentation","C. Folding","D. Deposition","","A.    there are faults on the earths crust.","B.   the earths crust bends due to compression.","C. the general level of land is lowered by agents of denudation.","D.  materials are ejected from the interior of the earth and deposited on the earth surface. (WAEC.2000)","","A.   a flood plain","B. an outwash plain","C.  a deltaic plain","D. a high plain","","A. Andes","B.  Kilimanjaro","C. Rockies","D.   Himalayas","","A.  Pedi plains","B.  alluvial plains","C.  deflation plains","D. Outwash plains (WAEC, 2004.)","","A.  Conical mountain","B.  Block mountain","C. Fold mountain","D.  Mountain of accumulation. (WAEC, 2009)",""};int geography_1_8_answers[] = {3,2,3,2,1,3,5,2,1,1};
		String geography_1_9[] = {"1.  Which of the following is not a type of environment?","2.   The following are the domains of the environment, except","3. Rivers, streams, ponds, lakes are components of the","4.  The atmosphere is the portion of the environment","5.   ......................... environment refers to the immediate physical and social setting in which people live and develops.","6. The biological active part of the earth where plants, animals and micro organisms are found is called___________","7.  The liquid part of the environment is referred to as the __________","8.  ................. is the outermost layer of the earth crust that forms the landmass.","9.  The political, economic, social and moral aspects of human life constitute the environment.","10. Physical environment is known as environment."};String geography_1_9no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String geography_1_9_options[] = {"A. Physical","B. Economic","C.  Social","D. Cultural.","","A.  atmosphere","B. hydrosphere","C.  genosphere","D.  lithosphere","","A. hydrosphere","B. atmosphere","C. biosphere","D. lithosphere","","A.   liquid","B.  solid","C.  biological","D. gaseous.","","A. Physical","B.   Cultural","C. Social","D. Local","","A.  Biology.","B.   Ecology.","C. Biosphere.","D. Cultural.","","A.   Hydrosphere.","B. Lithosphere.","C.  Genosphere.","D. Atmosphere.","","A. Lithosphere","B. Social","C. Atmosphere","D. Biosphere.","","A.   cultural","B.  physical","C. social","D. biological","","A.  cultural","B.   social","C. natural","D.  chemical",""};int geography_1_9_answers[] = {2,2,1,4,2,3,1,1,3,3};
		String geography_1_10[] = {"1.  Which of the following pairs is not correct in the measurement of weather elements?","2.   Air pressure at sea levels is higher than at the top of mountain because the air at sea level","3. Diurnal range of temperature is the","4.   A rain guage is kept in an open place in the observatory because","5.   A fog is most likely to develop when a","6.  Humidity is the amount of","7.  Convectional rains are most common in","8. Which of the following is not a form of precipitation?","9.  Visibility usually decreases when a","10. The energy received from the sun at a particular place is called"};String geography_1_10no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String geography_1_10_options[] = {"A.  Rainfall with rain gauge","B.Humidity with hydrometer","C.  Pressure with anemometer","D.Temperature with thermometer","","A.  Originates from the sea","B. has a greater weight","C.  consists of a lesser mixture of gases","D. has a lesser weight","","A. Average of the highest and the longest month temperature","B. Difference between the maximum and the minimum temperature for a year.","C. Average of the minimum and the maximum daily temperature.","D.  Differences between the maximum and the minimum temperature for a month.","","A.  It is much more easily accessible to the observer","B.  Rain drops get into the funnel without any obstruction","C.  rain falls more heavily in the open space","D.  rain drops are deflected by the wind in an open place (e) the funnel is easily filled with rain water (1990).","","A. Cold dry wind blows over a cold current","B. Warm moist wind blows over a cold current","C.Cold dry wind blows over a warm current","D.Warm moist wind blows over a warm current","","A. Cloud cover in the sky","B. Water vapour in the atmosphere","C. Moisture needed to cause condensation","D. Moist in the atmosphere","","A. Equatorial regions and tropical monsoon climates","B.  Polar region and Mediterranean climates","C. Temperature region","D. Temperate and Mediterranean regions (2007)","","A. Hail","B.  Dust","C. Dew","D. Snow (2010)","","A.   Cold, dry wind blows over a cold current","B. Body of fresh water mingles with salty water","C.Warm wind blows over a warm current","D. Warm, dry wind rises over a high mountain","","A.  Insolation","B.  Convection","C.  Conduction","D.  heat wave",""};int geography_1_10_answers[] = {3,2,5,2,1,2,1,2,3,1};
		String geography_1_11[] = {"1. Which of the following factors least affects the distribution of vegetation?","2. The longitudinal extent of Nigeria is","3. Which of the following soil types is reddish in colour due to the presence of iron?","4. Which soil type supports the cultivation of groundnut?","5. Rivers State is located in which vegetation belt?","6.The distance from North to South in Nigeria is","7.  Nigeria has _______ states","8.  Which of these is not a highland region in Nigeria?","9. Which of these is an inland drainage river?","10. Which of these rivers is the longest in Nigeria?"};String geography_1_11no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String geography_1_11_options[] = {"A.  Man","B.   Rainfall","C. Wind","D. Soil","","A. 80","B.    100","C.   120","D.  150.","","A.  Northern zone of sandy soils.","B. Interior zone of lateritic soils.","C. Zone of alluvial soil.","D.  Southern zone of forest soils","","A.  Northern zone of sandy soils.","B.  Interior zone of lateritic soils.","C.  Zone of alluvial soil.","D. Southern zone of forest soil","","A. Sudan savanna","B. Sahel savanna","C.  Rain forest","D. Mangrove swamp forest","","A.  1100km","B.  1300 km","C. 1500 km","D. 1900 km","","A.  29","B. 36","C. 19","D. 21","","A.  Jos Plateau","B.  Idanre hills","C. Lake Chad","D. Adamawa highlands","","A. Kaduna","B.  Imo","C.  Yobe","D. Osun","","A. Benue","B. Kaduna","C.  Niger","D. Cross River",""};int geography_1_11_answers[] = {1,4,2,1,4,1,2,3,3,3};
		String geography_1_12[] = {"1.  The UNs estimate of the worlds population by the year 2012 is","2.  Which of the following is a measure aimed at controlling world population growth?","3. Which of the following may not solve the problem of over population?","4.   The only factor that least explains the pattern of population distribution in Nigeria is the: (WASSCE JUNE 1998)","5. Which of the following best explains the drift of population from rural areas to the cities in Nigeria? (SSCE JUNE 1994)","6. Which of the following least influences population density in Nigeria?","7.  The most populous country in Africa is","8. A state with high population density in Nigeria is","9. As at 2006, the population of Nigeria was about","10. The number of persons per square kilometer of land is"};String geography_1_12no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String geography_1_12_options[] = {"A. 2,500,000,000","B. 3,000,000,000","C. 4,200, 000,000","D. 7,000,000,000","","A.   family planning","B. decrease in food production","C. migration","D.  Encouraging early marriage","","A.  increase in food production","B. birth control","C.  increase in the area extent of towns","D.  reduction of agricultural land","","A.  Arab slave raiding","B.  stability of Government of the Old Kingdoms","C.  Physical factor of rainfall","D. Movement of people from one place to another","","A. The cost of living in the cities is very high","B. There are more job opportunities in the cities.","C. There is food scarcity in the rural areas.","D. Better recreational facilities are available in the cities.","","A.  Mining","B. Fertile agricultural land","C.  thick forest vegetation","D.  tsetsefly infestation","","A. Egypt","B. Nigeria","C. Zimbabwe","D. Ghana","","A. Zamfara","B. Akwa Ibom","C. Kwara","D. Lagos","","A.   120m","B.  130m","C. 140m","D. 150m","","A.  population","B.  population census","C. population density","D. Land area",""};int geography_1_12_answers[] = {4,1,4,5,2,4,2,4,3,3};
		String geography_1_13[] = {"1.  All the following minerals are found in Nigeria except","2.  What is closely associated with petroleum?","3. Petroleum is a sedimentary rock mineral found in one of the following states.","4.   The largest of all vegetation belts in Nigeria is","5. Montane vegetation is found in highland area like","6. The Shiroro H.E.P. station is located on the river","7. River Niger takes its sources from","8. Which of the following rivers in Nigeria provides irrigation, water and hydro-electricity?","9. All the following are ways in which rivers are useful to man except the","10. Vegetation resources can be utilized by man for all the following except"};String geography_1_13no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String geography_1_13_options[] = {"A. coal","B.  bauxite","C. tin ore","D. columbite (WAEC 1990)","","A.    Tine","B. Coal","C. Natural gas","D.   Limestone","","A.  Borno state","B. Rivers state","C.  Kano state","D. Osun state","","A. rainforest","B. guinea savanna","C. Sudan savanna","D. Sahel savanna","","A.  Port Harcourt","B.  Lagos","C. Lokoja","D. Jos","","A.   Gongola","B. Shiroro","C.  Niger","D.  Kaduna (WAEC June 1992)","","A.  guinea highlands","B.  north central highlands","C. Enugu scarp","D. Adamawa highlands","","A.  Sokoto","B. Kainji","C. Cross River","D. Ogun","","A.   formation of political boundaries between countries","B. provision of rich agricultural land","C. generation of hydro-electricity","D.  relocation of settlement after flooding","","A. food","B.  habitat","C.  timber","D. medicine (June 1999)",""};int geography_1_13_answers[] = {2,3,2,2,4,4,1,2,4,2};
		String geography_1_14[] = {"1.  Transport can be defined as the involvement of","2.  Which of the following is the oldest mode of transport?","3. The major advantages of road transport over other modes is its","4.    The use of water transport is limited by all the following except","5. Which mode of transport is most suitable for international trade?","6.  The term gauge in railway transport refers to the","7. Which of these modes is most suitable to the movement of petroleum?","8.The standard railway guage is","9. Which of these countries was the densent road network?","10. One relative advantage of water transportation over road transport is its"};String geography_1_14no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String geography_1_14_options[] = {"A. people and goods","B.  industries","C. buildings","D. markets","","A.  head porterage","B. railways","C.  air transport","D.  pipelines","","A. low cost","B. speed","C.  ability to carry bulky goods","D.  flexibility","","A. wide valleys","B. waterfalls","C. floating vegetation","D. narrow gauges","","A.  Road","B. Ocean transport","C.Air transport","D. Railways","","A.  weight of vehicle that the rails can bear","B. amount of rainfall experienced","C. distance between the two rails in a frack","D. length of the rails in a country","","A. Railways","B.  Pipelines","C. Ship","D. Tankers","","A.  134cm","B. 143cm","C.  150cm","D. 153cm","","A.    Nigeria","B.  Nigeria","C. United State of America","D.  Brazil","","A. capacity for passenger traffic only","B. ability to link all parts of any country","C.  suitability for bulky and heavy cargo","D. great speed over long distances.",""};int geography_1_14_answers[] = {1,1,4,4,2,3,2,2,1,2};
		String geography_1_15[] = {"1.  Industries that are concerned with the extraction of raw materials from Nature are called","2.  All the following except one encourages industrial growth in tropical Africa.","3. The followings factors are responsible for the low level of Industrial Development in Tropical Africa except","4.    The location of iron and steel industry at Abeokuta in Nigeria is best explained by the presence of","5. All the following are examples of heavy industry except","6.  Which of the following is true of manufacturing industries in Nigeria?","7. A continuous urban or industrial stretch resulting from the merging of cities is called","8.Which of the following is true of manufacturing industries in Nigeria?","9. Most of the industries located in rural areas are","10. Which of the following is a service industry?"};String geography_1_15no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String geography_1_15_options[] = {"A. Secondary industries","B.  Craft industries","C.  Primary Industries","D. Tertiary industries","","A.  Availability of capital","B. Availability of labour","C. Political stability","D. Availability of water","","A.  Insufficient capital","B. International image","C. Poor management","D. Inadequate power supply","","A.  Tin at Jos","B. Limestone at Ewekoro","C. River port at Lokoja","D. Coal at Enugu","","A.  furniture making","B. ship building","C. refining of petroleum","D.  iron and steel","","A.  Most of them produce consumer goods","B. Large quantities of their products are exported","C.  They depend totally on local raw materials","D. They were established in pre-colonial times","","A. A satellite town","B. A squatter settlement","C. A conurbation","D. A holiday resort","","A. Most of them produce consumer goods","B. Large quantities of their products are exported","C. They depend entirely on local raw materials","D. They were established in pre-colonial times","","A. market-oriented","B. high energy-oriented","C. raw materials oriented","D. transport oriented (WASSCE 2002)","","A.  Leather processing","B. farming","C. Petroleum refining","D. Banking",""};int geography_1_15_answers[] = {3,4,2,2,1,1,3,1,3,4};
		String geography_1_16[] = {"1.  Transport can be defined as the involvement of","2.  Which of the following is the oldest mode of transport?","3. The major advantages of road transport over other modes is its","4.    The use of water transport is limited by all the following except","5. Which mode of transport is most suitable for international trade?","6.  The term gauge in railway transport refers to the","7. Which of these modes is most suitable to the movement of petroleum?","8.The standard railway guage is","9. Which of these countries was the densent road network?","10. One relative advantage of water transportation over road transport is its"};String geography_1_16no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String geography_1_16_options[] = {"A. people and goods","B.  industries","C. buildings","D. markets","","A.  head porterage","B. railways","C.  air transport","D.  pipelines","","A. low cost","B. speed","C.  ability to carry bulky goods","D.  flexibility","","A. wide valleys","B. waterfalls","C. floating vegetation","D. narrow gauges","","A.  Road","B. Ocean transport","C.Air transport","D. Railways","","A.  weight of vehicle that the rails can bear","B. amount of rainfall experienced","C. distance between the two rails in a frack","D. length of the rails in a country","","A. Railways","B.  Pipelines","C. Ship","D. Tankers","","A.  134cm","B. 143cm","C.  150cm","D. 153cm","","A.    Nigeria","B.  Nigeria","C. United State of America","D.  Brazil","","A. capacity for passenger traffic only","B. ability to link all parts of any country","C.  suitability for bulky and heavy cargo","D. great speed over long distances.",""};int geography_1_16_answers[] = {1,1,4,4,2,3,2,2,1,2};
		String geography_1_17[] = {"1.  The distance between Umutonu and Ukuani Iyagbu as the crow flies is;","2.  The length of the road from Akwukwu-Igbo to Aniboko is approximately;","3.  If the total areas of Onicha-Olona settlement is 15 square centimetres on the map what is the actual area on the ground","4.    What is the direction of Ukuani Iyagbu from Akwukwu-Igbo","5. What is the precise location of Ugumoeke using four figure reference;","6.  The True North refers to;","7. The difference between the Magnetic North and the True North is called;","8. The standard meridian is;","9. Lines of latitudes are called;","10. Which of this is not a method of setting map on the field;"};String geography_1_17no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String geography_1_17_options[] = {"A. 5km","B.  15km","C.  10km","D. 10.1km","","A.  5.0km","B.  5.2km","C.  10.3km","D.  10.0km","","A.  2.75 sq km","B. 5.75 sq km","C.  4.3 sq km","D.  3.75 sq km","","A. South-West","B. North-West","C. South-East","D. North-East","","A.  2308","B.  2208","C.  0807","D.  2207","","A.  The Magnetic North","B.  The Angular North","C. The Geographic North","D. The Position of the Sun","","A. Magnetic Declination","B. Annual change","C. Magnetic Bearing","D.  Magnetic change","","A. Longitude 900","B. Longitude 600","C. Longitude 00","D.  Longitude 300","","A.  Northings","B. Eastings","C. Westings","D. Southings","","A. Making use of celestial bodies.","B.  By using compass.","C. Comparison with a prominent linear feature.","D. Methods of overlay",""};int geography_1_17_answers[] = {1,2,4,4,2,3,1,3,1,4};
		String geography_1_18[] = {"1.  Which of the following best accounts for the decline in the importance of rail transport in Africa?","2.  Which of the following dams in Africa provides both migration water and hydro-electricity?","3.   Which of the following vegetation types is most common in Africa?","4.    Africa has the largest potential reserves of hydro-electric power in the world due to","5. In Africa, the veldt is located in the","6.  In Africa, coral reef coasts are largely located in the","7. The rampant famine in some parts of Africa is caused by all the following except","8. The southernmost latitude in Africa is approximately","9. Which of the following does not explain the poor development of inland waterways in Africa?","10. The initial problems of rail and road development in Africa were due primarily to the"};String geography_1_18no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String geography_1_18_options[] = {"A. Low volume of goods available for transportation","B.  Threat of  retrenchment of railway workers","C.  High competition from road transport","D. Short distances covered by railways","","A.  Gezira","B.   Kainji","C.  Cabora Bassa","D.  Aswan","","A.  Steppe","B. Campos","C.  Selvas","D.   Savanna","","A.  its many plateaus.","B.  its high demand for power.","C.  the abundant rainfall and waterfalls there.","D.  the presence of very long rivers.","","A.  Western coastland","B.  Eastern coastland","C. Continental interior","D. Extreme north and south","","A.  north-west","B.   south-west","C. west","D. south","","A. unstable prices paid to farmers for their products.","B. unreliable rainfall.","C.  civil strife and unrest.","D.  ravage of crops by pests.","","A.  200S","B. 300S.","C. 350S.","D.  400S","","A. Lack of goods to carry along the routes","B. Presence of plains and waterfalls","C. Obstruction of floating vegetation","D. high seasonally of the rivers","","A. dense population.","B.  inadequate mineral resources.","C. difficult physical environment.feature.","D.  insufficient agricultural resources.",""};int geography_1_18_answers[] = {3,4,4,3,5,1,1,2,5,3};
		String geography_1_19[] = {"1.  The major fruits grown in the Mediterranean regions of Africa include:","2.  WIn Africa, the Mediterranean climate occurs in the","3. The Mediterranean vegetation is described as xerophytic vegetation because it is","4.    The most important cash crop grown in the Nile Basin is","5.  The Mediterranean type of climate is best suited for","6.  All the following are benefits of irrigation farming except","7.  Bush fallowing ensures that","8. Which of the following favours the growth of Olives in the Mediterranean region of Africa?","9. The predominant system of agriculture among rural farmers in Nigeria is","10. The basin system of irrigation in the Nile Basin is not reliable because"};String geography_1_19no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String geography_1_19_options[] = {"A. grapes, olives, apricots and figs.","B. lime grape fruits, orange and lemon.","C. pineapples, bananas, lime and figs.","D. apples, pears, apricots and olives.","","A.  south eastern coastland.","B.  northern part.","C. continental interior.","D. extreme north and south.","","A.   heat resistant.","B. fire resistant.","C. wind resistant.","D. drought resistant.","","A.  barley.","B.  cotton.","C.   groundnut.","D.  sugar cane.","","A.  cattle rearing.","B.  fruit growing.","C. rice cultivation.","D. vegetative gardening.","","A.  provision of employment.","B.  making early planting possible.","C. making farmers depend solely on rainfall.","D. bumper harvest.","","A.  artificial fertilizers are used.","B. only one crop is cultivated at a time.","C. the land regain is fertility naturally.","D. modern agricultural machinery are used.","","A. High temperature and humidity.","B.Heavy rainfall.","C. Abundant supply of skilled labour.","D.  cool, wet winter and warm, dry summers.","","A. terrace farming","B.  irrigation farming","C. plantation agriculture","D. bush fallowing","","A. too much of the water percolates into the ground.","B. only a few crops can be grown.","C. it is not considered suitable by some farmers.","D. It depends on the flow of the Nile in a particular year.",""};int geography_1_19_answers[] = {1,4,4,2,2,3,3,4,4,4};
		
		//////////////////////////////////////////////
		// Government
		/////////////////////////////////////////
		String government_1_1[] = {"1.An indispensable feature of government is","2.The institution of government which is responsible for the trial of criminals is","3.Which of the following cannot exist without the element of sovereignty?","4.   Absence of government in a society is called","5.  Which of the following is not a characteristic of the State?"};String government_1_1no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String government_1_1_options[] = {"A. personnel","B.  definite territory.","C.  permanency.","D. sovereignty.","","A.   the police.","B. the executive","C. the legislature","D. the judiciary.","","A.Nation","B.Government","C.  Society","D.   State","","A. fascism.","B.   unconstitutionalism.","C.   anarchy.","D.  tyranny","","A.  Definite territory","B.   Adequate people","C. Sovereignty","D.  Written constitution",""};int government_1_1_answers[] = {1,4,4,3,4};
		String government_1_2[] = {"1.An indispensable element of authority is","2. One of the limitations of sovereignty is","3.One of the sources of political authority is","4.    An indispensable requirement of legitimacy is","5.   Sovereignty is located in"};String government_1_2no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String government_1_2_options[] = {"A.  legitimacy","B.   coercion.","C.  physical force.","D. sovereignty.","","A.     authority.","B. moral justice","C.  treaty.","D.separation of powers.","","A.judicial authority.","B. administrative authority.","C.   military authority.","D.   legal authority.","","A. conformity with rules and principles.","B.    power.","C.  physical force.","D.  influence.","","A. the armed forces.","B.  the supreme court","C.  the legislature.","D.  legal-political sovereign.",""};int government_1_2_answers[] = {1,3,4,1,4};
		String government_1_3[] = {"1. Which of the following aspects of a people�s culture expressed in their political system constitutes their political culture?","2. The political culture of a country is said to be participatory if","3.Which one of the following activities is political socialisation in progress?","4.    One of the following is an agent of political socialisation:","5.     A country is democratic if","6.    Fascist movement originated in","7.    The socialist philosophy was first propounded by","8.    Which one of the following features is common to socialism and communism?"};String government_1_3no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5"};String government_1_3_options[] = {"A. Their attitudes, values, beliefs, and climatic conditions","B.    Their attitudes, values, beliefs and the President","C.  Their attitudes, sentiments, values and traditional institutions","D. Their attitudes, beliefs, sentiments, values and the Minister for Culture and Social Welfare","","A.     individuals are not interested in visiting the museum but may vote to elect their rulers as well as contest elections.","B.  individuals cannot criticise the government but can take part in all cultural activities of the state.","C.   individuals cannot form as many political parties as possible but may become financial members of as many social clubs as possible.","D.   individuals cannot oppose the government�s decisions but may wear their traditional dresses to work and parties.","","A.  During the morning assembly, the principal asked the pupils to sing the national anthem and recite the national pledge.","B. At the end of the inter-school debate, the social prefect declared that the next item on the agenda was a social get-together.","C.   At the social studies lesson, the teacher asked the pupils to render a wedding song popular in their village.","D.    The pupils were told after the inter-house sports that there will be a raffle draw and an end-of-term party.","","A.The school�s social welfare prefect.","B.   The Master of Ceremony(M.C.) during an uncle�s wedding reception..","C.    The 1986 winner of Miss Nigeria contest.","D.   The 1986 winner of Miss Nigeria contest.","","A.   the rulers are elected by universal adult suffrage, but there is no absolute economic equality.","B.    the government is elected under a multi-party system and opposition is recognised but the government is not responsible.","C.  the government is based on majority rule and respect for minority right but the leadership is not popular.","D.   the government is elected under multi-party system but not accountable to the opposition.","","A.  Germany..","B.   Japan.","C.    Spain.","D.   Italy.","","A.  Adolf Hitler.","B.    Mussolini.","C.  Karl Marx","D.   Lenin.","","A.    Dictatorship of the proletariat","B.   Stateless society","C.  Social justice","D.   Existence of government",""};int government_1_3_answers[] = {3,1,1,1,4,3,3,4};
		String government_1_4[] = {"1.  Constitutionalism emphasises that those who govern the State should do so","2. Parliamentary statutes constitute a large measure of","3. A constitution is said to be flexible if it is","4.   The federal constitution is the foremost source of authority in","5.     The Lord Chancellor of Great Britain, whose roles fuse into the three arms of government - legislature, executive, and judiciary-modifies - the principle of"};String government_1_4no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String government_1_4_options[] = {"A.  according to their whims and caprices.","B.    according to laid down rules of public behaviour.","C. according to the direction of the judges","D. Th according to the satisfaction of their egos.","","A.      the preamble for most constitutions.","B. the source of the Nigerian Constitution.","C.    a source of most constitutions.","D.   the directive principle of most constitutions.","","A.   easy to amend.","B.  the British Constitution.","C.  easy to read and understand.","D.    written piece-meal, instead of in one book.","","A. dual coordinate powers of governmental authorities.","B.   the Federal government as the all-inclusive government.","C.     the component government as the junior partner.","D.    the local level of government.","","A.     fusion of powers.","B.     parliamentary supremacy.","C.    rule of law.","D.   separation of powers.",""};int government_1_4_answers[] = {2,3,1,1,4};
		String government_1_5[] = {"1.Any system of government in which one level of government exercises an overwhelmingly predominant authority vis-a-vis the subordinate level of administration can best be described as a","2.The principal demerit of confederal system of government is","3.Whilst confederation recognises the right of the component units to secede; federation, on the other hand, treats secession or attempts at secession as","4.     In a unitary system of government, the relationship between the central and local government authorities is based on the principle of","5.  Sovereignty of constituent units is an indispensable feature of"};String government_1_5no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String government_1_5_options[] = {"A.  federal system.","B.  confederal system.","C.  parliamentary system.","D. unitary system.","","A.   the tendency of the confederal state to be too large in size or territory.","B. the existence of independent sovereign states.","C.the weakness of the central government.","D.the unrepresentative character of the confederal state.","","A.an irresponsible measure.","B.  an arbitrary measure.","C.   an ill-considered option.","D.   an illegal measure.","","A. co-ordinate relationship between the centre and the local areas.","B.   dependence of the local authority on the central government.","C.  equality between the central and the local government.","D.    mutual co-operation between the centre and the local government.","","A. federal system of government..","B.   confederal system of government.","C.  presidential system of government.","D.    parliamentary system of government.",""};int government_1_5_answers[] = {4,3,4,2,2};
		String government_1_6[] = {"1. A government is presidential whenever","2.The cabinet system of government is a system in which","3. Collective responsibility of the cabinet is relevant in relation to","4.     Under Presidential system, the judiciary is","5.   The first republic in Africa is"};String government_1_6no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String government_1_6_options[] = {"A.  there is President as the Head of State","B.    there is a President as Head of State and Head of Government.","C.  there is a President who is a member of the legislature.","D. there is a President who reigns.","","A.      there is a cabinet.","B.there are cabinet ministers.","C.  there are cabinet ministers in the legislature.","D. there are ministers who are also members of the legislature.","","A.  parliamentary system of government.","B.  presidential system of government.","C.    republican system of government.","D.     monarchical system of government.","","A.   part of the executive.","B.   independent of the executive.","C.  above the executive.","D.   controlled by the President.","","A. Nigeria.","B.Libya.","C.Liberia.","D.  Ethiopia.",""};int government_1_6_answers[] = {2,4,1,2,3};
		String government_1_7[] = {"1. An important merit of unicameral legislature is that","2. Which one of the following is a member of the judiciary?","3.In all parliamentary systems of government, there is","4.    The termination of each session of a House of Assembly is called","5.   Case-laws are made by"};String government_1_7no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String government_1_7_options[] = {"A.  it is suitable for a federal system of government.","B.  it is more appropriate for a presidential system.","C.  it saves public funds.","D.  it ensures a more thorough and careful legislation.","","A.   Customary Court President.","B. Minister of Justice.","C.  A Prison Warder.","D.  The Police.","","A.    separation of powers between the executive and the legislature.","B.  fusion of executive and legislative powers.","C.   no independence of the judiciary.","D.  no ceremonial president.","","A.    prorogation.","B.   dissolution.","C.  adjournment.","D.   closure.","","A. the executive.","B. the legislature","C.  the judiciary.","D.  local government councils.",""};int government_1_7_answers[] = {3,1,2,1,3};
		String government_1_8[] = {"1.    Rule of law means","2.   Separation of powers means","3.   The rights of a citizen can be limited if","4.    When parliament votes against executive action, we say it is","5.  One notable disadvantage of separation of powers is that"};String government_1_8no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String government_1_8_options[] = {"A.  absence of arbitrary power.","absence of rules.","C.    there is no freedom.","D.    all citizens rule.","","A.  legislature and executive must antagonise each other.","B. organs of government must not work together.","C.  organs of government must not cooperate.","D.  he violates the law.","","A. he is a Christian.","B.   he is a Muslim.","C.   he does not believe in God.","D.  he violates the law.","","A.   checks and balances.","B. fusion of power","C. abuse of power.","D.   limitation of power","","A.  it may make government firm.","B.  it may make government impossible.","C.   it may make government inactive.","D.  it may make government inflexible.",""};int government_1_8_answers[] = {1,4,4,1,2};
		String government_1_9[] = {"1.    Political participation can be increased in some developing countries if","2.   Centralisation of power is a distinctive feature of only one of the following:","3.  Only one of the following is a form of decentralisation:","4.    The authority exercising delegated legislation is","5. One of the reasons advanced for the exercise of delegated power is that","6.   The invocation of the doctrine of ultra vires against an instrument is a case of control of delegated legislation by"};String government_1_9no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5"};String government_1_9_options[] = {"A.  many more citizens are given the national merits award.","B. there is a written constitution.","C.   enough ballot papers are provided during elections.","D.   the level of literacy is raised substantially.","","A.Confederal States.","B. Federal States.","C.  Unitary States.","D.A union of States.","","A.  Devolution.","B. Concentration.","C.   Delegation.","D. Decontrol.","","A.  superior to parliament.","B. parallel to parliament.","C.  subordinate to parliament","D.  concurrent with parliament.","","A.   parliament needs to share its powers with other bodies.","B. there is need to tap the expertise of professionals and experts.","C. the people cannot entrust all their powers in the hands of members of parliament.","D. the executive understands the needs of the society better than parliament","","A.  the parliament","B. public criticism.","C.  the executive.","D.  the judiciary.",""};int government_1_9_answers[] = {4,3,1,3,2,4};
		String government_1_10[] = {"1.   Citizenship by naturalisation implies that the person being naturalised must be","2. Which of the following is not a condition for naturalisation?","3.  Which of the following is a type of citizenship?","4.    Which of the following is a political right?","5.An important duty of a citizen to his State is"};String government_1_10no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String government_1_10_options[] = {"A.   an alien by birth.","B.an indigene by birth.","C.  a foreign woman married by an indigene.","D.a citizen through registration.","","A.   Taking oath of allegiance","B.Renouncing citizenship of another State","C. Receiving national merit award","D. Being of full age","","A. Indigeneship","B. Registration","C.  Conferment","D.  Naturalisation","","A. Right to life","B. Right to form social clubs","C.  Freedom to vote","D.   Right to property","","A. freedom to work for the State.","B.allegiance to the State","C. owning property in the State.","D. fighting for his own rights within the State.",""};int government_1_10_answers[] = {1,2,3,2,3};
		String government_1_11[] = {"1. Which of the following is a characteristic feature of a political party?","2. In a true democratic and mass party, the highest body in the party organisational hierarchy is the","3.  Which of the following is a function of a political party?","4. A political party which is dominated and controlled by people of traditional authority and wealth is","5. Which of the following is a merit that is associated only with a two-party system?"};String government_1_11no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String government_1_11_options[] = {"A.  Sovereignty","B.Common ethnic origin","C. Common ideology","D.Control of government","","A. National Congress.","B. National Congress.","C.National Secretariat.","D.Parliamentary Party Committee","","A.  Determination of public policy","B.  Removal of government through a censure vote","C.   Declaring candidates elected in elections","D.  Political recruitment","","A.democratic party.","B.personality party","C.   mass party.","D.special Interest party.","","A. Existence of strong and formidable opposition party","B. Political education of the people","C. Provision of political ideology to the nation","D.Provision of political ideology to the nation",""};int government_1_11_answers[] = {3,1,4,1,1};
		String government_1_12[] = {"1. One of the functions of pressure groups is","2.One of the characteristics of pressure groups is that","3.   Lobbying in its true meaning involves the application of pressure on","4. Which one of the political techniques is not commonly used by pressure groups in the pursuit of their interests?","5.  Which one of the following is a type of pressure group?"};String government_1_12no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String government_1_12_options[] = {"A.interest articulation.","B. interest aggregation.","C. political recruitment.","D. policy formulation.","","A. it is concerned with the interest of the entire nation.","B. it is always an associational group.","C. it is created by the constitution.","D. it is a voluntary organisation.","","A.  those in government through gift of money.","B. legislators through gift of materials.","C. those in government through persuasive arguments.","D.  legislators by intimidation and blackmail","","A.d Demonstration.","B.  Non-violent civil disobedience","C.   Strikes","D. Lobbying","","A. Organisation","B. Propaganda","C. Bureaucracy","D.Demonstration",""};int government_1_12_answers[] = {1,4,2,2,3};
		
	///////////////////////////////////////
	/// History
	//////////////////////////////////////
		String history_1_1[] = {"1. history is the study of","2.  history is concerned with","3. history promotes national consciousness by emphasizing","4. One important role of history is to promote","5. history develops in the individual a/an","6. history is better used to promote","7. A student of history can develop the important skill of","8. Knowledge of history makes an individual to be","9. The study of past events is","10. The study of history aims at understanding"};String history_1_1no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String history_1_1_options[] = {"A. past developments","B. future developments","C.  rock formation","D. road construction","","A. human activities","B. wild life","C. highway development","D. predicting the future","","A. the relationship that existed among Nigerian groups","B. our rich cultural past","C. achievements of past leaders","D. differences that exist among the various groups","","A. insecurity","B. national consciousness","C. poverty","D. ethnic consciousness","","A. critical mind","B.  uncritical thinking","C. subsequent mind","D. doubtful mind","","A. international peace","B. international wars","C. national crises","D. intergroup conflict","","A. sourcing and organizing information","B. buying and selling imported goods","C. building bridges","D. repairing vehicles","","A.  hateful","B. tolerant","C. xenophobic","D. fearful","","A. useful and rewarding","B. a waste of time","C. unprofitable","D. useless","","A. present situations","B. past developments","C. future situations","D. past, present and future situations",""};int history_1_1_answers[] = {1,1,1,2,1,1,1,2,1,4};
		String history_1_2[] = {"1. Sources are pieces of information used in the study of","2.   ____________ are sources stored in the memory and are transmitted by word of mouth.","3.  The following are examples of oral sources except ________________","4. Radiocarbon dating is an example of _____________","5. The following are types of sources except ____________","6. Linguistics sources are the study of the ____________ of the people to establish relationships between them.","7. The study of material remains of the people in the past is called ________","8. The method used in analysing the reliability of sources before they are used for historical reconstruction are called _________","9. The following are examples of historical skills except ________","10. ______ sources are the study of culture, tradition and belief of the people"};String history_1_2no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String history_1_2_options[] = {"A. Geography","B. Government","C.  Economics","D. history","","A. Oral sources","B. Written sources","C. Anthropological sources","D. Historical sources","","A. oral account","B. oral tradition","C. oral comedy","D. rumors","","A. analyzing of sources","B. writing of sources","C.  dating of sources","D. recording of sources","","A. oral sources","B.  written sources","C. archaeological sources","D. drama","","A. culture","B. tradition","C. dressing","D. language","","A. Archaeological sources","B.  Linguistics","C. Anthropology","D. history","","A.  dources","B. interpretation","C.  culture","D. historical skills","","A. collection of data","B.  interpretations of evidence","C. the historian should not be biased when assessing data","D. using fake information","","A. Archaeological","B. Anthropological","C. Linguistics","D. Written",""};int history_1_2_answers[] = {4,1,4,4,4,4,1,4,4,1};
		String history_1_3[] = {"1.  ICT is more relevant in the study of ___________________","2.  One of the ways we can learn history is by the use of ___________","3.  The most essential attribute of ICT is that it is __________","4. One of the benefits of using ICT is that it is _________","5.  ICT systems can be categorized into _______","6. The Power Point presentation is a facility of the _______","7. A ____________ is not so important in the operation of the computer system.","8. The electronic library which has interconnections of many libraries is the ______","9. The instructional approach to history is most facilitated by __________","10. In capturing current events, history makes use of ___________"};String history_1_3no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String history_1_3_options[] = {"A. Ethnography","B. Physics","C.  Research Studies","D. history and the social sciences","","A. Archeology","B. Climatology","C. Weather forecasting","D. Commercial goods","","A. Computerized","B. Digital","C. New","D. Electronical","","A. Cost effective","B. Innovative","C. Multisensory","D. Skill Oriented","","A. Four","B.  Three","C. Six","D. Two","","A. Software","B. Hardware","C. Digital versatile disk","D. Multimedia system","","A. mouse","B.  systems unit","C. monitor","D. keyboard","","A.  cable network","B. website protocols","C. virtual Library","D. documentary","","A. teacher interaction","B. book reading","C. e-library","D. audio-Visual materials","","A. satellite communications","B.  archeological finds","C. data capturing machine","D. over head projectors",""};int history_1_3_answers[] = {4,1,4,4,2,4,1,2,1,2};
		String history_1_4[] = {"1. The people living in the semi-savannah zone in Nigeria are the  _____","2.  One of the most noticeable features of the core savannah zone is its _____","3. The Gulf of Guinea served as a bulwark to the people of ______","4. The water hyacinth is common in the ___________","5. The absence of mountains in Nigeria ________","6. The tribe which influenced the life pattern of the Kanuri people the most was ________","7.  The seven bastard states of Hausa, were traditionally referred to as ___","8.  While Oyo remained the military capital of the Yoruba, the ancestral home remained ______","9.  The middle zone is said to be a buffer zone for interaction between _____","10. As a result of trade, the comer language which became more wildly spoken in Nigeria was the ______"};String history_1_4no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String history_1_4_options[] = {"A. Itsekiri","B. Oyo","C.  Jukun","D. Kanuri","","A. aridity","B. humidity","C. tree climbing animals","D. coastal zone","","A.  Angas","B. Nupe","C. Yoruba land","D. South of the Niger","","A. mangrove swampy area","B. montare vegetation belt","C. fresh water swamps","D. kanem-Borno empire","","A. encouraged tribal wars","B.  encouraged inter-ethnic interrations","C. made the north vunerable to hot weather","D. made the livers obviously important","","A. Sefawa dynasty","B. Hausa","C. So","D. Bini","","A. Jukun","B. Kanem","C. Apa","D. Hausa Bokwai","","A.  Bini","B. Ile-Ife","C. Orangun","D. Alaketu","","A. The Igbos and their neghbours","B. Jukun and the Igala people","C. Bini and the Yoruba","D.  The forest zone and the savannah","","A. Gwari","B. Hausa","C. Fulfulbe","D. Ibibio",""};int history_1_4_answers[] = {1,2,3,1,2,1,4,2,4,2};
		String history_1_5[] = {"1. The tribal group that established the Saifawa dynasty in Kanem in the 11th century was the ______","2.  The Bayajidda legend refers to the rise of states among the _____","3. _______ was responsible for the establishment of the Nupe kingdom","4. The Igbo adventurer who influenced the establishment of the Igala kingdom is ____","5. Which of the following Hausa city states are grouped as Banza Bakwai?","6. The Ife prince whose marriage with the daughter of Beni chief's daughter gave rise to the Eweka dynasty in Benin was called ____","7. _______ was the ancestral founding father of the Igbo stock.","8.  The most important secret cult in Efik land was ______","9. Which of the following statement is correct:","10. ______ was responsible for the founding of the Itsekiri kingdom."};String history_1_5no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String history_1_5_options[] = {"A. Kanuri","B. Soa","C.  Zaghawa","D. Tebu","","A. Nupe","B. Hausa","C. Jukun","D. Igala","","A. Bayajidda","B. Ayagba","C. Oduduwa","D. Tsoede","","A. Eri","B. Duru","C. Achadu","D. Eze Chima","","A. Gobir","B.  Daura","C. Kano","D. Jukun","","A. Owodo","B. Iginuwa","C. Obagodo","D. Oranmiyan","","A. Oduduwa","B. Bayajidda","C. Eri","D. Eze","","A.  Obong","B. Ibang","C. Ekpe","D. Okpan","","A. The Ijo, Efik and Itsekiri were fisherman and nothing else","B. The Ijo, Efik and Itsekiri did not participate in any trade with the Europeans","C. The Ijo, Efik and Itsekiri were Delta peoples.","D. The Ijo, Efik and Itsekiri did not fight any wars.","","A. Oranmiyan","B. Umade","C. Iginuwa","D. Olua",""};int history_1_5_answers[] = {3,2,4,3,4,4,4,3,3,3};
		String history_1_6[] = {"1. One of these is an early centre of civilization in Nigeria:","2.  Objects discovered in the Nok culture area included _________","3. Daima is found in __________","4. People that lived in iron age culture made use of _________","5.  Igbo-Ukwu is located in __________________","6.  Excavation work at Igbo-Ukwu took place between _____ and _____.","7. Ife civilization is famous for its _____________","8. Ile-Ife is located in __________","9. Benin civilization is located in _______","10.  In 1897, Benin art was looted by _______"};String history_1_6no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String history_1_6_options[] = {"A. Lagos","B. Owerri","C.  Umuahia","D. Nok","","A. terracotta figurines","B. vehicle parts","C. elephant tusks","D. leopard skin","","A. Kaduna state","B. Oyo state","C.  Osun state","D. Borno state","","A.  bronze implements","B.  stone implements","C. wood implements","D.  inferior implements","","A. Anambra state","B.   Imo state","C. Igbo state","D. Delta state","","A. 1959 - 1964","B. 1960 - 1970","C. 2001 - 2005","D. 1999 - 2000","","A. terracotta and bronze objects","B. Wood and paper materials","C.  stylized materials","D. mechanical designs","","A.  Abia State","B. Anambra State","C. Oyo State","D. Osun State","","A. Edo state","B.  Delta state","C.  Lagos state","D. Kaduna state","","A. Nigerian soldiers","B. British soldiers","C. federal police","D.  national defence",""};int history_1_6_answers[] = {1,1,4,1,1,1,1,4,1,2};
		String history_1_7[] = {"1. ______ is an example of a large centralized state in Northern Nigeria.","2.  _____ is an example of small centralized state in Northern Nigeria.","3. ____ is a false claim that state ideology and organization were imported into Negro Africa by non-Negro invaders from Egypt or the Middle East.","4. _______ is an example of a centralized state in pre-colonial Southern Nigeria","5. _____ used their armies to fight wars of expansion and annex weaker states which paid it tribute as vassal states.","6. ____ refer to societies that resents leadership by an all-powerful monarch.","7. Consultative Assemblies in non-centralized states are noted for ____","8. Gerontocratic leadership refers to _____________","9. __________ enforces laws made by consultative assemblies.","10. Age grades were important instruments of social control in non-centralized societies like Igbo land due to their roles as ________"};String history_1_7no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String history_1_7_options[] = {"A. Jukun","B. Igala","C.  Nupe","D. Kanem-Borno","","A. Igala","B. Kanem-Borno","C. Tiv","D. Jukun","","A. Oduduwa myth","B. Bayajidda myth","C. Hamitic myth","D. Egyptian myth","","A. Itsekiri","B. Agbor","C. Old Oyo Empire","D. Benin","","A. Non-centralized states","B.  Acephalous societies","C. Segmentary societies","D. Centralized states","","A. Centralized states","B. Nation state","C. Acephalous state","D.  Non-centralized states","","A. making of harsh laws","B.  enthroning dictators who oppress their subjects","C.  leads their community to fight unjust wars with their neighbours","D.  makes laws that govern their society through the principle of consensus (Igba-izu)","","A.   leadership by the elderly class","B. leadership by the wealthy class","C.  leadership by members of the Ekpe secret club","D.  leadership by women","","A. Village heads","B. Titled men like Nze na Ozo","C. Okpara","D. Secret societies such as Ekpe, Okonko, Mmanwu","","A. priests","B. diviners","C.  vigilante groups that guard lives and property of people in the villages, lineages and sub-lineages.","D. law enforcement agents",""};int history_1_7_answers[] = {4,2,3,4,4,4,4,1,4,3};
		String history_1_8[] = {"1. The Kanem Empire was founded in the area East of ______","2.  A centralized system of government developed in Kanem headed by the ___","3. Kanem was relocated to _______ due to external attack","4. Mai Ali Ghaji established a new capital for Bornu at ______","5. The following are Hausa states except ______","6.  Relations between Hausa states was based on economy and _______","7.  Islam was introduced in Kano during the reign of _______","8. Tsoede was the founder of _________","9. The Nupe kingdom was founded in ___________","10. The title of the rulers of the Nupe kingdom is _______"};String history_1_8no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String history_1_8_options[] = {"A. River Niger","B. River Benue","C.  River Kaduna","D. Lake Chad","","A. Ezes","B. Sarkis","C. Etsus","D. Mais","","A. Katsina","B. Kano","C. Bornu","D. Nupe","","A. Ngazargamu","B. Kanem","C. Chad","D. Agadez","","A. Kwararafa","B.  Katsina","C. Kano","D. Daura","","A. warfare","B. trade","C. commerce","D. production","","A. Sarki Yaji","B. Sarki Rumfa","C. Sarki Korau","D. Sarki Kanajeji","","A.  Kanem empire","B. Bornu empire","C. Nupe kingdom","D. Kano kingdom","","A. Niger-Kaduna area","B. Niger-Benue area","C. Sokoto area","D. Kaduna river","","A. Sarki","B. Oba","C. Etsu","D. Eze",""};int history_1_8_answers[] = {4,4,1,1,1,1,1,3,1,3};
		String history_1_9[] = {"1. The Yorubas trace their origin to ______","2.  The Oyo empire emerged in the 15th century and covered a large part of ____","3. Oyo developed a centralized system of government headed by the ___","4. The economy of Oyo was based on the following except _____","5. The Benin Empire emerged in the _____ region","6. Ewuare the Great was one of the rulers of ____","7. _______ was the title of the Benin ruler.","8.  The Efik state was founded around __________","9. _____ were organized as independent villages headed by priest heads.","10. The people of Efik participated as ____________ the trade between Europeans and the hinterland."};String history_1_9no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String history_1_9_options[] = {"A. Igbo land","B. Delta","C.  Asia","D. Ile-Ife","","A. Hausaland","B. Igbo land","C. Niger-Delta","D. Yoruba land","","A. Oba","B. Alafin","C. Oni","D. Oyemesi","","A. agriculture","B. trade","C. levying of taxes","D. groundnut production","","A. savannah","B.  forest region","C. dessert","D. lake","","A. Igboland","B. Hausa land","C. Benin Empire","D. Oyo Empire","","A. Obi","B. Oba","C. Emir","D. Etsu","","A.  River Niger","B. River Benue","C. Cross River Basin","D. Lake chad","","A. Igbos","B. Hausas","C. Efiks","D. Kanem Bornus","","A.  middlemen","B. buyers","C. sellers","D. producers",""};int history_1_9_answers[] = {4,4,3,4,3,3,2,3,3,1};
		String history_1_10[] = {"1. A marked difference between centralized and non-centralized states lies on the emergence of ____________ of centralized states.","2.  The age-grades and the secret societies acted as ______________ in the traditional societies","3. The circumstances could deny the Diokpara his entitlements in Igboland include:","4. The system of taxation was well established in _______","5. Religion was main was activity of ______","6. The new class of chiefs in Tivland who received their staff of office from the king of the Jukun the king was called ____","7. The most prominent part of Ibibioland has remained _________","8. The Esop Idung was usually regarded as _____","9. While Iduh was generally regarded as the father of the Idoma people, __________ was regarded as its ancestral origin.","10. Before the Islamic Jihad inter-group, relations in the North was mainly sustained through ____"};String history_1_10no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5"};String history_1_10_options[] = {"A. government","B. leaders","C.   oligarchy","D. political system","","A. the police","B. arbiters","C. wife catchers","D. oracles","","A. If he died while his father was alive","B. If he killed twins","C. If he married so early","D. If he never got married","","A. Igalaland","B. Ibibioland","C. Idoma","D. Kano","","A. caravan traders","B.  slave raiders","C. traditional hunters","D. carvers","","A. Tee","B. Drum Chiefs","C. Mba Tsav","D. Efiat","","A. Calabari","B. Eket","C. Efik","D. Efiat","","A.   an appellate court","B.  a court of first instance","C. a tribunal","D. a commission of inquiry","","A. Apa","B. Kanuri","C. Cross rivers","D. Idoma","","A. Inter-tribal wars","B. Common religious worship","C. Inter-tribal marriages","D. Hunting activities",""};int history_1_10_answers[] = {2,1,1,2,2,3,2,1,3,3};
		String history_1_11[] = {"1. Political and social organization of the Isoko people were based on","2.  ________________ was known for practicing gerentocracy","3. The Idoma titled elders were known as","4. _____________ served as a unifying factor among the Iroko people","5. _____________ is an important arm of government in Tiv"};String history_1_11no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String history_1_11_options[] = {"A. king","B. Village Structure","C.  Clan","D. Lineage","","A. Isoko","B. Tiv","C. Ibo","D. Ibibio","","A. Igabo","B. Erabo","C. Irobo","D. Eduabo","","A. age grade","B. Elders","C. community","D. Society","","A. Mba Tsau","B.  Wukari","C. Ogboni","D. Nri",""};int history_1_11_answers[] = {2,1,1,1,1};
		String history_1_12[] = {"1. The canoe men brought to Nupe taught the villagers _________ and ________","2.  The most important source of revenue in Nupe was monopoly of","3. The military system of Nupe made them to be successful in ________  and ________","4. During the waging war of expansion Gbara was the capital of Nupeko for  _______ year."};String history_1_12no[] = {"1 of 5","2 of 5","3 of 5","4 of 5"};String history_1_12_options[] = {"A. Farming and trading","B. Craft and Farming","C.  Fishing and Trading","D. Craft and building of large canoes","","A. Brass works","B. Bronze works","C. Kolanut","D. Slaves","","A. Trading and industry","B. Political and economic","C. Economic and trading","D. Fishing and political","","A. Six","B. Eight","C. Eighteen","D. Nine",""};int history_1_12_answers[] = {1,3,2,2};
		String history_1_13[] = {"1. ____________ dominated trade among the Ibos","2.  The Tivs were noted much for _________________","3. _______________ specialized in iron smelting","4. ______________ produced leather works","5. ___________ was a successful trader who rose to the position of a king"};String history_1_13no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String history_1_13_options[] = {"A. Akwa","B. Aro","C.  Idona","D. Orlu","","A.  slave trade","B. trading","C.  Agriculture","D. bronze making","","A. Ibo","B. Yoruba","C. Itsekiri","D. Nok in plateau","","A. Ibo","B. Yoruba","C. Hausa","D. Tiv","","A. Oba of Benin","B.   Nana of Itsekiri","C. Ewuare the great","D. Ogde",""};int history_1_13_answers[] = {2,3,4,3,2};
		String history_1_14[] = {"1. One of these factors did not pave way for European visit to Nigeria","2.  _________ were the first set of explorers to visit the coast","3.  ________________ mission also visited Benin and Warri","4. The early European contact led to all these factors except","5. The missionary that attempted converting the Oba of Benin first but failed was"};String history_1_14no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String history_1_14_options[] = {"A. Support from home government","B. Change in the production of good from hand to machine","C.  The First World War","D. The absence of Feudalism","","A. The Dutch","B. The Portuguese","C. The Spanish","D. The Italian","","A. The Italian Capuchiri missionaries","B. The Spanish missionaries","C. The Dutch missionaries","D. The British missionaries","","A. Introduction of titles and villages","B. Introduction of Christianity","C. Introduction of crops","D. Influence on the people culture","","A. Afonso d'Aveiro","B.  Prester John","C. Robbert Stone","D. William Wiber force",""};int history_1_14_answers[] = {3,2,1,1,1};
		String history_1_15[] = {"1. ___________ was strongly opposed to slave trade in the British Parliament","2.  Britain abolished slave trade in the year _________________","3. America finally granted the right of search in the year ________","4. One of these was an obstacle to the abolition of slave trade","5. The abolition of slave trade and slavery led to the establishment of"};String history_1_15no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String history_1_15_options[] = {"A. Ajayi Crowther","B. Sir Frederick Lugard","C.  Adams Smith","D. William Wilberforce","","A. 1906","B. 1789","C. 1807","D. 1729","","A. 1810","B. 1862","C. 1831","D. 1729","","A. The humanitarians rejected it","B. The British Government was not interested in abolition","C.  It was basis for economic development in Europe","D. Slaves were not willing to be freed","","A. Sierra-Leone and Liberia","B.  Liberia and Spain","C. France and Sierra- Leone","0",""};int history_1_15_answers[] = {4,3,2,3,1};
		
		//////////////////////////////////////
		// IRK
		//////////////////////////////
		String irk_1_1[] = {"There Are NO QUESTIONS Loaded Here"};String irk_1_1no[] = {"1 of 5"};String irk_1_1_options[] = {"Empty Answer","Empty Answer","Empty Answer","Empty Answer",""};int irk_1_1_answers[] = {4};
		
		/////////////////////////////////
		// Mathematics
		/////////////////////////////////
		String mathematics_1_1[] = {};String mathematics_1_1no[] = {};String mathematics_1_1_options[] = {};int mathematics_1_1_answers[] = {};
		
		///////////////////////////////////
		// Yoruba
		/////////////////////////////////
		String yoruba_1_1[] = {"There Are NO QUESTIONS Loaded Here"};String yoruba_1_1no[] = {"1 of 5"};String yoruba_1_1_options[] = {"Empty Answer","Empty Answer","Empty Answer","Empty Answer",""};int yoruba_1_1_answers[] = {4};
		//////////////////////////////////
		// Agriculture
		////////////////////////////////
		String agriculture_1_1[] = {"One factor which differentiates a subsistence from a commercial farmer is that the former produces","Which of the following land tenure system of owner encourages the establishment of permanent crops?","The land tenure system that result in fragmentation of land is","4. The  use of land for agriculture is not influenced by","5. Igneous rock formed within the crust are called","6. The chemical reaction in rocks represented  KAlSibase3Obase8 + Hbase2O ----> HAbase1Sibase3Obase8  +  KOH is an example","7. Clay particles are able to attract cations in the soil because they","9.  The most important soil micro-organisms causing decay and nutrient recycling are","10. Which of the following soil water is tightly held to the surface of soil particles?","11.Which of the following process will not lead to the loss of nutrient from the soil?","12. A disadvantage of using organic manure on the soil is that","13. leguminous cover crops   do not","14. in the nitrogen cycle, nitrite is oxidized to nitrate by","15. Which of the following will not  lead to loss of nitrogen compound from the soil?","16. Farm drainage  does not lead to","17. Which of the following methods of irrigation is best for a farm with an undulating landscape?","18.  The most unreliable source of farm power is.","19.   Water power is harnessed and converted into electrical power by","20. A  farm implement used for planting grains is the","21. Which of the following parts of a tractor is most essential in operating a mower?","22. Farm mechanisation in West Africa is not limited by","23.  Mechanisation in West Africa is limited by the following except","24. A fruit which will not normally be used in the manufacture of Jam is","25. The following crops are common staple food in West Africa except","26. In cassava farming rapid growth is promoted at the expense of tuber formation by the application","27.  Ananas comosus is a","28. The best way to preserve maize grains on a large scale is by","29. Parboiling is a term associated with the processing of","30. Maize is very important crop because apart from serving as a staple, it","31. A variety of oil palm is","32. Andropogon gayanus is commonly called","33. Pasture crops are best used in preparing hay","34. Which of the following is not a characteristic of a good pasture?","35. Black pod disease of cocoa is controlled by","36.    The side effects of chemical control of diseases and pests include the following except","37. Plant breeders aim at obtaining the following except","38.   Muturu and sahel","39. An adult female goat is a/an","40.The interval from the beginning of one heat period to the beginning of another is known as","41.Which of the following animal possesses the illustrated digestive tract?","42. The part where gastric juice is secreted is labelled","43. Water is largely reabsorbed in the part labelled","44. The act of parturition in ewe is known as","45. The pregnancy hormone in female animal is","46.  which of the following management practice is specific to dairy farming?","47. Which of the following is the main reason for not keeping birds meant for commercial egg production under the extensive system of management? Birds","48. Newcastle disease is cause by.","49.A viral disease of ruminants in which the affected animal shows symptoms of high fever and and blood stained diarrhoea is likely to be","50. The following are practical measures of controlling trypanosomiasis except","51. The following factors are necessary for siting fish ponds  except","52. The method of fish preservation in which most of the moisture is lost is","53. Which of the following statement is  not true of agricultural production?","54. `The functions of a farm manager include the following except","55. In the event of increased government subsidies, the farmers production cost","56. The curve for unitary elasticity of supply for maize flow is given by","57. The equilibrium price of rice is the price at which","58. Which of the following activities is not correct about extension agents?","59. One of the functions of agricultural extension is","60. Why would an Extension Officer prefer the use of television to radio when introducing a new technology to farmers?","61.  Which of the following defines agriculture?","62. In subsistence agriculture","63. Which of the following is not correct about the importance of agriculture?","64. An advantage of commercial agriculture is that it","65.Agriculture helps to solve all the following"};String agriculture_1_1no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5","11 of 5","12 of 5","13 of 5","14 of 5","15 of 5","16 of 5","17 of 5","18 of 5","19 of 5","20 of 5","21 of 5","22 of 5","23 of 5","24 of 5","25 of 5","26 of 5","27 of 5","28 of 5","29 of 5","30 of 5","31 of 5","32 of 5","33 of 5","34 of 5","35 of 5","36 of 5","37 of 5","38 of 5","39 of 5","40 of 5","41 of 5","42 of 5","43 of 5","44 of 5","45 of 5","46 of 5","47 of 5","48 of 5","49 of 5","50 of 5","51 of 5","52 of 5","53 of 5","54 of 5","55 of 5","56 of 5","57 of 5","58 of 5","59 of 5","60 of 5","61 of 5","62 of 5","63 of 5","64 of 5"};String agriculture_1_1_options[] = {"A.for the benefit of his community","B. only one type of crop","C. for the benefit of his family","D. Raw materials for agro-based industries","","A. Free hold system","B. Tenancy","C. Lease hold system","D.      Pledging","","A.   freehold","B.  government tenancy","C.   inheritance","D.       leasehold","","A.   Labour","B.  Climate","C.   Topography","D.     Population","","A.  Quartzite","B. Volcanic","C.    Plutonic","D. Stratified","","A. hydrolysis","B.    hydration","C.  oxidation","D.  carbonation","","A.   Are positively charged","B.   Have excess negative charges","C.  Are sticky","D.  can be moulded.","","A.  Bacteria and fungi","B.  Fungi and algae","C.  Algae and actinomycetes","D.  bacteria and algae","","A.  Hygroscopic water","B.  Gravitational water","C. Capillary water","D.  Superfluous water","","A.  Erosion","B. Mulching","C. Harvesting","D.  Leaching","","A. it decreases potassium content of the soil","B.  diseases spread fast","C. it increases the humus content of the soil","D.  it reduces the rate of soil water evaporation","","A.  bind the soil particles together through their roots","B.   increase the nitrogen content of soils.","C.  aid in improving the soil organic matter content.","D.  reduce the soil microbial population","","A.  Nitrobacter","B.  nitrosomonas","C. Azotobacter","D.  Rhizobium","","A. crop removal","B.   burning","C. lodging","D.  leaching","","A.   improvement in soil structure","B.   improvement of soil temperature","C.   increased soil aeration","D.    reduction of soil alkalinity","","A.  sprinkler","B. Flooding","C.   furrow","D.    basin","","A. wind","B. waterr","C. electricity","D.  machine","","A.   blades","B.cranes","C.   turbines","D.    hover craft","","A.   Drill","B.      Ridger","C.    Harrow","D.      Plough","","A.  steering wheel","B. Top link","C. Power- take- off shaft","D.    Stabilizer chains","","A.   The large population of farmers","B.  Poverty among the farmers.","C.    Inadequate infrastructural facilities.","D. Seasonality of agricultural production","","A.    size of farm holding.","B.     technical know how","C.   capital","D.    labour","","A.  orange","B. pineapple","C.  garden egg","D.   pawpaw","","A.   coconut","B.   maize","C.  cassava","D. cowpea","","A. potassium fertilizer","B.  nitrogen fertilizer","C.  complete fertilizer","D.   phosphate fertilizer","","A. fruit crop","B. oil crop","C.beverage crop","D. pasture crop.","","A.   storing under the sun.","B.  Storing in earthen pots.","C.   hanging over fire places","D.      storing in silos","","A. Cowpeay","B.   melon","C. sorghum","D.  rice","","A. is ploughed into the soil to increase soil fertilizer","B.   can be processed into corn flakes.","C.    forms the base of most livestock feeds","D.   is used for producing germ oil.","","A.   Trinitario","B. Teneras","C.  Criollo","D. Amelonado","","A.   guinea grass.","B.  Elephant grass","C.   gamba grass","D.   spear grass.","","A.  when they are two weeks old.","B.   just before flowering","C.   just after seed maturation","D.  after they have started to dry up","","A.    high quality grass","B.  provision of balance nutrients","C.  ability to withstand trampling","D.  high stem to leaf ratio","","A.   fumigation of soil.","B.   farm sanitation","C.  application of herbicide.","D.  application of fungicide","","A.  environmental pollution","B.   poisoning of livestock and man.","C.  destruction of some plant species","D.  increased cost of production.","","A.   tolerance to climatic extremes.","B.     resistance to pests...","C.    adaptation to pollination.","D.      uniformity in the time of maturity.","","A.   Ouda and white Fulani","B.     Ouda and white Fulani","C.    Ndama and keteku","D.    Gudali and red sokoto","","A.    Gilt","B. heifer","C. ewe","D.  doe","","A.gestation period.","B.     incubation period","C.   reproductive cycle.","D.   oestrus cycle","","A.  sheep","B.  fowl","C.  pig","D.  duck","","A.   II","B. IV","C. V","D.  VIII","","A. II","B.   IV","C.VI","D.  VIII","","A.   farrowing","B.  hatching","C. lambing","D.   calving","","A. Oestrogen","B.   Oxytocin","C. progesterone","D. relaxin","","A.  milking","B.deticking","C.  castration 				.","D. deworming","","A.  are exposed to attack by predators.","B.  waste a lot of productive energy roaming about","C.  are exposed to sufficient green forage.","D.     are exposed to theft","","A.  virus","B.   bacterium","C.protozoon","D.    fungus","","A.  Pleuropneumonia","B. rinderpest","C. gastroenteritis","D.  babesiosis","","A.   spraying against the vector.","B. modification of vectors habitat.","C. use of drug on diseased livestock.","D.  vaccinating against trypanosomes","","A.  reliable source of water supply","B.    type of soil","C.  topography","D.   climatic conditions","","A.   frying","B.   salting","C.  drying","D. canning","","A.  Land is an immobile asset","B. A fishing ground is a factor of production","C.  Human resource constitute a factor of production","D. Availability of land is more important than its quality","","A.  decision making","B.  coordination of farm activities","C.  supervision of farm labourers","D.      financing agricultural projects","","A.     falls, hence supply increases","B.  falls, hence supply decreases.","C.     rises, hence supply increases.","D.     rises, hence supply decreases","","A.   See attached graphic for Option A","B.    See attached graphic for Option B","C.  See attached graphic for Option C.","D.  See attached graphic for Option D.","","A.  its demand exceeds supply.","B.   its supply exceeds demand","C.  its supply equals demand","D. consumers cannot buy the quantity they want","","A.   educating farmers on the use of improved techniques.","B.   teaching farmers how to plan their planting.","C.   training school leavers to become professional farmers","D.   teaching farmers how to prepare and apply compost manure","","A.     cultivation of farmers farm lands","B.   construction of farmers farmsteads","C.      organization and coordination of credit sources","D.     running of the country formal education system","","A.    The extension officer can get immediate feedback.","B.    The farmer can observe the various steps involved","C.   Radio programmes are not reliable and clear enough","D.   It is cheaper to use television than radio","","A. An art of farming inherited from our fore-father","B.  The raising of crops to satisfy human needs","C. A way of life by which farmers produce food","D.the science and art of farming to satisfy human needs.","","A.  yields are high","B.   surplus produced is small","C.   farm sizes are large","D.      Large area of land is used","","A.  supply of food","B.  provision of employment","C.   serving as main source of foreign exchange","D.    serving as a source of income","","A.   requires small space","B.  produces high returns","C.    requires skilled labour","D.   involves low capital","","A.  malnutrition","B. unemployment","C.  high population growth rate","D. inadequate shelter materials",""};int agriculture_1_1_answers[] = {4,2,4,2,4,2,3,2,2,3,3,3,2,4,2,2,2,4,2,4,2,1,4,2,3,2,3,2,4,3,4,3,3,2,3,4,4,2,3,2,3,2,4,4,2,3,2,3,4,2,4,3,2,2,3,4,4,4,3,4,3,4,3,4};
		
		///////////////////////////////
		// Chemistry
		/////////////////////////////
		String chemistry_1_1[] = {"1. Which of the following statements about X  is correct, if its atom is represented as ?X ?","3. Which of the following properties increases from the top of a group to the bottom in the periodic table?","4. Which of the following statements about catalysts in reversible reactions is correct? They","5. Which of the following elements forms a covalent oxide?","6.  Aluminium atom contains 13 protons and 14 neutrons what is the number of electrons in its ion, Al??","7. Which of the following properties is characteristic of metals?","8.  Emission from radioactive substances can be detected by","9. Which of the following oxides will reacts with sodium hydroxide solution to form a salt?","10. The following salts can be prepared by double decomposition   except","11.A substance changes from the gaseous to the solid state. Which of the following properties of its molecules increases in the process?","12.","13. Hydrogen is one of the products of the reaction between","14. The bleaching of chlorine is due to its","15. Powdered aluminium reacts rapidly with NaOH solution whereas aluminium foil reacts only moderately with the solution. This is because the powdered form","16.","17. A Sample of water which was passed through a de-ioniser may still contain","18. Which of the following elements can prevent iron from rusting by functioning as sacrificial anode?","19.Biotechnology is applied in the production of","20. Which of the following reagents can be used to remove moisture from a sample of air? I.   Lime water II.   Fused calcium chloride III.   Alkaline pyrogallol","21. Global warming arises from atmospheric pollution by","22. Zinc ions can be displaced from a solution of ZnS? by","23. An element R  burns in air to form an oxide which dissolves in water to give a solution with pH greater than 7. R could be","24. The molar conductance of sodium hydroxide solution is higher than that of aqueous ammonia of the same concentration because sodium hydroxide","25. Which of the following polymers is a polysaccharide?","26. The structures of many enzymes  indicate that they are proteins which derive their catalytic ability from the presence of","27. What is the role of dibromoethane as petrol additive?","28. Which of the following substances, on being electrolysed, will produce mainly chlorine at the anode?","29. The mass of an element liberated at an electrode during electrolysis depends on which of the following. I. Nature of the electrode II Quantity of electricity passed III Magnitude of electrical charge its ion","30.","31. Alkanoic acids are prepared in the laborating by the","32. The by-product obtained during the industrial production of soap is","33. Bromobenzene is obtained from benzene by","34. Which of the following compounds form a reddish brown deposit with ammoniacal copper (I) chloride solution?"};String chemistry_1_1no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5","7 of 5","8 of 5","9 of 5","10 of 5","11 of 5","12 of 5","13 of 5","14 of 5","15 of 5","16 of 5","17 of 5","18 of 5","19 of 5","20 of 5","21 of 5","22 of 5","23 of 5","24 of 5","25 of 5","26 of 5","27 of 5","28 of 5","29 of 5","30 of 5","31 of 5","32 of 5","33 of 5"};String chemistry_1_1_options[] = {"A. The mass number of X is 12","B. X  is a group II element","C. X  has 12 electrons in its outermost shell","D. The atomic number of X  is 24.","","A. Atomic radius","B. Electronegativity","C. Ionization energy","D. Electron affinity.","","A. have no effect on the activation energy","B. alter equilibrium position","C.lower the value of heat change","D. affect the forward and reverse reaction rates equally","","A. Calcium","B. Sodium","C. Magnesium","D. Carbon","","A. 10","B. 13","C. 24","D. 27","","A. Ability to accept electrons readily","B. Formation of covalent hydrides and chlorides","C. Formation of acidic or neutral oxides","D. Ability to act as reducing agents","","A. bomb calorimeter","B. X-ray equipment","C. Geiger- Muller counter","D. electrochemical cell.","","A. Copper II oxide","B. Allumunium oxide","C. Sodium oxide","D. Magnesium oxide","","A. PbCl2","B. ZnSO4","C. AgCl","D. CaCO3","","A. Orderliness","B. Random motion.","C. Kinetic energy","D. Intermolecular distance","","A. 137k","B. 283k","C. 546k","D. 760k","","A. red hot iron filings and steam","B. copper and dilute hydrochloric acid","C. magnesium ribbon and cold water","D. trioxonitrates V acid and zinc","","A. reaction with water to form oxochlorate I acid","B. ability to combine directly with other elements","C. greenish yellow coloration","D. ability to destroy germs in water.","","A. has a larger surface area","B. contains weaker metallic bonds","C. is a stronger reducing agent","D. has a higher relative density.","","A. 10.0cm","B. 37.5cm","C. 60.0cm","D. 62.5cm","","A. Temporary hardness","B. Organic impurities","C. Permanent hardness.","D. heavy metal ions","","A. Copper","B. Silver","C. Graphite","D. Magnesium","","A. Plastics from ethene.","B. Drugs using micro-organisms","C. Petrol from petroleum.","D. Magarine using vegetable oil.","","A. I only","B. II only","C. I and II only","D. II and III only","","A. Radioactive fallout","B. carbon IV oxide","C. agrochemicals","D. sulphur VI oxide","","A. Silver","B. Copper","C. Iron","D. Magnesium","","A. Carbon.","B. Sodium","C. Phosphorus.","D. Copper.","","A. has a higher molar mass.","B. produces more ions in solution","C. is a compound of a metal.","D. is highly deliquescent.","","A. Cellulose","B. Rubber","C. Nylon","D. Polyethene","","A. Metal ions","B. non-metals","C. halogens","D. carbon atom","","A. Makes the petrol more volatile.","B. Reduces the level of pollution from vehicular emissions","C. Prevents the deposition of lead compounds in engines","D. Increases the energy value of the petrol","","A. Dilute sodium chloride solution","B. Dilute sodium chloride solution","C. Tetrachloromethane","D. concentrated potassium chloride solution","","A. I only","B. I and III only","C. II and III only","D. I, II and III","","A. Tetraoxosulphate (VI) ion are preferentially discharged","B. Acidity decreases at the anode","C. Concentration of the acid increases","D. The acid becomes more dilute","","A. Oxidation of primary alkanols","B. Fermentation of sugars","C. Hydrogenation of alkenes","D. Hydrolysis of fats and oils.","","A. Sodium hydroxide.","B. an alkanoate.","C. a polyhydric alkanol","D. Sodium chloride.","","A. Polymerization.","B. reduction.","C. substitution.","D. addition.","","A. Ethyne","B. Ethanoic acid.","C. Ethene.","D. Ethanol.",""};int chemistry_1_1_answers[] = {2,1,4,4,1,4,3,2,2,1,3,1,1,1,1,2,2,2,2,2,4,2,2,1,1,3,4,3,3,1,3,3,1};
		String chemistry_1_2[] = {};String chemistry_1_2no[] = {};String chemistry_1_2_options[] = {};int chemistry_1_2_answers[] = {};
		String chemistry_1_3[] = {};String chemistry_1_3no[] = {};String chemistry_1_3_options[] = {};int chemistry_1_3_answers[] = {};
		String chemistry_1_4[] = {};String chemistry_1_4no[] = {};String chemistry_1_4_options[] = {};int chemistry_1_4_answers[] = {};
		String chemistry_1_5[] = {};String chemistry_1_5no[] = {};String chemistry_1_5_options[] = {};int chemistry_1_5_answers[] = {};
		
		
		////////////////////////////////
		// Further MAths
		///////////////////////////////
		String further_math_1_1[] = {};String further_math_1_1no[] = {};String further_math_1_1_options[] = {};int further_math_1_1_answers[] = {};
		
		
		/////////////////////////////////////
		// Physics
		//////////////////////////////////
		String physics_1_1[] = {"1. Which of the following quantities is not a fundamental quantity?","2.Which is a fundamental unit?","3. Which of the units of the follow quantities are derived? I Area ii thrust iii pressure iv mass","4.Which of the following is the dimension of pressure?","5.Which of the following represents the correct precision if the length of a piece of wire is measured with a metre rule?"};String physics_1_1no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String physics_1_1_options[] = {"A.Electric current","B.Reactance","C.Time","D.none of the above","","A.Ampere","B.Joule","C.Newton","D.Ohm","","A.i, ii, iii and iv","B.i, ii andiii only","C.i, ii, and iv only","D.i, ii and only","","A. ML-1T-2","B.MLT-2","C. ML2T-2","D.ML-3","","A. 35mm","B.35.0mm","C.35.00mm","D.35.01mm",""};int physics_1_1_answers[] = {3,2,3,2,4};
		String physics_1_2[] = {"1.The speed of an object in rectilinear motion can be determined from the","2.Which of the following is not a consequence of a force field","3.The slope of a straight line displacement -time graph indicates","4.Which of the following correctly gives the relationship between linear speed v and angular speed w of a body moving uniformly in a circle of radius r?"};String physics_1_2no[] = {"1 of 5","2 of 5","3 of 5","4 of 5"};String physics_1_2_options[] = {"A.Area under a velocity-time graph","B.Area under a distance- time graph","C.slope of a distance -time graph","D.Slope of a velocity - time graph","","A.Weight","B.Surface tension","C.Magnetic force","D.Electrical force","","A.Distance gravelled","B.uniform velocity","C.uniform speed","D.uniform acceleration","","A. v=wr","B.v=w2r","C.v=wr2","D.v=w/r",""};int physics_1_2_answers[] = {4,3,3,2};
		String physics_1_3[] = {"1.The  motion of a cylindrical object rolling down an inclined place is","2.Which of the following is a displacement","3.Which of the following statement is correct about speed and velocity?","4.The time rate of change of displacement at an instant is?","5.The motion of the prongs of a sounding turning fork is"};String physics_1_3no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String physics_1_3_options[] = {"A.Rotational only","B.Rotational and translational","C. Circular and translational","D.Translational only","","A.10metres south","B.10metres per second","C.10 metres per second south","D.10metres","","A.Speed and velocity have the same unit","B.Speed and velocity are scalar quantity","C.Velocity relates to translational motion while speed relate to circular motion","D.velocity and speed cannot be represented graphically","","A. Uniform velocity","B.instantaneous velocity","C. Uniform acceleration","D.Linear acceleration","","A.Circular","B.Oscillatory","C.Random","D.Rotational",""};int physics_1_3_answers[] = {3,2,2,3,3};
		String physics_1_4[] = {"1.The  motion of a cylindrical object rolling down an inclined place is","2.Which of the following is a displacement","3.Which of the following statement is correct about speed and velocity?","4.The time rate of change of displacement at an instant is?","5.The motion of the prongs of a sounding turning fork is"};String physics_1_4no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String physics_1_4_options[] = {"A.Rotational only","B.Rotational and translational","C. Circular and translational","D.Translational only","","A.10metres south","B.10metres per second","C.10 metres per second south","D.10metres","","A.Speed and velocity have the same unit","B.Speed and velocity are scalar quantity","C.Velocity relates to translational motion while speed relate to circular motion","D.velocity and speed cannot be represented graphically","","A. Uniform velocity","B.instantaneous velocity","C. Uniform acceleration","D.Linear acceleration","","A.Circular","B.Oscillatory","C.Random","D.Rotational",""};int physics_1_4_answers[] = {3,2,2,3,3};
		String physics_1_5[] = {"1.Which of the following units has the same units as the watt?","2.Power is defined as the","3.An object of mass 50kg is released from the height of 2m.  Find the kinetic energy just before it strikes the ground g = 10mg-2","4.If the force and velocity on a system are each simultaneously reduced by half. The power of the system is"};String physics_1_5no[] = {"1 of 5","2 of 5","3 of 5","4 of 5"};String physics_1_5_options[] = {"A.force x time","B.force x distance","C.force x acceleration","D.force x velocity","","A.capacity to exert a force","B.product of force and time","C.Product of force and distance","D.Energy expended per unit time","","A.250J","B.1000J","C.10000J","D.100000J","","A. doubled","B.constant","C.reduce to half","D.reduce to a quarter",""};int physics_1_5_answers[] = {4,4,3,4};
		String physics_1_6[] = {"1.Mercury has an advantage over other liquids as thermometric liquid because it","2.The purpose of the constriction in a clinical thermometer is to","3.When water is heated between 0oC and 4oC, its density","4.An iron rod of length 50m and at a temperature of 600c is heated to 700c, calculate its new length (  = 1.2 x 10-5/k)","5.The expansion of solids can be considered a disadvantage in the"};String physics_1_6no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String physics_1_6_options[] = {"A.has low expansivity","B.has higher conductivity","C.vaporises easily","D.has relatively low freezing point","","A. Prevent the mercury from expanding beyond the bulb","B.prevent the mercury from falling back into the bulb until required","C.Enable the mercury to expand slowly","D.serve as the lower limit of the scale to the read","","A.Increases for a while and then decreases","B.Decreases for a while and then increase","C.Increases","D.decreases","","A.50.006m","B.50.060m","C.51.600m","D.51.200m","","A.Fire alarm system","B.thermostat","C.riveting of steel plates","D.balance wheel of a watch",""};int physics_1_6_answers[] = {3,3,4,2,3};
		String physics_1_7[] = {"1.Mercury has an advantage over other liquids as thermometric liquid because it","2.The purpose of the constriction in a clinical thermometer is to","3.When water is heated between 0oC and 4oC, its density","4.An iron rod of length 50m and at a temperature of 600c is heated to 700c, calculate its new length (  = 1.2 x 10-5/k)"};String physics_1_7no[] = {"1 of 5","2 of 5","3 of 5","4 of 5"};String physics_1_7_options[] = {"A.has low expansivity","B.has higher conductivity","C.vaporises easily","D.has relatively low freezing point","","A. Prevent the mercury from expanding beyond the bulb","B.prevent the mercury from falling back into the bulb until required","C.Enable the mercury to expand slowly","D.serve as the lower limit of the scale to the read","","A.Increases for a while and then decreases","B.Decreases for a while and then increase","C.Increases","D.decreases","","A.50.006m","B.50.060m","C.51.600m","D.51.200m",""};int physics_1_7_answers[] = {3,3,4,2};
		String physics_1_8[] = {"1.Mercury has an advantage over other liquids as thermometric liquid because it","2.The purpose of the constriction in a clinical thermometer is to","3.When water is heated between 0oC and 4oC, its density","4.An iron rod of length 50m and at a temperature of 600c is heated to 700c, calculate its new length (  = 1.2 x 10-5/k)"};String physics_1_8no[] = {"1 of 5","2 of 5","3 of 5","4 of 5"};String physics_1_8_options[] = {"A.has low expansivity","B.has higher conductivity","C.vaporises easily","D.has relatively low freezing point","","A. Prevent the mercury from expanding beyond the bulb","B.prevent the mercury from falling back into the bulb until required","C.Enable the mercury to expand slowly","D.serve as the lower limit of the scale to the read","","A.Increases for a while and then decreases","B.Decreases for a while and then increase","C.Increases","D.decreases","","A.50.006m","B.50.060m","C.51.600m","D.51.200m",""};int physics_1_8_answers[] = {3,3,4,2};
		String physics_1_9[] = {"1.Which of the following elements about the force field is correct?","2.Which of the following features of an electrostatic line of force is not correct","3.The space surrounding a magnet in which the magnetic force is exerted is","4.Which of the following forces does not generate a force  field","5.In the diagram above m1 and m2 represent two bar magnets, the line of force shows that the ends X and Y are respectively physics_1_9_5"};String physics_1_9no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String physics_1_9_options[] = {"A.Electrostatic, gravitational and magnetic forces are always attractive","B.Electric, gravitational and magnetic always obey inverse square laws","C.Field line are real but their corresponding fields are imaginary","D.Field lines and their corresponding fields are both real","","A.It is the imaginary line which a positive charge would describe if it is free to move","B.The tangent drawn at any point on a curved line of force shows the direction of the electric field intensity at that point","C.It is sometimes curved","D.It can cross another line of force in a region of intense electric field.","","A.magnetic pole","B.magnetic point","C. magnetic flux","D.Magnetic field","","A.Electrostatic forces between charges practice","B.Gravitational force of a planet on an object","C.Mutual force between the poles of two bar magnet*C*","D.Frictional force between two bodies","","A. South and north poles","B.north and south poles","C.south and south poles","D.north and north poles",""};int physics_1_9_answers[] = {3,4,4,4,4};
		String physics_1_10[] = {"1.A positively charged glass rod is placed near the cap of a positively charged electroscope.  The divergence of the leaf is objective to","2.Which of the following items can be used to compared the relative magnitudes of electric charge on two bodies","3.When an ebonite rod is rubbed with fur, it has","4.The sign of a charge on a charged glass rod may be determined with","5.In using a gold leaf electroscope to determine the nature of electric change on a body, it is observed that when the charges on the body and electroscope are the same the divergence of the leaf"};String physics_1_10no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String physics_1_10_options[] = {"A.decrease","B.increase","C.remain the same","D.increase and collapse immediately.","","A.Ebonite rod","B.Gold leaf electroscope","C.Proof planes","D.The electrophorus","","A.no charge at all","B.a negative charge","C. a positive charge","D.a negative and positive charge","","A.a charge electroscope","B.an uncharged electroscope","C.a galvanometer","D. an electrometer","","A. decreases","B.increases","C.remains uncharged","D.disappears",""};int physics_1_10_answers[] = {3,3,3,3,3};
		String physics_1_11[] = {"1.Which of the following statement is correct?  physics_1_11_1","2.Which of the following statement about line of force is not correct","3.Which of the following is/are correct about an electric field (i) An electric field is a  force field (ii) An electric field exists in a region around a charge body (iii) The field of  a point charge is uniform everywhere (iv) The field due to a point c","4.A steady current can be generated by","5.Line of force i) Begin and end on equal and opposite electric charge ii) Are in state of tension which cause them to be shorten iii) Attract one another.Which of the statements is/are corrects"};String physics_1_11no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String physics_1_11_options[] = {"A.A is positive and b is negatively charge","B.a and b are positively charge","C.a is negative and b is positively charge","D.a and b are negatively charge","","A.It is an imaginary line","B.It represent the direction of the electric field at that point","C.line of force can cross one another","D.the line are straight if the field is uniform.","","A.I only","B.I,iiand iv","C. iv and iii","D.I and iii","","A.Chemical energy","B.heat energy","C.mechanical energy","D.  sound energy","","A. I only","B.ii only","C.iii only","D.I andii only",""};int physics_1_11_answers[] = {4,4,3,4,2};
		String physics_1_12[] = {"1.Which of the following statement are not correct (i) Molecules are made up of atoms  (ii) the molecules of any one substance are identical (iii) molecules do not move (iv) a force of gravitational attraction exists between a pair of molecules.","2.Which of the following statement is false","3.Which of the statement below is false? In the X nucleus written as X816","4. Which of the following statement is not correct","5.Which of the following is false?","6.One is able to perceive the smell of fold from a distance because:"};String physics_1_12no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5","6 of 5"};String physics_1_12_options[] = {"A.I only","B.ii only","C.iii only","D.iii andi","","A.Brownian motion is a continued random motion","B.Brownian motion cannot be due to convection","C.increase in temperature slows down Brownian motion","D.Diffusion occur in liquid","","A.the nucleus number is 8","B.the atomic number is 8","C. the proton number is 8","D.the nucleus number of 16","","A.An atom is made up of protons","B.The mass of an atom is concentrated in its nucleus","C.An atom is normally negatively  charged.","D.  the mass of a proton is about 1837 times that of an electron","","A. The k.e of solid molecules is greater than that of gaseous molecules.","B. molecules of gases move faster than those of solids","C. molecules of solids are closer than those of liquids","D.the k.e of gaseous molecules is greater than that of liquid molecules","","A. the molecules of food travel by motion","B.the molecules of air are in constant motion","C.  the molecules of food travel by osmosis","D.the ingredients used travel by capillarity",""};int physics_1_12_answers[] = {4,4,2,4,2,3};
		String physics_1_13[] = {"1.The action of blotting paper on ink is due to","2.The meniscus of water in a capillary tube is","3.In which of the following is surface tension important","4. The rise or fall of a liquid in a narrow tube is due to","5.Which of the following factors may be used to explain why a steel pin may float in water?"};String physics_1_13no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String physics_1_13_options[] = {"A.Diffusion","B.osmosis","C.capillarity","D.surface tension","","A.conves","B.concave","C.straight","D.dependent on the bore of the tube","","A.the floating of a ship in water","B.the floating of a dry needle in water","C. the floating of a balloon in air","D.the diffusion of sugar solution across a membrane","","A.the friction between the walls of the tube and the liquid.","B.the viscosity of the liquid","C.the osmosis pressure of the liquid","D. the surface tension of the liquid","","A.  the force of cohesion between the pin and the water","B. the force of adhesion betweens the pin and water","C. the surface tension of the water","D.the force due to capillarity",""};int physics_1_13_answers[] = {4,3,3,4,4};
		String physics_1_14[] = {"1.Solar Energy is energy derived from the:","2.solar energy is used for all of these except","3.which of this is Solar /Energy not responsible for?","4. solar panels use light energy from the sun to generate electricity through"};String physics_1_14no[] = {"1 of 5","2 of 5","3 of 5","4 of 5"};String physics_1_14_options[] = {"A.moon","B.sun","C.solar system","D.planets","","A.water treatment","B.cooking","C.powering aircrafts","D.reduce weight","","A.Climate","B.Weather","C. Viscosity","D.Life on earth","","A.stabilizing effect","B.electrolytic effect","C.Photovoltaic effect","D. photosynthetic effect",""};int physics_1_14_answers[] = {3,4,4,4};
		String physics_1_15[] = {"1.Which of the following is not a vector?","2.The symbol for the S.I unit of volume is","3. Force is an example of I vector  II scalar  III vector and scalar. Which is/are correct statement"};String physics_1_15no[] = {"1 of 5","2 of 5","3 of 5"};String physics_1_15_options[] = {"A.velocity","B.energy","C.acceleration","D.momentum","","A.v","B.m3","C.m2","D.n","","A.I only","B.ii only","C.iii only","D. none of the above",""};int physics_1_15_answers[] = {3,3,2};
		String physics_1_16[] = {"1.A car covers a distance of 60km in half an hour.  What is the average speed of the car?","2.A body travels from rest and accelerates uniformly to a speed of 120km per hour in 10seconds.  What is the distance covered","3.Which of the following are derived units","4.The S.I unit for (i) acceleration = ms2  (ii)density = kgm-3 (iii) momentum (i.e mv = Ns-1 (iv) pressure (i.e force/area) = Nm-2. Which of the above statement is/are correct.","5. The above diagram shows a velocity time graph.  Calculate the total distance covered by the vehicle.  physics_1_16_5"};String physics_1_16no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String physics_1_16_options[] = {"A.60km/hr","B.30km/h","C.120km/h","D.1/6km/h","","A.1/3km","B.1/6km","C.600km","D.1000km","","A.meter","B.kilogram","C. newton","D.kelvin","","A.I only","B.i and ii only","C.ii and iii only","D.ii and iv only","","A.100m","B.200m","C.225m","D.300m",""};int physics_1_16_answers[] = {4,3,4,4,4};
		String physics_1_17[] = {"1.A boat travels from west to east at 40km h-1 at right angle to the bank of a river flowing north to south at 30km h-1.  Calculate the resultant velocity of the boat","2.Which of the following sets consists entirely of scalar quantities?","3.A motor cyclist passing a road junction moves due west for 8s at a uniform speed of 5ms-1.  He then moves due north for another 6s with the same speed. At the end of 6s his displacement from the road junction is 50m in the direction.","4.Two forces whose resultant is 100N are perpendicular to each other.  If one of them makes and angle of 600 with the resultant.  Calculate its magnitude (sin 600 = 0.8660, cos 600 =0.5000).","5.A lorry travels 10km northwards, 4km eastwards, 6km southwards and 4km westwards to arrive at a point T.  What is the total displacement?"};String physics_1_17no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String physics_1_17_options[] = {"A. 70km h-1","B. 50km h-1","C.40km h-1","D.10km h-1","","A.Impulse, mass and magnetic flux","B.Speed, momentum and distance","C.Displacement, electric field and energy","D.Pressure, work, and electric potential","","A.N530E","B.N370E","C.N530W","D.N370W","","A.300.0N","B.173.3N","C.115.5N","D. 50.0N","","A. 6km east","B.4km north","C.6km north","D.4km east",""};int physics_1_17_answers[] = {3,4,2,4,3};
		String physics_1_18[] = {};String physics_1_18no[] = {};String physics_1_18_options[] = {};int physics_1_18_answers[] = {};
		String physics_1_19[] = {"1.Molecules can exist as all of these except","2.these factors affect Diffusion except","3.A motor cyclist passing a road junction moves due west for 8s at a uniform speed of 5ms-1.  He then moves due north for another 6s with the same speed. At the end of 6s his displacement from the road junction is 50m in the direction.","4.if the molecules can freely move around and between each other","5.If the motion is fixed around a point in space, it is called a"};String physics_1_19no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String physics_1_19_options[] = {"A.Solid","B.gases","C.saw","D.liquid","","A.Density of the substance","B.temperature","C.mass","D.colour","","A.a gas","B.solid","C.soluble","D.liquid","","A.gas","B.solid","C.soluble","D. liquid","","A.gas","B.solid","C.soluble","D.liquid",""};int physics_1_19_answers[] = {4,4,2,4,3};
		String physics_1_20[] = {"1.A force of 250N is applied at the free end of a 25cm long spanner to remove a nut.  Calculate the moment of the force.  Calculate the moment of the force.","2.these factors affect Diffusion except","3.if the molecules can freely move around and between each other"};String physics_1_20no[] = {"1 of 5","2 of 5","3 of 5"};String physics_1_20_options[] = {"A.62.5Nm","B.40.0Nm","C.34.2Nm","D.10.0Nm","","A.Density of the substance","B.temperature","C.mass","D.colour","","A.gas","B.solid","C.soluble","D. liquid",""};int physics_1_20_answers[] = {2,4,4};
		
		///////////////////////////////////////
		// Accounting
		//////////////////////////////////////
		String accounting_1_1[] = {"1. Accounting equation is","2. The art of recording transactions in books of account is known as","3. The art of collecting, recording, presenting and interpreting accounting data is","4. Financial accounting information is for","5. Which of the following is not an external user of accounting information?"};String accounting_1_1no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String accounting_1_1_options[] = {"A.  capital less current assets amount to liabilities","B.Assets less liabilities amount to capital","C.fixed assets and current liabilities amount to capital","D. capital and assets amount  to liabilities.","","A.debiting","B.  book-keeping","C. auditing","D.crediting","","A. cost accounting","B. management accounting","C. financial accounting","D. data processing.","","A.  internal use only","B. external use only","C.  business use only","D. internal and external use.","","A. management","B. creditors","C.shareholders","D. government",""};int accounting_1_1_answers[] = {4,3,4,4,2};
		String accounting_1_2[] = {"1.  ____________ is the state of mind which as regard to all and none other than those considerations related to the task in hand.","2.Threats to integrity, objective and independence consist of the following except _________","3. Exceptions to the rule confidentiality are","4.Sanction which are commonly impose by the institute on members for misconduct include all except one of these","5. A person or firm possessing special skill or knowledge in a field other than accounting is called __________"};String accounting_1_2no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String accounting_1_2_options[] = {"A.  integrity","B.objectivity","C.Independence","D. Honesty","","A.Intimidation threat","B.  integrity threat","C.Advocacy threat","D.Familiarity threat","","A. Obligation and voluntary disclosure","B. enforcement and compelling disclosure","C. Necessity and compulsory disclosure","D.voluntary and enforcement disclosure","","A. reprimand","B. fine","C.  payment of cost","D. withdrawer of practicing rights","","A. professional","B.consultant","C.an adviser","D. specialist",""};int accounting_1_2_answers[] = {3,3,2,4,4};
		String accounting_1_3[] = {"1.  Which of the following is not an accounting concept?","2.A trader paid N15,000 on rent for 15 months but charged N12,000 to the profit and loss account for the year. This is a concept of","3.  Accountants do not  count chickens before they are hatched","4.The Accountant thinks the investment in the books are worthless. This is","5.Profits are recognized when goods are sold. What concept is this?"};String accounting_1_3no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String accounting_1_3_options[] = {"A. Entity","B.Going concern","C.consistency","D.historical cost","","A.accrual","B.  materiality","C.prudence","D.going since.","","A. accrual","B. materiality","C. realisation","D.going-concern","","A. consistency concept","B.objectivity concept","C. conservation concept","D.money measurement concept","","A. realization","B.matching","C.periodicity","D. going concern",""};int accounting_1_3_answers[] = {4,3,4,4,2};
		String accounting_1_4[] = {"1.  Which of the following is not an accounting concept?","2.A trader paid N15,000 on rent for 15 months but charged N12,000 to the profit and loss account for the year. This is a concept of","3.  Accountants do not  count chickens before they are hatched","4.The Accountant thinks the investment in the books are worthless. This is","5.Profits are recognized when goods are sold. What concept is this?"};String accounting_1_4no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String accounting_1_4_options[] = {"A. Entity","B.Going concern","C.consistency","D.historical cost","","A.accrual","B.  materiality","C.prudence","D.going since.","","A. accrual","B. materiality","C. realisation","D.going-concern","","A. consistency concept","B.objectivity concept","C. conservation concept","D.money measurement concept","","A. realization","B.matching","C.periodicity","D. going concern",""};int accounting_1_4_answers[] = {4,3,4,4,2};
		String accounting_1_5[] = {"1.  A subsidiary record is a book","2.The total of the purchases day book is posted to the purchases account in the","3.  Babson enterprises bought 40 pairs of shoes at N1,000 each from Gay shoe factory on credit. This transaction will first be recorded by Babson in the","4.A sales journal is used to record","5.A subsidiary record is a book"};String accounting_1_5no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String accounting_1_5_options[] = {"A. found in the ledger","B.of subsidiary entries","C.of original entry","D.containing the trial accounts.","","A. purchases ledger","B.  general ledger","C.private ledger","D.nominal ledger.","","A.ledger","B. Principal journal","C. cash book","D.purchases day book","","A. sales expenses","B.cash sales","C. sales return","D.credit sales","","A. of original entry","B.containing the ledgers","C.found in the journal","D. containing the balance sheet.",""};int accounting_1_5_answers[] = {4,3,5,5,2};
		String accounting_1_6[] = {"1.  Which of the following is a classified form contains a permanent record of all transactions.","2.Trade discount is","3. The fixed amount of money set aside for petty expenses is called","4.Which of the following is an advantage of the imprest system?","5.Where a proprietor withdraws cash from bank for office use, the entries would be"};String accounting_1_6no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String accounting_1_6_options[] = {"A. journal","B.sales day book","C.ledger","D.cash book","","A. allowance for prompt payment","B.  allowance for damaged good","C.allowance of price list","D.discount on creditors.","","A.accumulated fund","B. float","C.ordinary fund","D. imprest receipt","","A. making high profits in the business","B.rewarding the imprest holder","C. easy preparation of the final accounts","D.meeting small items of expenditure.","","A. credit cash account, debit bank account","B.debit cash account, credit bank account","C.debit office account, credit bank account","D. debit drawing account, credit bank account.",""};int accounting_1_6_answers[] = {4,4,3,4,3};
		String accounting_1_7[] = {"1.  Which of the following may have been recorded in the Cash Book and fail to appear in the Bank statement?","2.Which of the following will not affect the agreement of the cash book balance?","3.Which of the following is not a cause of discrepancy between cash book and bank statement balances?","4.Which of the following are on the bank statement before reconciliation?  (i) standing order  (ii) payment to creditor (iii) receipt from a debtor  (iv) uncredited cheque  (v) unpresented cheques.","5.Which of the following does not appear in a bank statement?"};String accounting_1_7no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String accounting_1_7_options[] = {"A. Bank charges and commission","B.cheques issued, presented and cashed","C.bank lodgements","D.payments made by the bank on a standing order.","","A. bank charges","B. standing order","C.dishonoured cheques","D. Cash payment.","","A.deposits in transit","B.paid cheques","C.dishonoured cheques","D. standing order","","A. i, ii and iii only","B.I and v only","C. I and ii only","D.I and iv only","","A. Dividend received","B.Bank charges","C.debit office account, credit bank account","D. Uncredited cheques",""};int accounting_1_7_answers[] = {4,4,3,2,4};
		String accounting_1_8[] = {"1.  Which of the following accounts belongs to the private ledger?","2.Which of the following account belongs to the personal ledger?","3.Which of the following is a classified form, contains a permanent record of all transactions?","4.Which of the following belongs to the nominal ledger?","5.Which of the following belongs to the sales ledger"};String accounting_1_8no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String accounting_1_8_options[] = {"A. drawings account","B.premises account","C.bank account","D.debtors' account.","","A. purchases account","B.capital account","C.machinery account","D. V. okafor's account","","A.sales day book","B.cash book","C.ledger","D.  journal.","","A. fixers account","B. salaries account","C.stock account","D.debtor's account.","","A. motor van disposal account","B.bank account","C.debtors account","D. rent and rates account.",""};int accounting_1_8_answers[] = {2,4,4,3,4};
		String accounting_1_9[] = {"1.  The purchase of a typewriter for the office was debited to purchase account. This is an error of","2.Garbon Ltd failed to record N5,000 wages. The error committed is that of","3.A trial balance is a proof of","4.A sales of N2,570 was recorded in the day book as N2,750. The error commited was","5.Which of the following errors affects the trial balance ?"};String accounting_1_9no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String accounting_1_9_options[] = {"A. commisssion","B.ommission","C.original entry","D.principle","","A.omission","B.principle","C.commission","D.compensation","","A. accurate narration of the journal entries","B. accuracy of the credit entries.","C.arithmetical accuracy of posting only","D. accuracy of the debt entries","","A. compensating error","B.error of omission","C.error of original entry","D.error of principle","","A.commission","B.single entry","C.principle","D.omission",""};int accounting_1_9_answers[] = {4,2,4,4,3};
		String accounting_1_10[] = {"1.  Carriage inwards is shown on the","2.Commission received is entered on the","3.Which  of the following is not a liability?","4.Which of the following is not a balance sheet item?","5.Discount allowed is charged to"};String accounting_1_10no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String accounting_1_10_options[] = {"A. debit side of the profit and loss account","B.credit side of the trading account","C.debit side of the trading account","D.credit side of the profit and loss account","","A.credit side of the profit and loss account","B.credit side of the trading account","C.credit side of the cash book","D.debit side of the profit and loss account.","","A. accrued wages","B. credit","C.prepayment","D. insurance due but unpaid.","","A.outstanding wages","B.dividend received","C.prepaid rent","D.provision for discount","","A.trading account","B.balance sheet","C.profit and loss account","D.current account",""};int accounting_1_10_answers[] = {4,2,4,3,4};
		String accounting_1_11[] = {"1.  Carriage inwards is shown on the","2.Commission received is entered on the","3.Which  of the following is not a liability?","4.Which of the following is not a balance sheet item?","5.Discount allowed is charged to"};String accounting_1_11no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String accounting_1_11_options[] = {"A. debit side of the profit and loss account","B.credit side of the trading account","C.debit side of the trading account","D.credit side of the profit and loss account","","A.credit side of the profit and loss account","B.credit side of the trading account","C.credit side of the cash book","D.debit side of the profit and loss account.","","A. accrued wages","B. credit","C.prepayment","D. insurance due but unpaid.","","A.outstanding wages","B.dividend received","C.prepaid rent","D.provision for discount","","A.trading account","B.balance sheet","C.profit and loss account","D.current account",""};int accounting_1_11_answers[] = {4,2,4,3,4};
		String accounting_1_12[] = {"1.  Carriage inwards is shown on the","2.Commission received is entered on the","3.Which  of the following is not a liability?","4.Which of the following is not a balance sheet item?","5.Discount allowed is charged to"};String accounting_1_12no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String accounting_1_12_options[] = {"A. debit side of the profit and loss account","B.credit side of the trading account","C.debit side of the trading account","D.credit side of the profit and loss account","","A.credit side of the profit and loss account","B.credit side of the trading account","C.credit side of the cash book","D.debit side of the profit and loss account.","","A. accrued wages","B. credit","C.prepayment","D. insurance due but unpaid.","","A.outstanding wages","B.dividend received","C.prepaid rent","D.provision for discount","","A.trading account","B.balance sheet","C.profit and loss account","D.current account",""};int accounting_1_12_answers[] = {4,2,4,3,4};
		String accounting_1_13[] = {"1.  Carriage inwards is shown on the","2.Commission received is entered on the","3.Which  of the following is not a liability?","4.Which of the following is not a balance sheet item?","5.Discount allowed is charged to"};String accounting_1_13no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String accounting_1_13_options[] = {"A. debit side of the profit and loss account","B.credit side of the trading account","C.debit side of the trading account","D.credit side of the profit and loss account","","A.credit side of the profit and loss account","B.credit side of the trading account","C.credit side of the cash book","D.debit side of the profit and loss account.","","A. accrued wages","B. credit","C.prepayment","D. insurance due but unpaid.","","A.outstanding wages","B.dividend received","C.prepaid rent","D.provision for discount","","A.trading account","B.balance sheet","C.profit and loss account","D.current account",""};int accounting_1_13_answers[] = {4,2,4,3,4};
		String accounting_1_14[] = {"1. One of the advantages of control account is that","2.Which of the following is not on Debtor's Ledger control Account","3.Which of the following is not contained in the sales ledger control account?","4.Which of the following is not a debit entry on a sales ledger control account?","5.Items on the left-hand side of a total debtors account are debits in the"};String accounting_1_14no[] = {"1 of 5","2 of 5","3 of 5","4 of 5","5 of 5"};String accounting_1_14_options[] = {"A. all errors are localized thus reducing delays in balancing accounts","B.all errors are generalized  thus reducing delays in balancing accounts","C.book keepers cannot make mistakes","D.all errors are posted into the accounts.","","A.credit sales","B.cash sales","C. bills receivable","D.dishonored cheque.","","A. receipt from debtors","B.returns inwards","C.return outwards","D. dishonoured cheque.","","A.goods sold","B.cash received","C.dishonoured cheque","D.interest charges.","","A.journal","B.general ledger","C.sales ledger","D.purchases  ledger",""};int accounting_1_14_answers[] = {2,3,4,3,3};
		
		
		
		
		
		@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.questions);
		getActionBar().hide();
		Intent in = getIntent();
		subject = in.getStringExtra("subject");
		chapter = in.getStringExtra("chapter");
		Toast.makeText(Questions1.this, String.valueOf(chapter), 5000).show();
			initialize();
		setOnClickListener();
		questiontypevalue.setText(subject);
		set(subject);
        
    questionradioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() 
    {
        public void onCheckedChanged(RadioGroup group, int checkedId) {
        	if(chapter.equals("1") && subject.equals("biology")){
        	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+biology_1_1_options[check*5+biology_1_1_answers[check]-1]);
        	}
        	else if(chapter.equals("2") && subject.equals("biology")){
        	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+biology_1_2_options[check*5+biology_1_2_answers[check]-1]);
        	}
        	else if(chapter.equals("3") && subject.equals("biology")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+biology_1_3_options[check*5+biology_1_3_answers[check]-1]);
            	}
        	else if(chapter.equals("4") && subject.equals("biology")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+biology_1_4_options[check*5+biology_1_4_answers[check]-1]);
            	}
        	else if(chapter.equals("5") && subject.equals("biology")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+biology_1_5_options[check*5+biology_1_5_answers[check]-1]);
            	}
        	else if(chapter.equals("6") && subject.equals("biology")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+biology_1_6_options[check*5+biology_1_6_answers[check]-1]);
            	}
        	else if(chapter.equals("7") && subject.equals("biology")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+biology_1_7_options[check*5+biology_1_7_answers[check]-1]);
            	}
        	else if(chapter.equals("8") && subject.equals("biology")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+biology_1_8_options[check*5+biology_1_8_answers[check]-1]);
            	}
        	else if(chapter.equals("9") && subject.equals("biology")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+biology_1_9_options[check*5+biology_1_9_answers[check]-1]);
            	}
        	else if(chapter.equals("10") && subject.equals("biology")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+biology_1_10_options[check*5+biology_1_10_answers[check]-1]);
            	}
        	///////////////////////
        	else if(chapter.equals("1") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_1_options[check*5+commerce_1_1_answers[check]-1]);
            	}
        	else if(chapter.equals("2") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_2_options[check*5+commerce_1_2_answers[check]-1]);
            	}
        	else if(chapter.equals("3") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_3_options[check*5+commerce_1_3_answers[check]-1]);
            	}
        	else if(chapter.equals("4") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_4_options[check*5+commerce_1_4_answers[check]-1]);
            	}
        	else if(chapter.equals("5") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_5_options[check*5+commerce_1_5_answers[check]-1]);
            	}
        	else if(chapter.equals("6") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_6_options[check*5+commerce_1_6_answers[check]-1]);
            	}
        	else if(chapter.equals("7") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_7_options[check*5+commerce_1_7_answers[check]-1]);
            	}
        	else if(chapter.equals("8") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_8_options[check*5+commerce_1_8_answers[check]-1]);
            	}
        	else if(chapter.equals("9") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_9_options[check*5+commerce_1_9_answers[check]-1]);
            	}
        	else if(chapter.equals("10") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_10_options[check*5+commerce_1_10_answers[check]-1]);
            	}
        	else if(chapter.equals("11") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_11_options[check*5+commerce_1_11_answers[check]-1]);
            	}
        	else if(chapter.equals("12") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_12_options[check*5+commerce_1_12_answers[check]-1]);
            	}
        	else if(chapter.equals("13") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_13_options[check*5+commerce_1_13_answers[check]-1]);
            	}
        	else if(chapter.equals("14") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_14_options[check*5+commerce_1_14_answers[check]-1]);
            	}
        	else if(chapter.equals("15") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_15_options[check*5+commerce_1_15_answers[check]-1]);
            	}
        	else if(chapter.equals("16") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_16_options[check*5+commerce_1_16_answers[check]-1]);
            	}
        	else if(chapter.equals("17") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_17_options[check*5+commerce_1_17_answers[check]-1]);
            	}
        	else if(chapter.equals("18") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_18_options[check*5+commerce_1_18_answers[check]-1]);
            	}
        	else if(chapter.equals("19") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_19_options[check*5+commerce_1_19_answers[check]-1]);
            	}
        	else if(chapter.equals("20") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_20_options[check*5+commerce_1_20_answers[check]-1]);
            	}
        	else if(chapter.equals("21") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_21_options[check*5+commerce_1_21_answers[check]-1]);
            	}
        	else if(chapter.equals("22") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_22_options[check*5+commerce_1_22_answers[check]-1]);
            	}
        	else if(chapter.equals("23") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_23_options[check*5+commerce_1_23_answers[check]-1]);
            	}
        	else if(chapter.equals("24") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_24_options[check*5+commerce_1_24_answers[check]-1]);
            	}
        	else if(chapter.equals("25") && subject.equals("commerce")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+commerce_1_24_options[check*5+commerce_1_24_answers[check]-1]);
            	}
        	else if(subject.equals("crs")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+crs_1_1_options[check*5+crs_1_1_answers[check]-1]);
            	}
        	else if(chapter.equals("1") && subject.equals("economics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+economics_1_1_options[check*5+economics_1_1_answers[check]-1]);
            	}
        	else if(chapter.equals("2") && subject.equals("economics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+economics_1_2_options[check*5+economics_1_2_answers[check]-1]);
            	}
        	else if(chapter.equals("3") && subject.equals("economics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+economics_1_3_options[check*5+economics_1_3_answers[check]-1]);
            	}
        	else if(chapter.equals("4") && subject.equals("economics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+economics_1_4_options[check*5+economics_1_4_answers[check]-1]);
            	}
        	else if(chapter.equals("5") && subject.equals("economics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+economics_1_5_options[check*5+economics_1_5_answers[check]-1]);
            	}
        	else if(chapter.equals("6") && subject.equals("economics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+economics_1_6_options[check*5+economics_1_6_answers[check]-1]);
            	}
        	else if(chapter.equals("7") && subject.equals("economics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+economics_1_7_options[check*5+economics_1_7_answers[check]-1]);
            	}
        	else if(chapter.equals("8") && subject.equals("economics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+economics_1_8_options[check*5+economics_1_8_answers[check]-1]);
            	}
        	else if(chapter.equals("9") && subject.equals("economics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+economics_1_9_options[check*5+economics_1_9_answers[check]-1]);
            	}
        	else if(chapter.equals("10") && subject.equals("economics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+economics_1_10_options[check*5+economics_1_10_answers[check]-1]);
            	}
        	else if(chapter.equals("11") && subject.equals("economics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+economics_1_11_options[check*5+economics_1_11_answers[check]-1]);
            	}
        	else if(chapter.equals("12") && subject.equals("economics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+economics_1_12_options[check*5+economics_1_12_answers[check]-1]);
            	}
        	else if(chapter.equals("13") && subject.equals("economics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+economics_1_13_options[check*5+economics_1_13_answers[check]-1]);
            	}
        	else if(chapter.equals("14") && subject.equals("economics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+economics_1_14_options[check*5+economics_1_14_answers[check]-1]);
            	}
        	else if(chapter.equals("15") && subject.equals("economics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+economics_1_15_options[check*5+economics_1_15_answers[check]-1]);
            	}
        	else if(chapter.equals("16") && subject.equals("economics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+economics_1_16_options[check*5+economics_1_16_answers[check]-1]);
            	}
        	else if(chapter.equals("17") && subject.equals("economics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+economics_1_17_options[check*5+economics_1_17_answers[check]-1]);
            	}
        	else if(chapter.equals("18") && subject.equals("economics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+economics_1_18_options[check*5+economics_1_18_answers[check]-1]);
            	}
        	else if(chapter.equals("19") && subject.equals("economics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+economics_1_19_options[check*5+economics_1_19_answers[check]-1]);
            	}
        	else if(subject.equals("englishlanguage")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+english_language_1_1_options[check*5+english_language_1_1_answers[check]-1]);
            	}
        	// geography
        	else if(chapter.equals("1") && subject.equals("geography")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+geography_1_1_options[check*5+geography_1_1_answers[check]-1]);
            	}
        	else if(chapter.equals("2") && subject.equals("geography")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+geography_1_2_options[check*5+geography_1_2_answers[check]-1]);
            	}
        	else if(chapter.equals("3") && subject.equals("geography")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+geography_1_3_options[check*5+geography_1_3_answers[check]-1]);
            	}
        	else if(chapter.equals("4") && subject.equals("geography")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+geography_1_4_options[check*5+geography_1_4_answers[check]-1]);
            	}
        	else if(chapter.equals("5") && subject.equals("geography")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+geography_1_5_options[check*5+geography_1_5_answers[check]-1]);
            	}
        	else if(chapter.equals("6") && subject.equals("geography")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+geography_1_6_options[check*5+geography_1_6_answers[check]-1]);
            	}
        	else if(chapter.equals("7") && subject.equals("geography")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+geography_1_7_options[check*5+geography_1_7_answers[check]-1]);
            	}
        	else if(chapter.equals("8") && subject.equals("geography")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+geography_1_8_options[check*5+geography_1_8_answers[check]-1]);
            	}
        	else if(chapter.equals("9") && subject.equals("geography")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+geography_1_9_options[check*5+geography_1_9_answers[check]-1]);
            	}
        	else if(chapter.equals("10") && subject.equals("geography")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+geography_1_10_options[check*5+geography_1_10_answers[check]-1]);
            	}
        	else if(chapter.equals("11") && subject.equals("geography")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+geography_1_11_options[check*5+geography_1_11_answers[check]-1]);
            	}
        	else if(chapter.equals("12") && subject.equals("geography")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+geography_1_12_options[check*5+geography_1_12_answers[check]-1]);
            	}
        	else if(chapter.equals("13") && subject.equals("geography")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+geography_1_13_options[check*5+geography_1_13_answers[check]-1]);
            	}
        	else if(chapter.equals("14") && subject.equals("geography")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+geography_1_14_options[check*5+geography_1_14_answers[check]-1]);
            	}
        	else if(chapter.equals("15") && subject.equals("geography")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+geography_1_15_options[check*5+geography_1_15_answers[check]-1]);
            	}
        	else if(chapter.equals("16") && subject.equals("geography")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+geography_1_16_options[check*5+geography_1_16_answers[check]-1]);
            	}
        	else if(chapter.equals("17") && subject.equals("geography")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+geography_1_17_options[check*5+geography_1_17_answers[check]-1]);
            	}
        	else if(chapter.equals("18") && subject.equals("geography")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+geography_1_18_options[check*5+geography_1_18_answers[check]-1]);
            	}
        	else if(chapter.equals("19") && subject.equals("geography")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+geography_1_19_options[check*5+geography_1_19_answers[check]-1]);
            	}
        	//government
        	else if(chapter.equals("1") && subject.equals("government")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+government_1_1_options[check*5+government_1_1_answers[check]-1]);
            	}
        	else if(chapter.equals("2") && subject.equals("government")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+government_1_2_options[check*5+government_1_2_answers[check]-1]);
            	}
        	else if(chapter.equals("3") && subject.equals("government")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+government_1_3_options[check*5+government_1_3_answers[check]-1]);
            	}
        	else if(chapter.equals("4") && subject.equals("government")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+government_1_4_options[check*5+government_1_4_answers[check]-1]);
            	}
        	else if(chapter.equals("5") && subject.equals("government")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+government_1_5_options[check*5+government_1_5_answers[check]-1]);
            	}
        	else if(chapter.equals("6") && subject.equals("government")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+government_1_6_options[check*5+government_1_6_answers[check]-1]);
            	}
        	else if(chapter.equals("7") && subject.equals("government")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+government_1_7_options[check*5+government_1_7_answers[check]-1]);
            	}
        	else if(chapter.equals("8") && subject.equals("government")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+government_1_8_options[check*5+government_1_8_answers[check]-1]);
            	}
        	else if(chapter.equals("9") && subject.equals("government")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+government_1_9_options[check*5+government_1_9_answers[check]-1]);
            	}
        	else if(chapter.equals("10") && subject.equals("government")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+government_1_10_options[check*5+government_1_10_answers[check]-1]);
            	}
        	else if(chapter.equals("11") && subject.equals("government")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+government_1_11_options[check*5+government_1_11_answers[check]-1]);
            	}
        	else if(chapter.equals("12") && subject.equals("government")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+government_1_12_options[check*5+government_1_12_answers[check]-1]);
            	}
        	// history
        	else if(chapter.equals("1") && subject.equals("history")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+history_1_1_options[check*5+history_1_1_answers[check]-1]);
            	}
        	else if(chapter.equals("2") && subject.equals("history")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+history_1_2_options[check*5+history_1_2_answers[check]-1]);
            	}
        	else if(chapter.equals("3") && subject.equals("history")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+history_1_3_options[check*5+history_1_3_answers[check]-1]);
            	}
        	else if(chapter.equals("4") && subject.equals("history")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+history_1_4_options[check*5+history_1_4_answers[check]-1]);
            	}
        	else if(chapter.equals("5") && subject.equals("history")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+history_1_5_options[check*5+history_1_5_answers[check]-1]);
            	}
        	else if(chapter.equals("6") && subject.equals("history")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+history_1_6_options[check*5+history_1_6_answers[check]-1]);
            	}
        	else if(chapter.equals("7") && subject.equals("history")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+history_1_7_options[check*5+history_1_7_answers[check]-1]);
            	}
        	else if(chapter.equals("8") && subject.equals("history")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+history_1_8_options[check*5+history_1_8_answers[check]-1]);
            	}
        	else if(chapter.equals("9") && subject.equals("history")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+history_1_9_options[check*5+history_1_9_answers[check]-1]);
            	}
        	else if(chapter.equals("10") && subject.equals("history")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+history_1_10_options[check*5+history_1_10_answers[check]-1]);
            	}
        	else if(chapter.equals("11") && subject.equals("history")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+history_1_11_options[check*5+history_1_11_answers[check]-1]);
            	}
        	else if(chapter.equals("12") && subject.equals("history")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+history_1_12_options[check*5+history_1_12_answers[check]-1]);
            	}
        	else if(chapter.equals("13") && subject.equals("history")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+history_1_13_options[check*5+history_1_13_answers[check]-1]);
            	}
        	else if(chapter.equals("14") && subject.equals("history")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+history_1_14_options[check*5+history_1_14_answers[check]-1]);
            	}
        	else if(chapter.equals("15") && subject.equals("history")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+history_1_15_options[check*5+history_1_15_answers[check]-1]);
            	}
        	//irk
        	else if(subject.equals("irk")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+ irk_1_1_options[check*5+irk_1_1_answers[check]-1]);
            	}
        	//mathematics

        	else if(subject.equals("mathematics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+ mathematics_1_1_options[check*5+irk_1_1_answers[check]-1]);
            	}
        	//yoruba

        	else if(subject.equals("yoruba")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+ yoruba_1_1_options[check*5+ yoruba_1_1_answers[check]-1]);
            	}
        	///agriculture
        	else if(subject.equals("agriculture")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+ agriculture_1_1_options[check*5+ agriculture_1_1_answers[check]-1]);
            	}
        	///chemistry
        	else if(subject.equals("chemistry")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+ chemistry_1_1_options[check*5+ chemistry_1_1_answers[check]-1]);
            	}
        	
        	///furthermaths
        	else if(subject.equals("furthermath")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+ further_math_1_1_options[check*5+ further_math_1_1_answers[check]-1]);
            	}
        	
        	//physics
        	
        	else if(chapter.equals("1") && subject.equals("physics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+physics_1_1_options[check*5+physics_1_1_answers[check]-1]);
            	}
        	else if(chapter.equals("2") && subject.equals("physics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+physics_1_2_options[check*5+physics_1_2_answers[check]-1]);
            	}
        	else if(chapter.equals("3") && subject.equals("physics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+physics_1_3_options[check*5+physics_1_3_answers[check]-1]);
            	}
        	else if(chapter.equals("4") && subject.equals("physics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+physics_1_4_options[check*5+physics_1_4_answers[check]-1]);
            	}
        	else if(chapter.equals("5") && subject.equals("physics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+physics_1_5_options[check*5+physics_1_5_answers[check]-1]);
            	}
        	else if(chapter.equals("6") && subject.equals("physics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+physics_1_6_options[check*5+physics_1_6_answers[check]-1]);
            	}
        	else if(chapter.equals("7") && subject.equals("physics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+physics_1_7_options[check*5+physics_1_7_answers[check]-1]);
            	}
        	else if(chapter.equals("8") && subject.equals("physics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+physics_1_8_options[check*5+physics_1_8_answers[check]-1]);
            	}
        	else if(chapter.equals("9") && subject.equals("physics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+physics_1_9_options[check*5+physics_1_9_answers[check]-1]);
            	}
        	else if(chapter.equals("10") && subject.equals("physics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+physics_1_10_options[check*5+physics_1_10_answers[check]-1]);
            	}
        	else if(chapter.equals("11") && subject.equals("physics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+physics_1_11_options[check*5+physics_1_11_answers[check]-1]);
            	}
        	else if(chapter.equals("12") && subject.equals("physics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+physics_1_12_options[check*5+physics_1_12_answers[check]-1]);
            	}
        	else if(chapter.equals("13") && subject.equals("physics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+physics_1_13_options[check*5+physics_1_13_answers[check]-1]);
            	}
        	else if(chapter.equals("14") && subject.equals("physics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+physics_1_14_options[check*5+physics_1_14_answers[check]-1]);
            	}
        	else if(chapter.equals("15") && subject.equals("physics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+physics_1_15_options[check*5+physics_1_15_answers[check]-1]);
            	}
        	else if(chapter.equals("16") && subject.equals("physics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+physics_1_16_options[check*5+physics_1_16_answers[check]-1]);
            	}
        	else if(chapter.equals("17") && subject.equals("physics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+physics_1_17_options[check*5+physics_1_17_answers[check]-1]);
            	}
        	else if(chapter.equals("18") && subject.equals("physics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+physics_1_18_options[check*5+physics_1_18_answers[check]-1]);
            	}
        	else if(chapter.equals("19") && subject.equals("physics")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+physics_1_19_options[check*5+physics_1_19_answers[check]-1]);
            	}
        	//accounting
        	
        	else if(chapter.equals("1") && subject.equals("accounting")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+accounting_1_1_options[check*5+accounting_1_1_answers[check]-1]);
            	}
        	else if(chapter.equals("2") && subject.equals("accounting")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+accounting_1_2_options[check*5+accounting_1_2_answers[check]-1]);
            	}
        	else if(chapter.equals("3") && subject.equals("accounting")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+accounting_1_3_options[check*5+accounting_1_3_answers[check]-1]);
            	}
        	else if(chapter.equals("4") && subject.equals("accounting")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+accounting_1_4_options[check*5+accounting_1_4_answers[check]-1]);
            	}
        	else if(chapter.equals("5") && subject.equals("accounting")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+accounting_1_5_options[check*5+accounting_1_5_answers[check]-1]);
            	}
        	else if(chapter.equals("6") && subject.equals("accounting")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+accounting_1_6_options[check*5+accounting_1_6_answers[check]-1]);
            	}
        	else if(chapter.equals("7") && subject.equals("accounting")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+accounting_1_7_options[check*5+accounting_1_7_answers[check]-1]);
            	}
        	else if(chapter.equals("8") && subject.equals("accounting")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+accounting_1_8_options[check*5+accounting_1_8_answers[check]-1]);
            	}
        	else if(chapter.equals("9") && subject.equals("accounting")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+accounting_1_9_options[check*5+accounting_1_9_answers[check]-1]);
            	}
        	else if(chapter.equals("10") && subject.equals("accounting")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+accounting_1_10_options[check*5+accounting_1_10_answers[check]-1]);
            	}
        	else if(chapter.equals("11") && subject.equals("accounting")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+accounting_1_11_options[check*5+accounting_1_11_answers[check]-1]);
            	}
        	else if(chapter.equals("12") && subject.equals("accounting")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+accounting_1_12_options[check*5+accounting_1_12_answers[check]-1]);
            	}
        	else if(chapter.equals("13") && subject.equals("accounting")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+accounting_1_13_options[check*5+accounting_1_13_answers[check]-1]);
            	}
        	else if(chapter.equals("14") && subject.equals("accounting")){
            	answertextview.setText("Answer is :  \n \t\t\t\t\t\t"+accounting_1_14_options[check*5+accounting_1_14_answers[check]-1]);
            	}
        	
        	
        }
    });	
	
	
	}
	private void set(String subject) {
		if(subject.equals("biology")){setbiology();}
		else if(subject.equals("commerce")){setcommerce();}
		else if(subject.equals("crs")){setcrs();}
		else if(subject.equals("economics")){seteconomics();}
		else if(subject.equals("geography")){setgeography();}
		else if(subject.equals("government")){setgovernment();}
		else if(subject.equals("history")){sethistory();}
		else if(subject.equals("irk")){setirk();}
		else if(subject.equals("mathematics")){setmathematics();}
		else if(subject.equals("yoruba")){setyoruba();}
		else if(subject.equals("agriculture")){setagriculture();}
		else if(subject.equals("chemistry")){setagriculture();}
		else if(subject.equals("furthermath")){setfurthermath();}
		else if(subject.equals("physics")){setphysics();}
		else if(subject.equals("accounting")){setaccounting();}
	}
	public void setfurthermath()
	{
		
		questionmain.setText(further_math_1_1[check]);
		questionno.setText(further_math_1_1no[check]);
		radio1.setText(further_math_1_1_options[check*5]);
		radio2.setText(further_math_1_1_options[check*5+1]);
		radio3.setText(further_math_1_1_options[check*5+2]);
		radio4.setText(further_math_1_1_options[check*5+3]);
		if(further_math_1_1_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
		else {radio5.setText(further_math_1_1_options[check*5+4]);}
		
		
	}
	public void setchemistry()
	{
		
		questionmain.setText(chemistry_1_1[check]);
		questionno.setText(chemistry_1_1no[check]);
		radio1.setText(chemistry_1_1_options[check*5]);
		radio2.setText(chemistry_1_1_options[check*5+1]);
		radio3.setText(chemistry_1_1_options[check*5+2]);
		radio4.setText(chemistry_1_1_options[check*5+3]);
		if(chemistry_1_1_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
		else {radio5.setText(chemistry_1_1_options[check*5+4]);}
		
		
	}public void setagriculture()
	{
		
		questionmain.setText(agriculture_1_1[check]);
		questionno.setText(agriculture_1_1no[check]);
		radio1.setText(agriculture_1_1_options[check*5]);
		radio2.setText(agriculture_1_1_options[check*5+1]);
		radio3.setText(agriculture_1_1_options[check*5+2]);
		radio4.setText(agriculture_1_1_options[check*5+3]);
		if(agriculture_1_1_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
		else {radio5.setText(agriculture_1_1_options[check*5+4]);}
		
		
	}public void setirk()
	{
		
		questionmain.setText(irk_1_1[check]);
		questionno.setText(irk_1_1no[check]);
		radio1.setText(irk_1_1_options[check*5]);
		radio2.setText(irk_1_1_options[check*5+1]);
		radio3.setText(irk_1_1_options[check*5+2]);
		radio4.setText(irk_1_1_options[check*5+3]);
		if(irk_1_1_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
		else {radio5.setText(irk_1_1_options[check*5+4]);}
		
		
	}
	public void setmathematics()
	{
		
		questionmain.setText(mathematics_1_1[check]);
		questionno.setText(mathematics_1_1no[check]);
		radio1.setText(mathematics_1_1_options[check*5]);
		radio2.setText(mathematics_1_1_options[check*5+1]);
		radio3.setText(mathematics_1_1_options[check*5+2]);
		radio4.setText(mathematics_1_1_options[check*5+3]);
		if(mathematics_1_1_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
		else {radio5.setText(mathematics_1_1_options[check*5+4]);}
		
		
	}public void setyoruba()
	{
		
		questionmain.setText(yoruba_1_1[check]);
		questionno.setText(yoruba_1_1no[check]);
		radio1.setText(yoruba_1_1_options[check*5]);
		radio2.setText(yoruba_1_1_options[check*5+1]);
		radio3.setText(yoruba_1_1_options[check*5+2]);
		radio4.setText(yoruba_1_1_options[check*5+3]);
		if(yoruba_1_1_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
		else {radio5.setText(yoruba_1_1_options[check*5+4]);}
		
		
	}
	public void setcrs()
	{
		
		questionmain.setText(crs_1_1[check]);
		questionno.setText(crs_1_1no[check]);
		radio1.setText(crs_1_1_options[check*5]);
		radio2.setText(crs_1_1_options[check*5+1]);
		radio3.setText(crs_1_1_options[check*5+2]);
		radio4.setText(crs_1_1_options[check*5+3]);
		if(crs_1_1_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
		else {radio5.setText(crs_1_1_options[check*5+4]);}
		
		
	}
	public void setbiology()
	{
		if(chapter.equals("1")){
		questionmain.setText(biology_1_1[check]);
		questionno.setText(biology_1_1no[check]);
		radio1.setText(biology_1_1_options[check*5]);
		radio2.setText(biology_1_1_options[check*5+1]);
		radio3.setText(biology_1_1_options[check*5+2]);
		radio4.setText(biology_1_1_options[check*5+3]);
		if(biology_1_1_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
		else {radio5.setText(biology_1_1_options[check*5+4]);}
		}
		else if(chapter.equals("2")){
			questionmain.setText(biology_1_2[check]);
			questionno.setText(biology_1_2no[check]);
			radio1.setText(biology_1_2_options[check*5]);
			radio2.setText(biology_1_2_options[check*5+1]);
			radio3.setText(biology_1_2_options[check*5+2]);
			radio4.setText(biology_1_2_options[check*5+3]);
			if(biology_1_2_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(biology_1_2_options[check*5+4]);}
			}
		else if(chapter.equals("3")){
			questionmain.setText(biology_1_3[check]);
			questionno.setText(biology_1_3no[check]);
			radio1.setText(biology_1_3_options[check*5]);
			radio2.setText(biology_1_3_options[check*5+1]);
			radio3.setText(biology_1_3_options[check*5+2]);
			radio4.setText(biology_1_3_options[check*5+3]);
			if(biology_1_3_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(biology_1_3_options[check*5+4]);}
			}
		else if(chapter.equals("4")){
			questionmain.setText(biology_1_4[check]);
			questionno.setText(biology_1_4no[check]);
			radio1.setText(biology_1_4_options[check*5]);
			radio2.setText(biology_1_4_options[check*5+1]);
			radio3.setText(biology_1_4_options[check*5+2]);
			radio4.setText(biology_1_4_options[check*5+3]);
			if(biology_1_4_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(biology_1_4_options[check*5+4]);}
			}
		else if(chapter.equals("5")){
			questionmain.setText(biology_1_5[check]);
			questionno.setText(biology_1_5no[check]);
			radio1.setText(biology_1_5_options[check*5]);
			radio2.setText(biology_1_5_options[check*5+1]);
			radio3.setText(biology_1_5_options[check*5+2]);
			radio4.setText(biology_1_5_options[check*5+3]);
			if(biology_1_5_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(biology_1_5_options[check*5+4]);}
			}
		else if(chapter.equals("6")){
			questionmain.setText(biology_1_6[check]);
			questionno.setText(biology_1_6no[check]);
			radio1.setText(biology_1_6_options[check*5]);
			radio2.setText(biology_1_6_options[check*5+1]);
			radio3.setText(biology_1_6_options[check*5+2]);
			radio4.setText(biology_1_6_options[check*5+3]);
			if(biology_1_6_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(biology_1_6_options[check*5+4]);}
			}
		else if(chapter.equals("7")){
			questionmain.setText(biology_1_7[check]);
			questionno.setText(biology_1_7no[check]);
			radio1.setText(biology_1_7_options[check*5]);
			radio2.setText(biology_1_7_options[check*5+1]);
			radio3.setText(biology_1_7_options[check*5+2]);
			radio4.setText(biology_1_7_options[check*5+3]);
			if(biology_1_7_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(biology_1_7_options[check*5+4]);}
			}
		else if(chapter.equals("8")){
			questionmain.setText(biology_1_8[check]);
			questionno.setText(biology_1_8no[check]);
			radio1.setText(biology_1_8_options[check*5]);
			radio2.setText(biology_1_8_options[check*5+1]);
			radio3.setText(biology_1_8_options[check*5+2]);
			radio4.setText(biology_1_8_options[check*5+3]);
			if(biology_1_8_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(biology_1_8_options[check*5+4]);}
			}
		else if(chapter.equals("9")){
			questionmain.setText(biology_1_9[check]);
			questionno.setText(biology_1_9no[check]);
			radio1.setText(biology_1_9_options[check*5]);
			radio2.setText(biology_1_9_options[check*5+1]);
			radio3.setText(biology_1_9_options[check*5+2]);
			radio4.setText(biology_1_9_options[check*5+3]);
			if(biology_1_9_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(biology_1_9_options[check*5+4]);}
			}
		else if(chapter.equals("10")){
			questionmain.setText(biology_1_10[check]);
			questionno.setText(biology_1_10no[check]);
			radio1.setText(biology_1_10_options[check*5]);
			radio2.setText(biology_1_10_options[check*5+1]);
			radio3.setText(biology_1_10_options[check*5+2]);
			radio4.setText(biology_1_10_options[check*5+3]);
			if(biology_1_10_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(biology_1_10_options[check*5+4]);}
			}
	}
	public void setgovernment()
	{
		if(chapter.equals("s1")){
		questionmain.setText(government_1_1[check]);
		questionno.setText(government_1_1no[check]);
		radio1.setText(government_1_1_options[check*5]);
		radio2.setText(government_1_1_options[check*5+1]);
		radio3.setText(government_1_1_options[check*5+2]);
		radio4.setText(government_1_1_options[check*5+3]);
		if(government_1_1_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
		else {radio5.setText(government_1_1_options[check*5+4]);}
		}
		else if(chapter.equals("2")){
			questionmain.setText(government_1_2[check]);
			questionno.setText(government_1_2no[check]);
			radio1.setText(government_1_2_options[check*5]);
			radio2.setText(government_1_2_options[check*5+1]);
			radio3.setText(government_1_2_options[check*5+2]);
			radio4.setText(government_1_2_options[check*5+3]);
			if(government_1_2_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(government_1_2_options[check*5+4]);}
			}
		else if(chapter.equals("3")){
			questionmain.setText(government_1_3[check]);
			questionno.setText(government_1_3no[check]);
			radio1.setText(government_1_3_options[check*5]);
			radio2.setText(government_1_3_options[check*5+1]);
			radio3.setText(government_1_3_options[check*5+2]);
			radio4.setText(government_1_3_options[check*5+3]);
			if(government_1_3_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(government_1_3_options[check*5+4]);}
			}
		else if(chapter.equals("4")){
			questionmain.setText(government_1_4[check]);
			questionno.setText(government_1_4no[check]);
			radio1.setText(government_1_4_options[check*5]);
			radio2.setText(government_1_4_options[check*5+1]);
			radio3.setText(government_1_4_options[check*5+2]);
			radio4.setText(government_1_4_options[check*5+3]);
			if(government_1_4_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(government_1_4_options[check*5+4]);}
			}
		else if(chapter.equals("5")){
			questionmain.setText(government_1_5[check]);
			questionno.setText(government_1_5no[check]);
			radio1.setText(government_1_5_options[check*5]);
			radio2.setText(government_1_5_options[check*5+1]);
			radio3.setText(government_1_5_options[check*5+2]);
			radio4.setText(government_1_5_options[check*5+3]);
			if(government_1_5_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(government_1_5_options[check*5+4]);}
			}
		else if(chapter.equals("6")){
			questionmain.setText(government_1_6[check]);
			questionno.setText(government_1_6no[check]);
			radio1.setText(government_1_6_options[check*5]);
			radio2.setText(government_1_6_options[check*5+1]);
			radio3.setText(government_1_6_options[check*5+2]);
			radio4.setText(government_1_6_options[check*5+3]);
			if(government_1_6_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(government_1_6_options[check*5+4]);}
			}
		else if(chapter.equals("7")){
			questionmain.setText(government_1_7[check]);
			questionno.setText(government_1_7no[check]);
			radio1.setText(government_1_7_options[check*5]);
			radio2.setText(government_1_7_options[check*5+1]);
			radio3.setText(government_1_7_options[check*5+2]);
			radio4.setText(government_1_7_options[check*5+3]);
			if(government_1_7_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(government_1_7_options[check*5+4]);}
			}
		else if(chapter.equals("8")){
			questionmain.setText(government_1_8[check]);
			questionno.setText(government_1_8no[check]);
			radio1.setText(government_1_8_options[check*5]);
			radio2.setText(government_1_8_options[check*5+1]);
			radio3.setText(government_1_8_options[check*5+2]);
			radio4.setText(government_1_8_options[check*5+3]);
			if(government_1_8_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(government_1_8_options[check*5+4]);}
			}
		else if(chapter.equals("9")){
			questionmain.setText(government_1_9[check]);
			questionno.setText(government_1_9no[check]);
			radio1.setText(government_1_9_options[check*5]);
			radio2.setText(government_1_9_options[check*5+1]);
			radio3.setText(government_1_9_options[check*5+2]);
			radio4.setText(government_1_9_options[check*5+3]);
			if(government_1_9_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(government_1_9_options[check*5+4]);}
			}
		else if(chapter.equals("10")){
			questionmain.setText(government_1_10[check]);
			questionno.setText(government_1_10no[check]);
			radio1.setText(government_1_10_options[check*5]);
			radio2.setText(government_1_10_options[check*5+1]);
			radio3.setText(government_1_10_options[check*5+2]);
			radio4.setText(government_1_10_options[check*5+3]);
			if(government_1_10_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(government_1_10_options[check*5+4]);}
			}
		else if(chapter.equals("11")){
			questionmain.setText(government_1_11[check]);
			questionno.setText(government_1_11no[check]);
			radio1.setText(government_1_11_options[check*5]);
			radio2.setText(government_1_11_options[check*5+1]);
			radio3.setText(government_1_11_options[check*5+2]);
			radio4.setText(government_1_11_options[check*5+3]);
			if(government_1_11_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(government_1_11_options[check*5+4]);}
			}
		else if(chapter.equals("12")){
			questionmain.setText(government_1_12[check]);
			questionno.setText(government_1_12no[check]);
			radio1.setText(government_1_12_options[check*5]);
			radio2.setText(government_1_12_options[check*5+1]);
			radio3.setText(government_1_12_options[check*5+2]);
			radio4.setText(government_1_12_options[check*5+3]);
			if(government_1_12_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(government_1_12_options[check*5+4]);}
			}
	}
	public void setphysics()
	{
		if(chapter.equals("s1")){
		questionmain.setText(physics_1_1[check]);
		questionno.setText(physics_1_1no[check]);
		radio1.setText(physics_1_1_options[check*5]);
		radio2.setText(physics_1_1_options[check*5+1]);
		radio3.setText(physics_1_1_options[check*5+2]);
		radio4.setText(physics_1_1_options[check*5+3]);
		if(physics_1_1_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
		else {radio5.setText(physics_1_1_options[check*5+4]);}
		}
		else if(chapter.equals("2")){
			questionmain.setText(physics_1_2[check]);
			questionno.setText(physics_1_2no[check]);
			radio1.setText(physics_1_2_options[check*5]);
			radio2.setText(physics_1_2_options[check*5+1]);
			radio3.setText(physics_1_2_options[check*5+2]);
			radio4.setText(physics_1_2_options[check*5+3]);
			if(physics_1_2_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(physics_1_2_options[check*5+4]);}
			}
		else if(chapter.equals("3")){
			questionmain.setText(physics_1_3[check]);
			questionno.setText(physics_1_3no[check]);
			radio1.setText(physics_1_3_options[check*5]);
			radio2.setText(physics_1_3_options[check*5+1]);
			radio3.setText(physics_1_3_options[check*5+2]);
			radio4.setText(physics_1_3_options[check*5+3]);
			if(physics_1_3_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(physics_1_3_options[check*5+4]);}
			}
		else if(chapter.equals("4")){
			questionmain.setText(physics_1_4[check]);
			questionno.setText(physics_1_4no[check]);
			radio1.setText(physics_1_4_options[check*5]);
			radio2.setText(physics_1_4_options[check*5+1]);
			radio3.setText(physics_1_4_options[check*5+2]);
			radio4.setText(physics_1_4_options[check*5+3]);
			if(physics_1_4_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(physics_1_4_options[check*5+4]);}
			}
		else if(chapter.equals("5")){
			questionmain.setText(physics_1_5[check]);
			questionno.setText(physics_1_5no[check]);
			radio1.setText(physics_1_5_options[check*5]);
			radio2.setText(physics_1_5_options[check*5+1]);
			radio3.setText(physics_1_5_options[check*5+2]);
			radio4.setText(physics_1_5_options[check*5+3]);
			if(physics_1_5_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(physics_1_5_options[check*5+4]);}
			}
		else if(chapter.equals("6")){
			questionmain.setText(physics_1_6[check]);
			questionno.setText(physics_1_6no[check]);
			radio1.setText(physics_1_6_options[check*5]);
			radio2.setText(physics_1_6_options[check*5+1]);
			radio3.setText(physics_1_6_options[check*5+2]);
			radio4.setText(physics_1_6_options[check*5+3]);
			if(physics_1_6_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(physics_1_6_options[check*5+4]);}
			}
		else if(chapter.equals("7")){
			questionmain.setText(physics_1_7[check]);
			questionno.setText(physics_1_7no[check]);
			radio1.setText(physics_1_7_options[check*5]);
			radio2.setText(physics_1_7_options[check*5+1]);
			radio3.setText(physics_1_7_options[check*5+2]);
			radio4.setText(physics_1_7_options[check*5+3]);
			if(physics_1_7_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(physics_1_7_options[check*5+4]);}
			}
		else if(chapter.equals("8")){
			questionmain.setText(physics_1_8[check]);
			questionno.setText(physics_1_8no[check]);
			radio1.setText(physics_1_8_options[check*5]);
			radio2.setText(physics_1_8_options[check*5+1]);
			radio3.setText(physics_1_8_options[check*5+2]);
			radio4.setText(physics_1_8_options[check*5+3]);
			if(physics_1_8_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(physics_1_8_options[check*5+4]);}
			}
		else if(chapter.equals("9")){
			questionmain.setText(physics_1_9[check]);
			questionno.setText(physics_1_9no[check]);
			radio1.setText(physics_1_9_options[check*5]);
			radio2.setText(physics_1_9_options[check*5+1]);
			radio3.setText(physics_1_9_options[check*5+2]);
			radio4.setText(physics_1_9_options[check*5+3]);
			if(physics_1_9_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(physics_1_9_options[check*5+4]);}
			}
		else if(chapter.equals("10")){
			questionmain.setText(physics_1_10[check]);
			questionno.setText(physics_1_10no[check]);
			radio1.setText(physics_1_10_options[check*5]);
			radio2.setText(physics_1_10_options[check*5+1]);
			radio3.setText(physics_1_10_options[check*5+2]);
			radio4.setText(physics_1_10_options[check*5+3]);
			if(physics_1_10_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(physics_1_10_options[check*5+4]);}
			}
		else if(chapter.equals("11")){
			questionmain.setText(physics_1_11[check]);
			questionno.setText(physics_1_11no[check]);
			radio1.setText(physics_1_11_options[check*5]);
			radio2.setText(physics_1_11_options[check*5+1]);
			radio3.setText(physics_1_11_options[check*5+2]);
			radio4.setText(physics_1_11_options[check*5+3]);
			if(physics_1_11_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(physics_1_11_options[check*5+4]);}
			}
		else if(chapter.equals("12")){
			questionmain.setText(physics_1_12[check]);
			questionno.setText(physics_1_12no[check]);
			radio1.setText(physics_1_12_options[check*5]);
			radio2.setText(physics_1_12_options[check*5+1]);
			radio3.setText(physics_1_12_options[check*5+2]);
			radio4.setText(physics_1_12_options[check*5+3]);
			if(physics_1_12_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(physics_1_12_options[check*5+4]);}
			}
		else if(chapter.equals("13")){
			questionmain.setText(physics_1_13[check]);
			questionno.setText(physics_1_13no[check]);
			radio1.setText(physics_1_13_options[check*5]);
			radio2.setText(physics_1_13_options[check*5+1]);
			radio3.setText(physics_1_13_options[check*5+2]);
			radio4.setText(physics_1_13_options[check*5+3]);
			if(physics_1_13_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(physics_1_13_options[check*5+4]);}
			}
		else if(chapter.equals("14")){
			questionmain.setText(physics_1_14[check]);
			questionno.setText(physics_1_14no[check]);
			radio1.setText(physics_1_14_options[check*5]);
			radio2.setText(physics_1_14_options[check*5+1]);
			radio3.setText(physics_1_14_options[check*5+2]);
			radio4.setText(physics_1_14_options[check*5+3]);
			if(physics_1_14_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(physics_1_14_options[check*5+4]);}
			}
		else if(chapter.equals("15")){
			questionmain.setText(physics_1_15[check]);
			questionno.setText(physics_1_15no[check]);
			radio1.setText(physics_1_15_options[check*5]);
			radio2.setText(physics_1_15_options[check*5+1]);
			radio3.setText(physics_1_15_options[check*5+2]);
			radio4.setText(physics_1_15_options[check*5+3]);
			if(physics_1_15_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(physics_1_15_options[check*5+4]);}
			}
		else if(chapter.equals("16")){
			questionmain.setText(physics_1_16[check]);
			questionno.setText(physics_1_16no[check]);
			radio1.setText(physics_1_16_options[check*5]);
			radio2.setText(physics_1_16_options[check*5+1]);
			radio3.setText(physics_1_16_options[check*5+2]);
			radio4.setText(physics_1_16_options[check*5+3]);
			if(physics_1_16_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(physics_1_16_options[check*5+4]);}
			}
		else if(chapter.equals("17")){
			questionmain.setText(physics_1_17[check]);
			questionno.setText(physics_1_17no[check]);
			radio1.setText(physics_1_17_options[check*5]);
			radio2.setText(physics_1_17_options[check*5+1]);
			radio3.setText(physics_1_17_options[check*5+2]);
			radio4.setText(physics_1_17_options[check*5+3]);
			if(physics_1_17_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(physics_1_17_options[check*5+4]);}
			}
		else if(chapter.equals("18")){
			questionmain.setText(physics_1_18[check]);
			questionno.setText(physics_1_18no[check]);
			radio1.setText(physics_1_18_options[check*5]);
			radio2.setText(physics_1_18_options[check*5+1]);
			radio3.setText(physics_1_18_options[check*5+2]);
			radio4.setText(physics_1_18_options[check*5+3]);
			if(physics_1_18_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(physics_1_18_options[check*5+4]);}
			}
		else if(chapter.equals("19")){
			questionmain.setText(physics_1_19[check]);
			questionno.setText(physics_1_19no[check]);
			radio1.setText(physics_1_19_options[check*5]);
			radio2.setText(physics_1_19_options[check*5+1]);
			radio3.setText(physics_1_19_options[check*5+2]);
			radio4.setText(physics_1_19_options[check*5+3]);
			if(physics_1_19_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(physics_1_19_options[check*5+4]);}
			}
	
	}

	public void setaccounting()
	{
		if(chapter.equals("s1")){
		questionmain.setText(accounting_1_1[check]);
		questionno.setText(accounting_1_1no[check]);
		radio1.setText(accounting_1_1_options[check*5]);
		radio2.setText(accounting_1_1_options[check*5+1]);
		radio3.setText(accounting_1_1_options[check*5+2]);
		radio4.setText(accounting_1_1_options[check*5+3]);
		if(accounting_1_1_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
		else {radio5.setText(accounting_1_1_options[check*5+4]);}
		}
		else if(chapter.equals("2")){
			questionmain.setText(accounting_1_2[check]);
			questionno.setText(accounting_1_2no[check]);
			radio1.setText(accounting_1_2_options[check*5]);
			radio2.setText(accounting_1_2_options[check*5+1]);
			radio3.setText(accounting_1_2_options[check*5+2]);
			radio4.setText(accounting_1_2_options[check*5+3]);
			if(accounting_1_2_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(accounting_1_2_options[check*5+4]);}
			}
		else if(chapter.equals("3")){
			questionmain.setText(accounting_1_3[check]);
			questionno.setText(accounting_1_3no[check]);
			radio1.setText(accounting_1_3_options[check*5]);
			radio2.setText(accounting_1_3_options[check*5+1]);
			radio3.setText(accounting_1_3_options[check*5+2]);
			radio4.setText(accounting_1_3_options[check*5+3]);
			if(accounting_1_3_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(accounting_1_3_options[check*5+4]);}
			}
		else if(chapter.equals("4")){
			questionmain.setText(accounting_1_4[check]);
			questionno.setText(accounting_1_4no[check]);
			radio1.setText(accounting_1_4_options[check*5]);
			radio2.setText(accounting_1_4_options[check*5+1]);
			radio3.setText(accounting_1_4_options[check*5+2]);
			radio4.setText(accounting_1_4_options[check*5+3]);
			if(accounting_1_4_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(accounting_1_4_options[check*5+4]);}
			}
		else if(chapter.equals("5")){
			questionmain.setText(accounting_1_5[check]);
			questionno.setText(accounting_1_5no[check]);
			radio1.setText(accounting_1_5_options[check*5]);
			radio2.setText(accounting_1_5_options[check*5+1]);
			radio3.setText(accounting_1_5_options[check*5+2]);
			radio4.setText(accounting_1_5_options[check*5+3]);
			if(accounting_1_5_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(accounting_1_5_options[check*5+4]);}
			}
		else if(chapter.equals("6")){
			questionmain.setText(accounting_1_6[check]);
			questionno.setText(accounting_1_6no[check]);
			radio1.setText(accounting_1_6_options[check*5]);
			radio2.setText(accounting_1_6_options[check*5+1]);
			radio3.setText(accounting_1_6_options[check*5+2]);
			radio4.setText(accounting_1_6_options[check*5+3]);
			if(accounting_1_6_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(accounting_1_6_options[check*5+4]);}
			}
		else if(chapter.equals("7")){
			questionmain.setText(accounting_1_7[check]);
			questionno.setText(accounting_1_7no[check]);
			radio1.setText(accounting_1_7_options[check*5]);
			radio2.setText(accounting_1_7_options[check*5+1]);
			radio3.setText(accounting_1_7_options[check*5+2]);
			radio4.setText(accounting_1_7_options[check*5+3]);
			if(accounting_1_7_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(accounting_1_7_options[check*5+4]);}
			}
		else if(chapter.equals("8")){
			questionmain.setText(accounting_1_8[check]);
			questionno.setText(accounting_1_8no[check]);
			radio1.setText(accounting_1_8_options[check*5]);
			radio2.setText(accounting_1_8_options[check*5+1]);
			radio3.setText(accounting_1_8_options[check*5+2]);
			radio4.setText(accounting_1_8_options[check*5+3]);
			if(accounting_1_8_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(accounting_1_8_options[check*5+4]);}
			}
		else if(chapter.equals("9")){
			questionmain.setText(accounting_1_9[check]);
			questionno.setText(accounting_1_9no[check]);
			radio1.setText(accounting_1_9_options[check*5]);
			radio2.setText(accounting_1_9_options[check*5+1]);
			radio3.setText(accounting_1_9_options[check*5+2]);
			radio4.setText(accounting_1_9_options[check*5+3]);
			if(accounting_1_9_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(accounting_1_9_options[check*5+4]);}
			}
		else if(chapter.equals("10")){
			questionmain.setText(accounting_1_10[check]);
			questionno.setText(accounting_1_10no[check]);
			radio1.setText(accounting_1_10_options[check*5]);
			radio2.setText(accounting_1_10_options[check*5+1]);
			radio3.setText(accounting_1_10_options[check*5+2]);
			radio4.setText(accounting_1_10_options[check*5+3]);
			if(accounting_1_10_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(accounting_1_10_options[check*5+4]);}
			}
		else if(chapter.equals("11")){
			questionmain.setText(accounting_1_11[check]);
			questionno.setText(accounting_1_11no[check]);
			radio1.setText(accounting_1_11_options[check*5]);
			radio2.setText(accounting_1_11_options[check*5+1]);
			radio3.setText(accounting_1_11_options[check*5+2]);
			radio4.setText(accounting_1_11_options[check*5+3]);
			if(accounting_1_11_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(accounting_1_11_options[check*5+4]);}
			}
		else if(chapter.equals("12")){
			questionmain.setText(accounting_1_12[check]);
			questionno.setText(accounting_1_12no[check]);
			radio1.setText(accounting_1_12_options[check*5]);
			radio2.setText(accounting_1_12_options[check*5+1]);
			radio3.setText(accounting_1_12_options[check*5+2]);
			radio4.setText(accounting_1_12_options[check*5+3]);
			if(accounting_1_12_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(accounting_1_12_options[check*5+4]);}
			}
		else if(chapter.equals("13")){
			questionmain.setText(accounting_1_13[check]);
			questionno.setText(accounting_1_13no[check]);
			radio1.setText(accounting_1_13_options[check*5]);
			radio2.setText(accounting_1_13_options[check*5+1]);
			radio3.setText(accounting_1_13_options[check*5+2]);
			radio4.setText(accounting_1_13_options[check*5+3]);
			if(accounting_1_13_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(accounting_1_13_options[check*5+4]);}
			}
		else if(chapter.equals("14")){
			questionmain.setText(accounting_1_14[check]);
			questionno.setText(accounting_1_14no[check]);
			radio1.setText(accounting_1_14_options[check*5]);
			radio2.setText(accounting_1_14_options[check*5+1]);
			radio3.setText(accounting_1_14_options[check*5+2]);
			radio4.setText(accounting_1_14_options[check*5+3]);
			if(accounting_1_14_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(accounting_1_14_options[check*5+4]);}
			}
	
	}
	public void setcommerce()
	{
		if(chapter.equals("s1")){
		questionmain.setText(commerce_1_1[check]);
		questionno.setText(commerce_1_1no[check]);
		radio1.setText(commerce_1_1_options[check*5]);
		radio2.setText(commerce_1_1_options[check*5+1]);
		radio3.setText(commerce_1_1_options[check*5+2]);
		radio4.setText(commerce_1_1_options[check*5+3]);
		if(commerce_1_1_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
		else {radio5.setText(commerce_1_1_options[check*5+4]);}
		}
		else if(chapter.equals("2")){
			questionmain.setText(commerce_1_2[check]);
			questionno.setText(commerce_1_2no[check]);
			radio1.setText(commerce_1_2_options[check*5]);
			radio2.setText(commerce_1_2_options[check*5+1]);
			radio3.setText(commerce_1_2_options[check*5+2]);
			radio4.setText(commerce_1_2_options[check*5+3]);
			if(commerce_1_2_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_2_options[check*5+4]);}
			}
		else if(chapter.equals("3")){
			questionmain.setText(commerce_1_3[check]);
			questionno.setText(commerce_1_3no[check]);
			radio1.setText(commerce_1_3_options[check*5]);
			radio2.setText(commerce_1_3_options[check*5+1]);
			radio3.setText(commerce_1_3_options[check*5+2]);
			radio4.setText(commerce_1_3_options[check*5+3]);
			if(commerce_1_3_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_3_options[check*5+4]);}
			}
		else if(chapter.equals("4")){
			questionmain.setText(commerce_1_4[check]);
			questionno.setText(commerce_1_4no[check]);
			radio1.setText(commerce_1_4_options[check*5]);
			radio2.setText(commerce_1_4_options[check*5+1]);
			radio3.setText(commerce_1_4_options[check*5+2]);
			radio4.setText(commerce_1_4_options[check*5+3]);
			if(commerce_1_4_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_4_options[check*5+4]);}
			}
		else if(chapter.equals("5")){
			questionmain.setText(commerce_1_5[check]);
			questionno.setText(commerce_1_5no[check]);
			radio1.setText(commerce_1_5_options[check*5]);
			radio2.setText(commerce_1_5_options[check*5+1]);
			radio3.setText(commerce_1_5_options[check*5+2]);
			radio4.setText(commerce_1_5_options[check*5+3]);
			if(commerce_1_5_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_5_options[check*5+4]);}
			}
		else if(chapter.equals("6")){
			questionmain.setText(commerce_1_6[check]);
			questionno.setText(commerce_1_6no[check]);
			radio1.setText(commerce_1_6_options[check*5]);
			radio2.setText(commerce_1_6_options[check*5+1]);
			radio3.setText(commerce_1_6_options[check*5+2]);
			radio4.setText(commerce_1_6_options[check*5+3]);
			if(commerce_1_6_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_6_options[check*5+4]);}
			}
		else if(chapter.equals("7")){
			questionmain.setText(commerce_1_7[check]);
			questionno.setText(commerce_1_7no[check]);
			radio1.setText(commerce_1_7_options[check*5]);
			radio2.setText(commerce_1_7_options[check*5+1]);
			radio3.setText(commerce_1_7_options[check*5+2]);
			radio4.setText(commerce_1_7_options[check*5+3]);
			if(commerce_1_7_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_7_options[check*5+4]);}
			}
		else if(chapter.equals("8")){
			questionmain.setText(commerce_1_8[check]);
			questionno.setText(commerce_1_8no[check]);
			radio1.setText(commerce_1_8_options[check*5]);
			radio2.setText(commerce_1_8_options[check*5+1]);
			radio3.setText(commerce_1_8_options[check*5+2]);
			radio4.setText(commerce_1_8_options[check*5+3]);
			if(commerce_1_8_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_8_options[check*5+4]);}
			}
		else if(chapter.equals("9")){
			questionmain.setText(commerce_1_9[check]);
			questionno.setText(commerce_1_9no[check]);
			radio1.setText(commerce_1_9_options[check*5]);
			radio2.setText(commerce_1_9_options[check*5+1]);
			radio3.setText(commerce_1_9_options[check*5+2]);
			radio4.setText(commerce_1_9_options[check*5+3]);
			if(commerce_1_9_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_9_options[check*5+4]);}
			}
		else if(chapter.equals("10")){
			questionmain.setText(commerce_1_10[check]);
			questionno.setText(commerce_1_10no[check]);
			radio1.setText(commerce_1_10_options[check*5]);
			radio2.setText(commerce_1_10_options[check*5+1]);
			radio3.setText(commerce_1_10_options[check*5+2]);
			radio4.setText(commerce_1_10_options[check*5+3]);
			if(commerce_1_10_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_10_options[check*5+4]);}
			}
		else if(chapter.equals("11")){
			questionmain.setText(commerce_1_11[check]);
			questionno.setText(commerce_1_11no[check]);
			radio1.setText(commerce_1_11_options[check*5]);
			radio2.setText(commerce_1_11_options[check*5+1]);
			radio3.setText(commerce_1_11_options[check*5+2]);
			radio4.setText(commerce_1_11_options[check*5+3]);
			if(commerce_1_11_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_11_options[check*5+4]);}
			}
		else if(chapter.equals("12")){
			questionmain.setText(commerce_1_12[check]);
			questionno.setText(commerce_1_12no[check]);
			radio1.setText(commerce_1_12_options[check*5]);
			radio2.setText(commerce_1_12_options[check*5+1]);
			radio3.setText(commerce_1_12_options[check*5+2]);
			radio4.setText(commerce_1_12_options[check*5+3]);
			if(commerce_1_12_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_12_options[check*5+4]);}
			}
		else if(chapter.equals("13")){
			questionmain.setText(commerce_1_13[check]);
			questionno.setText(commerce_1_13no[check]);
			radio1.setText(commerce_1_13_options[check*5]);
			radio2.setText(commerce_1_13_options[check*5+1]);
			radio3.setText(commerce_1_13_options[check*5+2]);
			radio4.setText(commerce_1_13_options[check*5+3]);
			if(commerce_1_13_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_13_options[check*5+4]);}
			}
		else if(chapter.equals("14")){
			questionmain.setText(commerce_1_14[check]);
			questionno.setText(commerce_1_14no[check]);
			radio1.setText(commerce_1_14_options[check*5]);
			radio2.setText(commerce_1_14_options[check*5+1]);
			radio3.setText(commerce_1_14_options[check*5+2]);
			radio4.setText(commerce_1_14_options[check*5+3]);
			if(commerce_1_14_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_14_options[check*5+4]);}
			}
		else if(chapter.equals("15")){
			questionmain.setText(commerce_1_15[check]);
			questionno.setText(commerce_1_15no[check]);
			radio1.setText(commerce_1_15_options[check*5]);
			radio2.setText(commerce_1_15_options[check*5+1]);
			radio3.setText(commerce_1_15_options[check*5+2]);
			radio4.setText(commerce_1_15_options[check*5+3]);
			if(commerce_1_15_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_15_options[check*5+4]);}
			}
		else if(chapter.equals("16")){
			questionmain.setText(commerce_1_16[check]);
			questionno.setText(commerce_1_16no[check]);
			radio1.setText(commerce_1_16_options[check*5]);
			radio2.setText(commerce_1_16_options[check*5+1]);
			radio3.setText(commerce_1_16_options[check*5+2]);
			radio4.setText(commerce_1_16_options[check*5+3]);
			if(commerce_1_16_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_16_options[check*5+4]);}
			}
		else if(chapter.equals("17")){
			questionmain.setText(commerce_1_17[check]);
			questionno.setText(commerce_1_17no[check]);
			radio1.setText(commerce_1_17_options[check*5]);
			radio2.setText(commerce_1_17_options[check*5+1]);
			radio3.setText(commerce_1_17_options[check*5+2]);
			radio4.setText(commerce_1_17_options[check*5+3]);
			if(commerce_1_17_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_17_options[check*5+4]);}
			}
		else if(chapter.equals("18")){
			questionmain.setText(commerce_1_18[check]);
			questionno.setText(commerce_1_18no[check]);
			radio1.setText(commerce_1_18_options[check*5]);
			radio2.setText(commerce_1_18_options[check*5+1]);
			radio3.setText(commerce_1_18_options[check*5+2]);
			radio4.setText(commerce_1_18_options[check*5+3]);
			if(commerce_1_18_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_18_options[check*5+4]);}
			}
		else if(chapter.equals("19")){
			questionmain.setText(commerce_1_19[check]);
			questionno.setText(commerce_1_19no[check]);
			radio1.setText(commerce_1_19_options[check*5]);
			radio2.setText(commerce_1_19_options[check*5+1]);
			radio3.setText(commerce_1_19_options[check*5+2]);
			radio4.setText(commerce_1_19_options[check*5+3]);
			if(commerce_1_19_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_19_options[check*5+4]);}
			}
		else if(chapter.equals("20")){
			questionmain.setText(commerce_1_20[check]);
			questionno.setText(commerce_1_20no[check]);
			radio1.setText(commerce_1_20_options[check*5]);
			radio2.setText(commerce_1_20_options[check*5+1]);
			radio3.setText(commerce_1_20_options[check*5+2]);
			radio4.setText(commerce_1_20_options[check*5+3]);
			if(commerce_1_20_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_20_options[check*5+4]);}
			}
		else if(chapter.equals("21")){
			questionmain.setText(commerce_1_21[check]);
			questionno.setText(commerce_1_21no[check]);
			radio1.setText(commerce_1_21_options[check*5]);
			radio2.setText(commerce_1_21_options[check*5+1]);
			radio3.setText(commerce_1_21_options[check*5+2]);
			radio4.setText(commerce_1_21_options[check*5+3]);
			if(commerce_1_21_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_21_options[check*5+4]);}
			}
		else if(chapter.equals("22")){
			questionmain.setText(commerce_1_22[check]);
			questionno.setText(commerce_1_22no[check]);
			radio1.setText(commerce_1_22_options[check*5]);
			radio2.setText(commerce_1_22_options[check*5+1]);
			radio3.setText(commerce_1_22_options[check*5+2]);
			radio4.setText(commerce_1_22_options[check*5+3]);
			if(commerce_1_22_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_22_options[check*5+4]);}
			}
		else if(chapter.equals("23")){
			questionmain.setText(commerce_1_23[check]);
			questionno.setText(commerce_1_23no[check]);
			radio1.setText(commerce_1_23_options[check*5]);
			radio2.setText(commerce_1_23_options[check*5+1]);
			radio3.setText(commerce_1_23_options[check*5+2]);
			radio4.setText(commerce_1_23_options[check*5+3]);
			if(commerce_1_23_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_23_options[check*5+4]);}
			}	
		else if(chapter.equals("24")){
			questionmain.setText(commerce_1_24[check]);
			questionno.setText(commerce_1_24no[check]);
			radio1.setText(commerce_1_24_options[check*5]);
			radio2.setText(commerce_1_24_options[check*5+1]);
			radio3.setText(commerce_1_24_options[check*5+2]);
			radio4.setText(commerce_1_24_options[check*5+3]);
			if(commerce_1_24_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_24_options[check*5+4]);}
			}
		else if(chapter.equals("25")){
			questionmain.setText(commerce_1_24[check]);
			questionno.setText(commerce_1_24no[check]);
			radio1.setText(commerce_1_24_options[check*5]);
			radio2.setText(commerce_1_24_options[check*5+1]);
			radio3.setText(commerce_1_24_options[check*5+2]);
			radio4.setText(commerce_1_24_options[check*5+3]);
			if(commerce_1_24_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(commerce_1_24_options[check*5+4]);}
			}
	}
	
	public void seteconomics()
	{
		if(chapter.equals("s1")){
		questionmain.setText(economics_1_1[check]);
		questionno.setText(economics_1_1no[check]);
		radio1.setText(economics_1_1_options[check*5]);
		radio2.setText(economics_1_1_options[check*5+1]);
		radio3.setText(economics_1_1_options[check*5+2]);
		radio4.setText(economics_1_1_options[check*5+3]);
		if(economics_1_1_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
		else {radio5.setText(economics_1_1_options[check*5+4]);}
		}
		else if(chapter.equals("2")){
			questionmain.setText(economics_1_2[check]);
			questionno.setText(economics_1_2no[check]);
			radio1.setText(economics_1_2_options[check*5]);
			radio2.setText(economics_1_2_options[check*5+1]);
			radio3.setText(economics_1_2_options[check*5+2]);
			radio4.setText(economics_1_2_options[check*5+3]);
			if(economics_1_2_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(economics_1_2_options[check*5+4]);}
			}
		else if(chapter.equals("3")){
			questionmain.setText(economics_1_3[check]);
			questionno.setText(economics_1_3no[check]);
			radio1.setText(economics_1_3_options[check*5]);
			radio2.setText(economics_1_3_options[check*5+1]);
			radio3.setText(economics_1_3_options[check*5+2]);
			radio4.setText(economics_1_3_options[check*5+3]);
			if(economics_1_3_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(economics_1_3_options[check*5+4]);}
			}
		else if(chapter.equals("4")){
			questionmain.setText(economics_1_4[check]);
			questionno.setText(economics_1_4no[check]);
			radio1.setText(economics_1_4_options[check*5]);
			radio2.setText(economics_1_4_options[check*5+1]);
			radio3.setText(economics_1_4_options[check*5+2]);
			radio4.setText(economics_1_4_options[check*5+3]);
			if(economics_1_4_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(economics_1_4_options[check*5+4]);}
			}
		else if(chapter.equals("5")){
			questionmain.setText(economics_1_5[check]);
			questionno.setText(economics_1_5no[check]);
			radio1.setText(economics_1_5_options[check*5]);
			radio2.setText(economics_1_5_options[check*5+1]);
			radio3.setText(economics_1_5_options[check*5+2]);
			radio4.setText(economics_1_5_options[check*5+3]);
			if(economics_1_5_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(economics_1_5_options[check*5+4]);}
			}
		else if(chapter.equals("6")){
			questionmain.setText(economics_1_6[check]);
			questionno.setText(economics_1_6no[check]);
			radio1.setText(economics_1_6_options[check*5]);
			radio2.setText(economics_1_6_options[check*5+1]);
			radio3.setText(economics_1_6_options[check*5+2]);
			radio4.setText(economics_1_6_options[check*5+3]);
			if(economics_1_6_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(economics_1_6_options[check*5+4]);}
			}
		else if(chapter.equals("7")){
			questionmain.setText(economics_1_7[check]);
			questionno.setText(economics_1_7no[check]);
			radio1.setText(economics_1_7_options[check*5]);
			radio2.setText(economics_1_7_options[check*5+1]);
			radio3.setText(economics_1_7_options[check*5+2]);
			radio4.setText(economics_1_7_options[check*5+3]);
			if(economics_1_7_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(economics_1_7_options[check*5+4]);}
			}
		else if(chapter.equals("8")){
			questionmain.setText(economics_1_8[check]);
			questionno.setText(economics_1_8no[check]);
			radio1.setText(economics_1_8_options[check*5]);
			radio2.setText(economics_1_8_options[check*5+1]);
			radio3.setText(economics_1_8_options[check*5+2]);
			radio4.setText(economics_1_8_options[check*5+3]);
			if(economics_1_8_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(economics_1_8_options[check*5+4]);}
			}
		else if(chapter.equals("9")){
			questionmain.setText(economics_1_9[check]);
			questionno.setText(economics_1_9no[check]);
			radio1.setText(economics_1_9_options[check*5]);
			radio2.setText(economics_1_9_options[check*5+1]);
			radio3.setText(economics_1_9_options[check*5+2]);
			radio4.setText(economics_1_9_options[check*5+3]);
			if(economics_1_9_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(economics_1_9_options[check*5+4]);}
			}
		else if(chapter.equals("10")){
			questionmain.setText(economics_1_10[check]);
			questionno.setText(economics_1_10no[check]);
			radio1.setText(economics_1_10_options[check*5]);
			radio2.setText(economics_1_10_options[check*5+1]);
			radio3.setText(economics_1_10_options[check*5+2]);
			radio4.setText(economics_1_10_options[check*5+3]);
			if(economics_1_10_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(economics_1_10_options[check*5+4]);}
			}
		else if(chapter.equals("11")){
			questionmain.setText(economics_1_11[check]);
			questionno.setText(economics_1_11no[check]);
			radio1.setText(economics_1_11_options[check*5]);
			radio2.setText(economics_1_11_options[check*5+1]);
			radio3.setText(economics_1_11_options[check*5+2]);
			radio4.setText(economics_1_11_options[check*5+3]);
			if(economics_1_11_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(economics_1_11_options[check*5+4]);}
			}
		else if(chapter.equals("12")){
			questionmain.setText(economics_1_12[check]);
			questionno.setText(economics_1_12no[check]);
			radio1.setText(economics_1_12_options[check*5]);
			radio2.setText(economics_1_12_options[check*5+1]);
			radio3.setText(economics_1_12_options[check*5+2]);
			radio4.setText(economics_1_12_options[check*5+3]);
			if(economics_1_12_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(economics_1_12_options[check*5+4]);}
			}
		else if(chapter.equals("13")){
			questionmain.setText(economics_1_13[check]);
			questionno.setText(economics_1_13no[check]);
			radio1.setText(economics_1_13_options[check*5]);
			radio2.setText(economics_1_13_options[check*5+1]);
			radio3.setText(economics_1_13_options[check*5+2]);
			radio4.setText(economics_1_13_options[check*5+3]);
			if(economics_1_13_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(economics_1_13_options[check*5+4]);}
			}
		else if(chapter.equals("14")){
			questionmain.setText(economics_1_14[check]);
			questionno.setText(economics_1_14no[check]);
			radio1.setText(economics_1_14_options[check*5]);
			radio2.setText(economics_1_14_options[check*5+1]);
			radio3.setText(economics_1_14_options[check*5+2]);
			radio4.setText(economics_1_14_options[check*5+3]);
			if(economics_1_14_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(economics_1_14_options[check*5+4]);}
			}
		else if(chapter.equals("15")){
			questionmain.setText(economics_1_15[check]);
			questionno.setText(economics_1_15no[check]);
			radio1.setText(economics_1_15_options[check*5]);
			radio2.setText(economics_1_15_options[check*5+1]);
			radio3.setText(economics_1_15_options[check*5+2]);
			radio4.setText(economics_1_15_options[check*5+3]);
			if(economics_1_15_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(economics_1_15_options[check*5+4]);}
			}
		else if(chapter.equals("16")){
			questionmain.setText(economics_1_16[check]);
			questionno.setText(economics_1_16no[check]);
			radio1.setText(economics_1_16_options[check*5]);
			radio2.setText(economics_1_16_options[check*5+1]);
			radio3.setText(economics_1_16_options[check*5+2]);
			radio4.setText(economics_1_16_options[check*5+3]);
			if(economics_1_16_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(economics_1_16_options[check*5+4]);}
			}
		else if(chapter.equals("17")){
			questionmain.setText(economics_1_17[check]);
			questionno.setText(economics_1_17no[check]);
			radio1.setText(economics_1_17_options[check*5]);
			radio2.setText(economics_1_17_options[check*5+1]);
			radio3.setText(economics_1_17_options[check*5+2]);
			radio4.setText(economics_1_17_options[check*5+3]);
			if(economics_1_17_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(economics_1_17_options[check*5+4]);}
			}
		else if(chapter.equals("18")){
			questionmain.setText(economics_1_18[check]);
			questionno.setText(economics_1_18no[check]);
			radio1.setText(economics_1_18_options[check*5]);
			radio2.setText(economics_1_18_options[check*5+1]);
			radio3.setText(economics_1_18_options[check*5+2]);
			radio4.setText(economics_1_18_options[check*5+3]);
			if(economics_1_18_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(economics_1_18_options[check*5+4]);}
			}
		else if(chapter.equals("19")){
			questionmain.setText(economics_1_19[check]);
			questionno.setText(economics_1_19no[check]);
			radio1.setText(economics_1_19_options[check*5]);
			radio2.setText(economics_1_19_options[check*5+1]);
			radio3.setText(economics_1_19_options[check*5+2]);
			radio4.setText(economics_1_19_options[check*5+3]);
			if(economics_1_19_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(economics_1_19_options[check*5+4]);}
			}
		
	}
	public void setgeography()
	{
		if(chapter.equals("s1")){
		questionmain.setText(geography_1_1[check]);
		questionno.setText(geography_1_1no[check]);
		radio1.setText(geography_1_1_options[check*5]);
		radio2.setText(geography_1_1_options[check*5+1]);
		radio3.setText(geography_1_1_options[check*5+2]);
		radio4.setText(geography_1_1_options[check*5+3]);
		if(geography_1_1_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
		else {radio5.setText(geography_1_1_options[check*5+4]);}
		}
		else if(chapter.equals("2")){
			questionmain.setText(geography_1_2[check]);
			questionno.setText(geography_1_2no[check]);
			radio1.setText(geography_1_2_options[check*5]);
			radio2.setText(geography_1_2_options[check*5+1]);
			radio3.setText(geography_1_2_options[check*5+2]);
			radio4.setText(geography_1_2_options[check*5+3]);
			if(geography_1_2_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(geography_1_2_options[check*5+4]);}
			}
		else if(chapter.equals("3")){
			questionmain.setText(geography_1_3[check]);
			questionno.setText(geography_1_3no[check]);
			radio1.setText(geography_1_3_options[check*5]);
			radio2.setText(geography_1_3_options[check*5+1]);
			radio3.setText(geography_1_3_options[check*5+2]);
			radio4.setText(geography_1_3_options[check*5+3]);
			if(geography_1_3_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(geography_1_3_options[check*5+4]);}
			}
		else if(chapter.equals("4")){
			questionmain.setText(geography_1_4[check]);
			questionno.setText(geography_1_4no[check]);
			radio1.setText(geography_1_4_options[check*5]);
			radio2.setText(geography_1_4_options[check*5+1]);
			radio3.setText(geography_1_4_options[check*5+2]);
			radio4.setText(geography_1_4_options[check*5+3]);
			if(geography_1_4_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(geography_1_4_options[check*5+4]);}
			}
		else if(chapter.equals("5")){
			questionmain.setText(geography_1_5[check]);
			questionno.setText(geography_1_5no[check]);
			radio1.setText(geography_1_5_options[check*5]);
			radio2.setText(geography_1_5_options[check*5+1]);
			radio3.setText(geography_1_5_options[check*5+2]);
			radio4.setText(geography_1_5_options[check*5+3]);
			if(geography_1_5_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(geography_1_5_options[check*5+4]);}
			}
		else if(chapter.equals("6")){
			questionmain.setText(geography_1_6[check]);
			questionno.setText(geography_1_6no[check]);
			radio1.setText(geography_1_6_options[check*5]);
			radio2.setText(geography_1_6_options[check*5+1]);
			radio3.setText(geography_1_6_options[check*5+2]);
			radio4.setText(geography_1_6_options[check*5+3]);
			if(geography_1_6_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(geography_1_6_options[check*5+4]);}
			}
		else if(chapter.equals("7")){
			questionmain.setText(geography_1_7[check]);
			questionno.setText(geography_1_7no[check]);
			radio1.setText(geography_1_7_options[check*5]);
			radio2.setText(geography_1_7_options[check*5+1]);
			radio3.setText(geography_1_7_options[check*5+2]);
			radio4.setText(geography_1_7_options[check*5+3]);
			if(geography_1_7_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(geography_1_7_options[check*5+4]);}
			}
		else if(chapter.equals("8")){
			questionmain.setText(geography_1_8[check]);
			questionno.setText(geography_1_8no[check]);
			radio1.setText(geography_1_8_options[check*5]);
			radio2.setText(geography_1_8_options[check*5+1]);
			radio3.setText(geography_1_8_options[check*5+2]);
			radio4.setText(geography_1_8_options[check*5+3]);
			if(geography_1_8_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(geography_1_8_options[check*5+4]);}
			}
		else if(chapter.equals("9")){
			questionmain.setText(geography_1_9[check]);
			questionno.setText(geography_1_9no[check]);
			radio1.setText(geography_1_9_options[check*5]);
			radio2.setText(geography_1_9_options[check*5+1]);
			radio3.setText(geography_1_9_options[check*5+2]);
			radio4.setText(geography_1_9_options[check*5+3]);
			if(geography_1_9_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(geography_1_9_options[check*5+4]);}
			}
		else if(chapter.equals("10")){
			questionmain.setText(geography_1_10[check]);
			questionno.setText(geography_1_10no[check]);
			radio1.setText(geography_1_10_options[check*5]);
			radio2.setText(geography_1_10_options[check*5+1]);
			radio3.setText(geography_1_10_options[check*5+2]);
			radio4.setText(geography_1_10_options[check*5+3]);
			if(geography_1_10_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(geography_1_10_options[check*5+4]);}
			}
		else if(chapter.equals("11")){
			questionmain.setText(geography_1_11[check]);
			questionno.setText(geography_1_11no[check]);
			radio1.setText(geography_1_11_options[check*5]);
			radio2.setText(geography_1_11_options[check*5+1]);
			radio3.setText(geography_1_11_options[check*5+2]);
			radio4.setText(geography_1_11_options[check*5+3]);
			if(geography_1_11_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(geography_1_11_options[check*5+4]);}
			}
		else if(chapter.equals("12")){
			questionmain.setText(geography_1_12[check]);
			questionno.setText(geography_1_12no[check]);
			radio1.setText(geography_1_12_options[check*5]);
			radio2.setText(geography_1_12_options[check*5+1]);
			radio3.setText(geography_1_12_options[check*5+2]);
			radio4.setText(geography_1_12_options[check*5+3]);
			if(geography_1_12_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(geography_1_12_options[check*5+4]);}
			}
		else if(chapter.equals("13")){
			questionmain.setText(geography_1_13[check]);
			questionno.setText(geography_1_13no[check]);
			radio1.setText(geography_1_13_options[check*5]);
			radio2.setText(geography_1_13_options[check*5+1]);
			radio3.setText(geography_1_13_options[check*5+2]);
			radio4.setText(geography_1_13_options[check*5+3]);
			if(geography_1_13_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(geography_1_13_options[check*5+4]);}
			}
		else if(chapter.equals("14")){
			questionmain.setText(geography_1_14[check]);
			questionno.setText(geography_1_14no[check]);
			radio1.setText(geography_1_14_options[check*5]);
			radio2.setText(geography_1_14_options[check*5+1]);
			radio3.setText(geography_1_14_options[check*5+2]);
			radio4.setText(geography_1_14_options[check*5+3]);
			if(geography_1_14_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(geography_1_14_options[check*5+4]);}
			}
		else if(chapter.equals("15")){
			questionmain.setText(geography_1_15[check]);
			questionno.setText(geography_1_15no[check]);
			radio1.setText(geography_1_15_options[check*5]);
			radio2.setText(geography_1_15_options[check*5+1]);
			radio3.setText(geography_1_15_options[check*5+2]);
			radio4.setText(geography_1_15_options[check*5+3]);
			if(geography_1_15_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(geography_1_15_options[check*5+4]);}
			}
		else if(chapter.equals("16")){
			questionmain.setText(geography_1_16[check]);
			questionno.setText(geography_1_16no[check]);
			radio1.setText(geography_1_16_options[check*5]);
			radio2.setText(geography_1_16_options[check*5+1]);
			radio3.setText(geography_1_16_options[check*5+2]);
			radio4.setText(geography_1_16_options[check*5+3]);
			if(geography_1_16_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(geography_1_16_options[check*5+4]);}
			}
		else if(chapter.equals("17")){
			questionmain.setText(geography_1_17[check]);
			questionno.setText(geography_1_17no[check]);
			radio1.setText(geography_1_17_options[check*5]);
			radio2.setText(geography_1_17_options[check*5+1]);
			radio3.setText(geography_1_17_options[check*5+2]);
			radio4.setText(geography_1_17_options[check*5+3]);
			if(geography_1_17_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(geography_1_17_options[check*5+4]);}
			}
		else if(chapter.equals("18")){
			questionmain.setText(geography_1_18[check]);
			questionno.setText(geography_1_18no[check]);
			radio1.setText(geography_1_18_options[check*5]);
			radio2.setText(geography_1_18_options[check*5+1]);
			radio3.setText(geography_1_18_options[check*5+2]);
			radio4.setText(geography_1_18_options[check*5+3]);
			if(geography_1_18_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(geography_1_18_options[check*5+4]);}
			}
		else if(chapter.equals("19")){
			questionmain.setText(geography_1_19[check]);
			questionno.setText(geography_1_19no[check]);
			radio1.setText(geography_1_19_options[check*5]);
			radio2.setText(geography_1_19_options[check*5+1]);
			radio3.setText(geography_1_19_options[check*5+2]);
			radio4.setText(geography_1_19_options[check*5+3]);
			if(geography_1_19_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(geography_1_19_options[check*5+4]);}
			}
		
	}
	private void setOnClickListener() {
		questionnext.setOnClickListener(this);
		questionend.setOnClickListener(this);
	}
	public void sethistory()
	{
		if(chapter.equals("s1")){
		questionmain.setText(history_1_1[check]);
		questionno.setText(history_1_1no[check]);
		radio1.setText(history_1_1_options[check*5]);
		radio2.setText(history_1_1_options[check*5+1]);
		radio3.setText(history_1_1_options[check*5+2]);
		radio4.setText(history_1_1_options[check*5+3]);
		if(history_1_1_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
		else {radio5.setText(history_1_1_options[check*5+4]);}
		}
		else if(chapter.equals("2")){
			questionmain.setText(history_1_2[check]);
			questionno.setText(history_1_2no[check]);
			radio1.setText(history_1_2_options[check*5]);
			radio2.setText(history_1_2_options[check*5+1]);
			radio3.setText(history_1_2_options[check*5+2]);
			radio4.setText(history_1_2_options[check*5+3]);
			if(history_1_2_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(history_1_2_options[check*5+4]);}
			}
		else if(chapter.equals("3")){
			questionmain.setText(history_1_3[check]);
			questionno.setText(history_1_3no[check]);
			radio1.setText(history_1_3_options[check*5]);
			radio2.setText(history_1_3_options[check*5+1]);
			radio3.setText(history_1_3_options[check*5+2]);
			radio4.setText(history_1_3_options[check*5+3]);
			if(history_1_3_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(history_1_3_options[check*5+4]);}
			}
		else if(chapter.equals("4")){
			questionmain.setText(history_1_4[check]);
			questionno.setText(history_1_4no[check]);
			radio1.setText(history_1_4_options[check*5]);
			radio2.setText(history_1_4_options[check*5+1]);
			radio3.setText(history_1_4_options[check*5+2]);
			radio4.setText(history_1_4_options[check*5+3]);
			if(history_1_4_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(history_1_4_options[check*5+4]);}
			}
		else if(chapter.equals("5")){
			questionmain.setText(history_1_5[check]);
			questionno.setText(history_1_5no[check]);
			radio1.setText(history_1_5_options[check*5]);
			radio2.setText(history_1_5_options[check*5+1]);
			radio3.setText(history_1_5_options[check*5+2]);
			radio4.setText(history_1_5_options[check*5+3]);
			if(history_1_5_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(history_1_5_options[check*5+4]);}
			}
		else if(chapter.equals("6")){
			questionmain.setText(history_1_6[check]);
			questionno.setText(history_1_6no[check]);
			radio1.setText(history_1_6_options[check*5]);
			radio2.setText(history_1_6_options[check*5+1]);
			radio3.setText(history_1_6_options[check*5+2]);
			radio4.setText(history_1_6_options[check*5+3]);
			if(history_1_6_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(history_1_6_options[check*5+4]);}
			}
		else if(chapter.equals("7")){
			questionmain.setText(history_1_7[check]);
			questionno.setText(history_1_7no[check]);
			radio1.setText(history_1_7_options[check*5]);
			radio2.setText(history_1_7_options[check*5+1]);
			radio3.setText(history_1_7_options[check*5+2]);
			radio4.setText(history_1_7_options[check*5+3]);
			if(history_1_7_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(history_1_7_options[check*5+4]);}
			}
		else if(chapter.equals("8")){
			questionmain.setText(history_1_8[check]);
			questionno.setText(history_1_8no[check]);
			radio1.setText(history_1_8_options[check*5]);
			radio2.setText(history_1_8_options[check*5+1]);
			radio3.setText(history_1_8_options[check*5+2]);
			radio4.setText(history_1_8_options[check*5+3]);
			if(history_1_8_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(history_1_8_options[check*5+4]);}
			}
		else if(chapter.equals("9")){
			questionmain.setText(history_1_9[check]);
			questionno.setText(history_1_9no[check]);
			radio1.setText(history_1_9_options[check*5]);
			radio2.setText(history_1_9_options[check*5+1]);
			radio3.setText(history_1_9_options[check*5+2]);
			radio4.setText(history_1_9_options[check*5+3]);
			if(history_1_9_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(history_1_9_options[check*5+4]);}
			}
		else if(chapter.equals("10")){
			questionmain.setText(history_1_10[check]);
			questionno.setText(history_1_10no[check]);
			radio1.setText(history_1_10_options[check*5]);
			radio2.setText(history_1_10_options[check*5+1]);
			radio3.setText(history_1_10_options[check*5+2]);
			radio4.setText(history_1_10_options[check*5+3]);
			if(history_1_10_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(history_1_10_options[check*5+4]);}
			}
		else if(chapter.equals("11")){
			questionmain.setText(history_1_11[check]);
			questionno.setText(history_1_11no[check]);
			radio1.setText(history_1_11_options[check*5]);
			radio2.setText(history_1_11_options[check*5+1]);
			radio3.setText(history_1_11_options[check*5+2]);
			radio4.setText(history_1_11_options[check*5+3]);
			if(history_1_11_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(history_1_11_options[check*5+4]);}
			}
		else if(chapter.equals("12")){
			questionmain.setText(history_1_12[check]);
			questionno.setText(history_1_12no[check]);
			radio1.setText(history_1_12_options[check*5]);
			radio2.setText(history_1_12_options[check*5+1]);
			radio3.setText(history_1_12_options[check*5+2]);
			radio4.setText(history_1_12_options[check*5+3]);
			if(history_1_12_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(history_1_12_options[check*5+4]);}
			}
		else if(chapter.equals("13")){
			questionmain.setText(history_1_13[check]);
			questionno.setText(history_1_13no[check]);
			radio1.setText(history_1_13_options[check*5]);
			radio2.setText(history_1_13_options[check*5+1]);
			radio3.setText(history_1_13_options[check*5+2]);
			radio4.setText(history_1_13_options[check*5+3]);
			if(history_1_13_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(history_1_13_options[check*5+4]);}
			}
		else if(chapter.equals("14")){
			questionmain.setText(history_1_14[check]);
			questionno.setText(history_1_14no[check]);
			radio1.setText(history_1_14_options[check*5]);
			radio2.setText(history_1_14_options[check*5+1]);
			radio3.setText(history_1_14_options[check*5+2]);
			radio4.setText(history_1_14_options[check*5+3]);
			if(history_1_14_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(history_1_14_options[check*5+4]);}
			}
		else if(chapter.equals("15")){
			questionmain.setText(history_1_15[check]);
			questionno.setText(history_1_15no[check]);
			radio1.setText(history_1_15_options[check*5]);
			radio2.setText(history_1_15_options[check*5+1]);
			radio3.setText(history_1_15_options[check*5+2]);
			radio4.setText(history_1_15_options[check*5+3]);
			if(history_1_15_options[check*5+4].equals("")){ radio5.setVisibility(View.GONE);}
			else {radio5.setText(history_1_15_options[check*5+4]);}
			}

} 
	
	private void initialize() {
		questiontypevalue= (TextView)findViewById(R.id.questiontypevalue);
		answertextview = (TextView)findViewById(R.id.answertextview);
		questionmain = (TextView)findViewById(R.id.questionsmain);
		questionno =  (TextView)findViewById(R.id.questionnobottom);
		questionnext =  (ImageView)findViewById(R.id.questionnext);
		questionend =  (ImageView)findViewById(R.id.questionend);
		radio1 =  (RadioButton)findViewById(R.id.radio1);
		radio2 =  (RadioButton)findViewById(R.id.radio2);
		radio3 =  (RadioButton)findViewById(R.id.radio3);
		radio4 =  (RadioButton)findViewById(R.id.radio4);
		radio5 =  (RadioButton)findViewById(R.id.radio5);
		questionradioGroup = (RadioGroup) findViewById(R.id.questionradiogroup);
		}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.questionnext:
			questionradioGroup.clearCheck();
			answertextview.setText("");
			check++;
			if(check==5)
			{
				check = 0;
			if(subject.equals("biology")){setbiology();}
			
			}
			else
			{
				setbiology();
			} 
			break;
		case R.id.questionend:
			questionradioGroup.clearCheck();
			answertextview.setText("");
			check=4;
			setbiology();
			break;
		default:
			break;
		}
	}
	}
