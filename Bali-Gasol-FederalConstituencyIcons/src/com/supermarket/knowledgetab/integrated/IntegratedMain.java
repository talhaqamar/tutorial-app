package com.supermarket.knowledgetab.integrated;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.supermarket.knowledgetab.R;
import com.supermarket.knowlegdetab.mock.MockMain;

public class IntegratedMain  extends Activity implements OnClickListener{
	
	LinearLayout practice_linear,mockexam_linear;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.integratedmain);
	
		initialize();
		setOnclicklistner();
		
	}

	private void setOnclicklistner() {
		practice_linear.setOnClickListener(this);
 		mockexam_linear.setOnClickListener(this);
	}

	private void initialize() {
		// TODO Auto-generated method stub
		practice_linear = (LinearLayout)findViewById(R.id.practice_linear);
		mockexam_linear = (LinearLayout)findViewById(R.id.mockexam_linear);
	
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.practice_linear:
			Intent in = new Intent(IntegratedMain.this,PracticeMain.class);
			in.putExtra("check", "practice");
			startActivity(in);
			break;
		case R.id.mockexam_linear:
			Intent in1= new Intent(IntegratedMain.this,MockMain.class);
			//in.putExtra("check", "practice");
			startActivity(in1);
			
			break;

		default:
			break;
		}
	}
}
