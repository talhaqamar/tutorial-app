package com.supermarket.knowledgetab.video;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.supermarket.knowledgetab.R;

import com.supermarket.knowledgetab.ktab.Ss1;

public class VideoMain extends Activity implements OnClickListener {
	LinearLayout ss1,ss2,ss3;
String check ="";
	@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.videomain);

	
	initialize();
	setOnclicklistner();
	
	}
private void setOnclicklistner() {
	ss1.setOnClickListener(this);
	ss2.setOnClickListener(this);
	ss3.setOnClickListener(this);
}
private void initialize() {
	ss1 =(LinearLayout)findViewById(R.id.ss1_linear);
	ss2 =(LinearLayout)findViewById(R.id.ss2_linear);
	ss3 =(LinearLayout)findViewById(R.id.ss3_linear);
	}

	@Override
	public void onClick(View v) {
	switch (v.getId()) {
	case R.id.ss1_linear:
		Intent in1 = new Intent(VideoMain.this,AfterVideoMainSS1.class);
		in1.putExtra("library","ss1");
		
		startActivity(in1);
		break;
	case R.id.ss2_linear:
		Intent in2 = new Intent(VideoMain.this,AfterVideoMainSS1.class);
		in2.putExtra("library","ss2");
	
		startActivity(in2);
		break;
	case R.id.ss3_linear:
		Intent in3 = new Intent(VideoMain.this,AfterVideoMainSS1.class);
		in3.putExtra("library","ss3");
	
		startActivity(in3);
		break;

	default:
		break;
	}
	
}
}