package com.supermarket.knowledgetab.video;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.supermarket.knowledgetab.R;


public class AfterVideoMainSS1  extends Activity implements OnClickListener {
	LinearLayout art,science,social;
	ImageView lib;
	TextView librarytext;
String check ="";
String library = "";
	@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.aftervideomainss1);

	Intent in = getIntent();
	library = in.getStringExtra("library");
		
	
	initialize();
	setOnclicklistner();
	if(library.equals("ss1"))
	{ 
		Drawable d = getResources().getDrawable(R.drawable.ss1);
	lib.setImageDrawable(d);librarytext.setText("SS1");}
	else if(library.equals("ss2"))
	{ 
	Drawable d = getResources().getDrawable(R.drawable.ss2);
	lib.setImageDrawable(d);librarytext.setText("SS2");
	}
	if(library.equals("ss3"))
	{ Drawable d = getResources().getDrawable(R.drawable.ss3);
	lib.setImageDrawable(d);
	librarytext.setText("SS3");}
}
private void setOnclicklistner() {
	art.setOnClickListener(this);
	science.setOnClickListener(this);
	social.setOnClickListener(this);
}
private void initialize() {
	art=(LinearLayout)findViewById(R.id.art_linear);
	science=(LinearLayout)findViewById(R.id.science_linear);
	social=(LinearLayout)findViewById(R.id.socialscience_linear);
	lib = (ImageView)findViewById(R.id.library1111);
	librarytext = (TextView)findViewById(R.id.librarytext);
}
@Override
public void onClick(View v) {
	switch (v.getId()) {
	case R.id.art_linear:
		Toast.makeText(AfterVideoMainSS1.this, "artclicked", 5000).show();
		Intent in1 = new Intent(AfterVideoMainSS1.this,ArtMain2.class);
		
		startActivity(in1);
		break;
	case R.id.science_linear:
		Toast.makeText(AfterVideoMainSS1.this, check.toString(), 5000).show();
		Intent in2 = new Intent(AfterVideoMainSS1.this,ScienceMain2.class);
		
		startActivity(in2);
		
		break;
	case R.id.socialscience_linear:
		Toast.makeText(AfterVideoMainSS1.this, check.toString(), 5000).show();
		Intent in3 = new Intent(AfterVideoMainSS1.this,SocialScienceMain2.class);
		
		startActivity(in3);
		
		break;

	default:
		break;
	}
}


}
