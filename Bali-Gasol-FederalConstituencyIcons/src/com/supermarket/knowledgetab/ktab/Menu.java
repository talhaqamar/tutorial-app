package com.supermarket.knowledgetab.ktab;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.supermarket.knowledgetab.R;
import com.supermarket.knowledgetab.integrated.Questions;

public class Menu extends Activity implements OnClickListener {
	LinearLayout chap1_linear,chap2_linear,chap3_linear,chap4_linear,chap5_linear,
	chap6_linear,chap7_linear,chap8_linear,chap9_linear,chap10_linear,
	chap11_linear,chap12_linear,chap13_linear,chap14_linear,chap15_linear,
	chap16_linear,chap17_linear,chap18_linear,chap19_linear,chap20_linear,
	chap21_linear,chap22_linear,chap23_linear,chap24_linear,chap25_linear
	;
	RelativeLayout chap1_relative,chap2_relative,chap3_relative,chap4_relative,
	chap5_relative,
	chap6_relative,chap7_relative,chap8_relative,chap9_relative,chap10_relative,
	chap11_relative,chap12_relative,chap13_relative,chap14_relative,chap15_relative,
	chap16_relative,chap17_relative,chap18_relative,chap19_relative,chap20_relative,
	chap21_relative,chap22_relative,chap23_relative,chap24_relative,chap25_relative
	;
	TextView chap1_text,chap2_text,chap3_text,chap4_text,chap5_text,
	chap6_text,chap7_text,chap8_text,chap9_text,chap10_text,
	chap11_text,chap12_text,chap13_text,chap14_text,chap15_text,
	chap16_text,chap17_text,chap18_text,chap19_text,chap20_text,
	chap21_text,chap22_text,chap23_text,chap24_text,chap25_text
	;
	LinearLayout linearlayoutarr [] = null;
	RelativeLayout relativelayoutarr [] = null;
	TextView textlayoutarr [] = null;
	String subject ="";
	String check = "";
	String[] biology_chaptitle = {

			"Chapter One",
			"Chapter Two",
			"Chapter Three",
			"Chapter Four",
			"Chapter Five",
			"Chapter Six",
			"Chapter Seven",
			"Chapter Eight",
			"Chapter Nine",
			"Chapter Ten"



	};
	/*String[] biology_chapdetail = {"Separation Techniques","Particulate Nature Of Matter","Three Chemical Laws"," Chemical Combination I",

			"Five Chemical Symbols, Formulae And Equation","Gas Laws","Acids , Bases And Salts","Carbon And Its Compounds","Organic Chemistry I (Taken From Ss3)","Industrial Chemistry"

	};*/
	String[] biology_chapdetail = {"Biology and Living Organisms","Nutrition In Animals I","Nutrition In Animals II","Nutrition In Animals III",

			"Basic Ecological Concepts","Functional Ecosystem","Population Studies","The Cell and Its Environment","Classification Of Living Organisms I","Local Biotic Communities Or Biomes In Nigeria"

	};

	String[] yoruba_chaptitle = {

			"Chapter One",
			"Chapter Two",
			"Chapter Three",
			"Chapter Four",
			"Chapter Five",
			"Chapter Six",
			"Chapter Seven",
			"Chapter Eight",
			"Chapter Nine",
			"Chapter Ten",
			"Chapter Eleven",
			"Chapter Twelve ",
			"Chapter Thirteen",
			"Chapter Fourteen",
			"Chapter Fifteen",
			"Chapter Sixteen",
			"Chapter Seventeen",
			"Chapter Eighteen",
			"Chapter Nineteen",
			"Chapter Twenty",
			"Chapter Twenty One",
			"Chapter Twenty Two",
			"Chapter Twenty Three",
			"Chapter Twenty Four",
			"Chapter Twenty Five"
	};

	String[] yoruba_chapdetail = {"Eko Ile","Atunyewo Awon lro Ede","Litereso Itan Oloro Geere","Awon Ounje lle Yoruba","Atunyewo Silebu",

			"Asayan Ewi Alohun Fun Itupale","Awon Ise Abinibi Ile Yoruba","lhun Oro Ati lseda Oro","Itupale Ewi Apileko","Asayan Ewi Alohun Fun Itupale","Owe Ati Akanlo Ede","Oyun Nini Ati ltoju Oyun","Ede Akaye","Litereso: Itupale lwe Ere Onitan Apoti Alakara"
			,"test","Asayan Ewi Alohun Fun Itupale","Awon Ise Abinibi Ile Yoruba","Asayan Ewi Alohun Fun Itupale","Awon Ise Abinibi Ile Yoruba",
			"Asayan Ewi Alohun Fun Itupale","Awon Ise Abinibi Ile Yoruba","Asayan Ewi Alohun Fun Itupale","Awon Ise Abinibi Ile Yoruba","Asayan Ewi Alohun Fun Itupale",
	};
	String[] irk_chaptitle = {

			"Chapter One",
			"Chapter Two",
			"Chapter Three",
			"Chapter Four",
			"Chapter Five",
			"Chapter Six",
			"Chapter Seven",
			"Chapter Eight",
			"Chapter Nine",
			"Chapter Ten",
			"Chapter Eleven",
			"Chapter Twelve ",
			"Chapter Thirteen",
			"Chapter Fourteen",
			"Chapter Fifteen",
			"Chapter Sixteen",
			"Chapter Seventeen",
			"Chapter Eighteen",
			"Chapter Nineteen",
			"Chapter Twenty",
			"Chapter Twenty One",
			"Chapter Twenty Two",
			"Chapter Twenty Three"
	};
	String[] irk_chapdetail = {
			"The Ouran: The Revelation, Recording And Memorization Of The Ouran During The Lifetime Of The Prophet (Saw) I",
			"The Quran: Compilation And Standardization Of The Glorious Quran",
			" The Ouran: Distinction Between Makkah And Madinah Suwar",
			"Quran: Importance Of The Glorious Quran Religious, Moral, Spiritual, Social-Political And Economic significance I"
			,"Reading In Arabic, Trasliteration, Translation And Brief Commentary Onsuratul Nfatiah O: 1"
			,"Reading In Arabic, Translation And Brief Commentarysuratul Shams (Q: 91)"
			,"Reading In Arabic, Translitration, Translation And Commentarv On: Suratul Tin (Ouran 95). Suratul Alaa"
			,"Three selected Suwrar (2)","Definition and importance of hadith","Categorisation of Hadith","Collections of Hadith",
			"TAFSIR OF THE QUR'AN: DEFINITION, KINDS, SOURCES AND IMPORTANCE",
			"AN-NAWAWI'S COLLECTION(1)","AN-NAWAWI'S COLLECTION(2)","KALIMATUSH-SHAHADAH",
			"SHIRK","THE CONCEPT OF 'IBADAH","TAHARAH AND SALAH IN 'IBADAH","ZAKAT","ARABIA BEFORE ISLAM","QUALITIES OF PROPHET MUHAMMAD (S.A.W)"
			,"SIRAH 3 - The Guided Caliphs","TARIKH"
	};
	String[] englishlang_chaptitle = {

			"Chapter One",
			"Chapter Two",
			"Chapter Three",
			"Chapter Four",
			"Chapter Five",
			"Chapter Six"
	};
	String[] englishlang_chapdetail = 
		{
			"Vocalbulary Development: Register","Oral English","Comprehension: Listening and Reading",
			"Essay Writing","Grammar","Summary"
		};

	String[] government_chaptitle = {

			"Chapter One",
			"Chapter Two",
			"Chapter Three",
			"Chapter Four",
			"Chapter Five",
			"Chapter Six",
			"Chapter Seven",
			"Chapter Eight",
			"Chapter Nine",
			"Chapter Ten",
			"Chapter Eleven",
	"Chapter Twelve"};
	String[] government_chapdetail = 
		{
			"Defining Government","Basic Concepts Of Government I","Basic Concepts Of Government II",
			"Consitutions And Constitutionalism","Types And Characteristics Of Government I","Consitutions And Constitutionalism","Types And Characteristics Of Government II"
			,"The Structure And Organization Of Government","Rule Of Law I","Rule Of Law II","CitizenShip","Political Parties",
			"Pressure Groups"
		};
	String[] geography_chaptitle = {
			"Chapter One",
			"Chapter Two",
			"Chapter Three",
			"Chapter Four",
			"Chapter Five",
			"Chapter Six",
			"Chapter Seven",
			"Chapter Eight",
			"Chapter Nine",
			"Chapter Ten",
			"Chapter Eleven",
			"Chapter Twelve ",
			"Chapter Thirteen",
			"Chapter Fourteen",
			"Chapter Fifteen",
			"Chapter Sixteen",
			"Chapter Seventeen",
			"Chapter Eighteen",
			"Chapter Nineteen"
	};
	String[] geography_chapdetail = 
		{
			"Introduction to Geography","Local Geography","The Earth and the Solar System I","The Earth and the Solar System II"
			,"The Earth and the Solar System III","The Earth and the Solar System IV","The Earth and the Solar System V","The Earth and the Solar System VI",
			"Environment and its Resources I","Environment and its Resources II","Environment and its Resources III"
			,"Economic and Human Geography I","Economic and Human Geography II","Economic and Human Geography II",
			"World Manufacturing industries","Intrduction to map reading*","Map measurements","Map reduction and enlargement"
			,"Introduction to Geographic"
		};

	String[] history_chaptitle = {
			"Chapter One",
			"Chapter Two",
			"Chapter Three",
			"Chapter Four",
			"Chapter Five",
			"Chapter Six",
			"Chapter Seven",
			"Chapter Eight",
			"Chapter Nine",
			"Chapter Ten",
			"Chapter Eleven",
			"Chapter Twelve ",
			"Chapter Thirteen",
			"Chapter Fourteen",
			"Chapter Fifteen"
	};
	String[] history_chapdetail = 
		{ 
			"Introduction To History I","Introduction To History II","Introduction To History III",
			"Pre-Colonial Nigeria I","Pre-Colonial Nigeria II","Pre-Colonial Nigeria III","Pre-Colonial Nigeria IV",
			"Pre-Colonial Nigeria V","Pre-Colonial Nigeria VI","Pre-Colonial Nigeria VII","Pre-Colonial Nigeria VIII",
			"Pre-Colonial Nigeria IX","Pre-Colonial Nigeria X","Pre-Colonial Nigeria XI","Pre-Colonial Nigeria XII",
			"Pre-Colonial Nigeria XIII","Pre-Colonial Nigeria XIV","Pre-Colonial Nigeria XV"
		};


	String[] mathematics_chaptitle = {

			"Chapter One",
			"Chapter Two",
			"Chapter Three",
			"Chapter Four",
			"Chapter Five",
			"Chapter Six",
			"Chapter Seven",
			"Chapter Eight",
			"Chapter Nine",
			"Chapter Ten",
			"Chapter Eleven",
			"Chapter Twelve","Chapter Thirteen","Chapter Fourteen"
	};
	String[] mathematics_chapdetail = 
		{
			"Number Base System","Modular Arithmetic","Indices and Lagarithms I",
			"Indices and Lagarithms II","Sets","Simle Equations and Variation",
			"Solving Linear and Quadratic Equations","Logical Reasoning","Geometric Construction",
			"Proof of some Basic Theorems","Trignometric Ratios I",
			"Proof of some Basic Theorems","Trignometric Ratios II",
			"Mensuration"
		};
	String[] commerce_chaptitle = {
			"Chapter One",
			"Chapter Two",
			"Chapter Three",
			"Chapter Four",
			"Chapter Five",
			"Chapter Six",
			"Chapter Seven",
			"Chapter Eight",
			"Chapter Nine",
			"Chapter Ten",
			"Chapter Eleven",
			"Chapter Twelve ",
			"Chapter Thirteen",
			"Chapter Fourteen",
			"Chapter Fifteen",
			"Chapter Sixteen",
			"Chapter Seventeen",
			"Chapter Eighteen",
			"Chapter Nineteen",
			"Chapter Twenty",
			"Chapter Twenty One",
			"Chapter Twenty Two",
			"Chapter Twenty Three",
			"Chapter Twenty Four",
			"Chapter Twenty Five"
	};
	String[] commerce_chapdetail = 
		{
			"Introduction to Commerce",
			"History Of Commerce I","History Of Commerce II",
			"Occupation","Production,Division Of Labour,Specialization And Exchange I",
			"Production,Division Of Labour,Specialization And Exchange II",
			"Home Trade I",
			"Home Trade II","Home Trade III","Home Trade IV","Home Trade V","Home Trade VI"
			,"Home Trade VII",//13
			"The Large Scale Retailers I","The Large Scale Retailers II",
			"Some modern trends in retailing I",
			"Some modern trends in retailing II",
			"Home Trade: The Whole Seller",
			"Internation Trade I",
			"Internation Trade II: Balance of Trade and Balance of Payment","International Trade III :Procedure",
			"Internation Trade IV : Main Documents Involved",
			"Business Organization or Business Units I",
			"Business Organization II",
			"Money"//25
			//"Aids to Commerce","Commercial Banks"
		};
	String[] crs_chaptitle = {

			"Chapter One",
			"Chapter Two",
			"Chapter Three",
			"Chapter Four",
			"Chapter Five",
			"Chapter Six",
			"Chapter Seven",
			"Chapter Eight",
			"Chapter Nine",
			"Chapter Ten",
			"Chapter Eleven",
			"Chapter Twelve","Chapter Thirteen","Chapter Fourteen",
			"Chapter Fifteen",
			"Chapter Sixteen",
			"Chapter Seventeen",
			"Chapter Eighteen",
			"Chapter Nineteen",
			"Chapter Twenty",
	};
	String[] crs_chapdetail = 
		{ 
			"Jesus Teaching About Himself","Signs Of Jesus(New)","Love","Justification By Faith I",
			"Justification By Faith II","New Life In Christ","Jesus,The Son Of God","Humility","Forgiveness",
			"Spiritual  Gifts","Christian Living In the Community I","Civic Responsibitlity","Diginity Of Labour",
			"The Family","The Second Coming","Resurrection","Faith and Works","Impartiality","Effective Prayers"
			,"Christian Living in the Community"
		};
	String[] economics_chaptitle = {

			"Chapter One",
			"Chapter Two",
			"Chapter Three",
			"Chapter Four",
			"Chapter Five",
			"Chapter Six",
			"Chapter Seven",
			"Chapter Eight",
			"Chapter Nine",
			"Chapter Ten",
			"Chapter Eleven",
			"Chapter Twelve","Chapter Thirteen","Chapter Fourteen",
			"Chapter Fifteen"
	};
	String[] economics_chapdetail = 
		{
			"Scope of Economics","Basic Tools for Economic Analysis",
			"Concepts of Demand and Supply","Economic Systems I","Production Theory of Labour and Specialization",
			"Business Organisation","Instrument Of Business Finance","Population","Labour Market","Distributive Channels","Money I",
			"Meaning of Financial Institution","Agriculture","Nature of Nigerian Economy","Mining"
		};
	String[] englishliterature_chaptitle = {

			"Chapter One",
			"Chapter Two",
			"Chapter Three",
			"Chapter Four",
			"Chapter Five",
			"Chapter Six",
			"Chapter Seven",
			"Chapter Eight",
			"Chapter Nine"
	};
	String[] englishliterature_chapdetail = 
		{ 
			"All Literary principles/Rudiments of Literature I",
			"All Literary principles/Rudiments of Literature II",
			"All Literary principles/Rudiments of Literature III",
			"All Literary principles/Rudiments of Literature IV",
			"All Literary principles/Rudiments of Literature V",
			"African Poetry I",
			"African and non-African Drama I",
			"African and non-African Drama II",
			"African Prose And Non African Prose I"
			};
	String[] agriculture_chaptitle = {

			"Chapter One",
			"Chapter Two",
			"Chapter Three",
			"Chapter Four",
			"Chapter Five",
			"Chapter Six",
			"Chapter Seven",
			"Chapter Eight",
			"Chapter Nine",
			"Chapter Ten",
			"Chapter Eleven",
			"Chapter Twelve ",
			"Chapter Thirteen",
			"Chapter Fourteen",
			"Chapter Fifteen",
			"Chapter Sixteen",
			"Chapter Seventeen",
			"Chapter Eighteen"
	};
	String[] agriculture_chapdetail = 
		{
   "Agricultural Development in Nigeria I","Agricultural Development in Nigeria II","Agricultural Development in Nigeria III",
   "Roles Of Govenrment And Non -Governmental Organization In Agricultural Development","Agricultural Law And Reforms",
   "Agricultural Ecology- Land And its Uses I","Agricultural Ecology- Land And its Uses II",
   "Agro-Allied Industries and the relationship between Agriculture and Industry","Environmental Factors Affecting Agricutural Production I",
   "Rock Formation","Formation, Composition and Propertise Of Soil","Farm Machinery and Implements I",
   "Farm Machinery and Implements II","Farm Mechanization","Sources of Farm Power","Classification of Crops",
   "Husbandry of Selected Crops","Pasture and Forage Crops"
   
   
		} ;
	String[] chemistry_chaptitle = {

			"Chapter One",
			"Chapter Two",
			"Chapter Three",
			"Chapter Four",
			"Chapter Five",
			"Chapter Six",
			"Chapter Seven",
			"Chapter Eight",
			"Chapter Nine",
			"Chapter Ten"
	};
	String[] chemistry_chapdetail = 
		{
	"Separation Techniques","Particulate Nature Of Matter","Chemical Laws",
	"Chemical Combination I","Chemical Symbols, Formulae And Equation",
	"Gas Laws","Acids, Bases And Salts","Carbon And Its Compound",
	"Organic Chemistry I (Takem From Ss3)","Industrial Chemistry"
			
		};
	
	String[] furthermath_chaptitle = {

			"Chapter One",
			"Chapter Two",
			"Chapter Three",
			"Chapter Four",
			"Chapter Five",
			"Chapter Six",
			"Chapter Seven",
			"Chapter Eight",
			"Chapter Nine","Chapter Ten","Chapter Elevan","Chapter Twelve"
	};
	String[] furthermath_chapdetail = 
		{  
			"Sets","Indices","Matrices and Determinant","Polynomial","Differentiation",
			"The Straight Line","Vectors I","Measure of Locations","Permutation And Combination"
			,"Probability","Variance"
		};
	String[] physics_chaptitle = {
			"Chapter One",
			"Chapter Two",
			"Chapter Three",
			"Chapter Four",
			"Chapter Five",
			"Chapter Six",
			"Chapter Seven",
			"Chapter Eight",
			"Chapter Nine",
			"Chapter Ten",
			"Chapter Eleven",
			"Chapter Twelve ",
			"Chapter Thirteen",
			"Chapter Fourteen"
	};
	String[] physics_chapdetail = 
		{ 
			"Introduction To Physics And Physical Quantities",
			"Position,Distance and Displacement","Motion I (force and motion)"
			,"Motion II (cicular motion)","Work, Energy and Power","Heat Energy I",
			"Heat Energy II","Heat Energy III","Field Description, Propertise of Fields and Gravitational Fields",
			"Electric Charges","Electrical Fields","Particulate Nature Of Matter","Fluids at Rest and in Motion"
			,"Solar Energy"
		}
	;
	String type = "";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu);

		Intent in = getIntent();
		if(!in.getStringExtra("subject").isEmpty() && !in.getStringExtra("check").isEmpty())
		{	subject = in.getStringExtra("subject").toString();
			check = in.getStringExtra("check").toString();
			initialize();
			setOnclicklistner();
			hide_alllayouts();
			selection(subject);
		
		}

	}
	private void selection(String s)
	{
		if(s.equals("biology")) {
			selectbiology(); }
		else if(s.equals("yoruba"))
		{
			selectyoruba();
		}
		else if(s.equals("irk"))
		{
			selectirk();
		}
		else if(s.equals("englishlanguage"))
		{
			selectenglang();
		}
		else if(s.equals("government"))
		{
			selectgovernment();
		}
		else if(s.equals("geography"))
		{
			selectgeography();
		}
		else if(s.equals("history"))
		{
			selecthistory();
		}
		else if(s.equals("mathematics"))
		{
			selectmathematics();
		}
		else if (s.equals("commerce"))
		{
			selectcommerce();
		}
		else if (s.equals("crs"))
		{
			selectcrs();
		}
		else if (s.equals("economics"))
		{
			selecteconomics();
		}
		else if (s.equals("englishliterature"))
		{
			selectenglishliterature();
		}
		else if (s.equals("agriculture"))
		{
			selectagriculture();
		}
		else if (s.equals("chemistry"))
		{
			selectchemistry();
		}
		else if(s.equals("furthermath"))
		{
			selectfurthermath();
		}
		else if(s.equals("physics"))
		{
			selectphysics();
		}

	}
	private void selectphysics()
	{
		for (int i=0;i< physics_chapdetail.length;i++)
		{
			textlayoutarr[i].setText(physics_chapdetail[i].toString());
			linearlayoutarr[i].setVisibility(View.VISIBLE);
		}	
	}
	private void selectfurthermath() {
		for (int i=0;i<furthermath_chapdetail.length;i++)
		{
			textlayoutarr[i].setText(furthermath_chapdetail[i].toString());
			linearlayoutarr[i].setVisibility(View.VISIBLE);
		}	
	}
	private void selectchemistry() {
		for (int i=0;i<chemistry_chapdetail.length;i++)
		{
			textlayoutarr[i].setText(chemistry_chapdetail[i].toString());
			linearlayoutarr[i].setVisibility(View.VISIBLE);
		}	
	}
	
	private void selectagriculture() {
		for (int i=0;i<agriculture_chapdetail.length;i++)
		{
			textlayoutarr[i].setText(agriculture_chapdetail[i].toString());
			linearlayoutarr[i].setVisibility(View.VISIBLE);
		}	
	}
	private void selectenglishliterature() {
		for (int i=0;i<englishliterature_chapdetail.length;i++)
		{
			textlayoutarr[i].setText(englishliterature_chapdetail[i].toString());
			linearlayoutarr[i].setVisibility(View.VISIBLE);
		}	
	}
	private void selecteconomics() {
		for (int i=0;i<economics_chapdetail.length;i++)
		{
			textlayoutarr[i].setText(economics_chapdetail[i].toString());
			linearlayoutarr[i].setVisibility(View.VISIBLE);
		}	
	}
	private void selectcrs() {
		for (int i=0;i<crs_chapdetail.length;i++)
		{
			textlayoutarr[i].setText(crs_chapdetail[i].toString());
			linearlayoutarr[i].setVisibility(View.VISIBLE);
		}	
	}
	
	private void selectcommerce() {
		for (int i=0;i<commerce_chapdetail.length;i++)
		{
			textlayoutarr[i].setText(commerce_chapdetail[i].toString());
			linearlayoutarr[i].setVisibility(View.VISIBLE);
		}	
	}
	private void selectmathematics() {
		for (int i=0;i< mathematics_chapdetail.length;i++)
		{
			textlayoutarr[i].setText(mathematics_chapdetail[i].toString());
			linearlayoutarr[i].setVisibility(View.VISIBLE);
		}	
	}
	private void selecthistory() {
		for (int i=0;i< history_chapdetail.length;i++)
		{
			textlayoutarr[i].setText(history_chapdetail[i].toString());
			linearlayoutarr[i].setVisibility(View.VISIBLE);
		}	
	}
	private void selectgeography() {
		for (int i=0;i< geography_chapdetail.length;i++)
		{
			textlayoutarr[i].setText(geography_chapdetail[i].toString());
			linearlayoutarr[i].setVisibility(View.VISIBLE);
		}	
	}
	private void selectgovernment() {
		for (int i=0;i< government_chapdetail.length;i++)
		{
			textlayoutarr[i].setText(government_chapdetail[i].toString());
			linearlayoutarr[i].setVisibility(View.VISIBLE);
		}	
	}
	private void selectenglang() {
		for (int i=0;i< englishlang_chapdetail.length;i++)
		{
			textlayoutarr[i].setText(englishlang_chapdetail[i].toString());
			linearlayoutarr[i].setVisibility(View.VISIBLE);
		}	

	}
	private void selectirk() {
		for (int i=0;i< irk_chapdetail.length;i++)
		{
			textlayoutarr[i].setText(irk_chapdetail[i].toString());
			linearlayoutarr[i].setVisibility(View.VISIBLE);
		}	

	}
	private void selectyoruba() {
		for (int i=0;i< yoruba_chapdetail.length;i++)
		{
			textlayoutarr[i].setText(yoruba_chapdetail[i].toString());
			linearlayoutarr[i].setVisibility(View.VISIBLE);
		}	

	}
	private void hide_alllayouts() {

		chap1_linear.setVisibility(View.INVISIBLE);
		chap2_linear.setVisibility(View.INVISIBLE);
		chap3_linear.setVisibility(View.INVISIBLE);
		chap4_linear.setVisibility(View.INVISIBLE);
		chap5_linear.setVisibility(View.INVISIBLE);
		chap6_linear.setVisibility(View.INVISIBLE);
		chap7_linear.setVisibility(View.INVISIBLE);
		chap8_linear.setVisibility(View.INVISIBLE);
		chap9_linear.setVisibility(View.INVISIBLE);
		chap10_linear.setVisibility(View.INVISIBLE);
		chap11_linear.setVisibility(View.INVISIBLE);
		chap12_linear.setVisibility(View.INVISIBLE);
		chap13_linear.setVisibility(View.INVISIBLE);
		chap14_linear.setVisibility(View.INVISIBLE);
		chap15_linear.setVisibility(View.INVISIBLE);
		chap16_linear.setVisibility(View.INVISIBLE);
		chap17_linear.setVisibility(View.INVISIBLE);
		chap18_linear.setVisibility(View.INVISIBLE);
		chap19_linear.setVisibility(View.INVISIBLE);
		chap20_linear.setVisibility(View.INVISIBLE);
		chap21_linear.setVisibility(View.INVISIBLE);
		chap22_linear.setVisibility(View.INVISIBLE);
		chap23_linear.setVisibility(View.INVISIBLE);
		chap24_linear.setVisibility(View.INVISIBLE);
		chap25_linear.setVisibility(View.INVISIBLE);
		;
	}
	private void setOnclicklistner() {
		chap1_relative.setOnClickListener(this);
		chap2_relative.setOnClickListener(this);
		chap3_relative.setOnClickListener(this);
		chap4_relative.setOnClickListener(this);
		chap5_relative.setOnClickListener(this);
		chap6_relative.setOnClickListener(this);
		chap7_relative.setOnClickListener(this);
		chap8_relative.setOnClickListener(this);
		chap9_relative.setOnClickListener(this);
		chap10_relative.setOnClickListener(this);
		chap11_relative.setOnClickListener(this);
		chap12_relative.setOnClickListener(this);
		chap13_relative.setOnClickListener(this);
		chap14_relative.setOnClickListener(this);
		chap15_relative.setOnClickListener(this);
		chap16_relative.setOnClickListener(this);
		chap17_relative.setOnClickListener(this);
		chap18_relative.setOnClickListener(this);
		chap19_relative.setOnClickListener(this);
		chap20_relative.setOnClickListener(this);
		chap21_relative.setOnClickListener(this);
		chap22_relative.setOnClickListener(this);
		chap23_relative.setOnClickListener(this);
		chap24_relative.setOnClickListener(this);
		chap25_relative.setOnClickListener(this);

	}
	private void initialize() {
		chap1_linear = (LinearLayout)findViewById(R.id.chapter1_linear);
		chap2_linear = (LinearLayout)findViewById(R.id.chapter2_linear);
		chap3_linear = (LinearLayout)findViewById(R.id.chapter3_linear);
		chap4_linear = (LinearLayout)findViewById(R.id.chapter4_linear);
		chap5_linear = (LinearLayout)findViewById(R.id.chapter5_linear);
		chap6_linear = (LinearLayout)findViewById(R.id.chapter6_linear);
		chap7_linear = (LinearLayout)findViewById(R.id.chapter7_linear);
		chap8_linear = (LinearLayout)findViewById(R.id.chapter8_linear);
		chap9_linear = (LinearLayout)findViewById(R.id.chapter9_linear);
		chap10_linear = (LinearLayout)findViewById(R.id.chapter10_linear);
		chap11_linear = (LinearLayout)findViewById(R.id.chapter11_linear);
		chap12_linear = (LinearLayout)findViewById(R.id.chapter12_linear);
		chap13_linear = (LinearLayout)findViewById(R.id.chapter13_linear);
		chap14_linear = (LinearLayout)findViewById(R.id.chapter14_linear);
		chap15_linear = (LinearLayout)findViewById(R.id.chapter15_linear);
		chap16_linear = (LinearLayout)findViewById(R.id.chapter16_linear);
		chap17_linear = (LinearLayout)findViewById(R.id.chapter17_linear);
		chap18_linear = (LinearLayout)findViewById(R.id.chapter18_linear);
		chap19_linear = (LinearLayout)findViewById(R.id.chapter19_linear);
		chap20_linear = (LinearLayout)findViewById(R.id.chapter20_linear);
		chap21_linear = (LinearLayout)findViewById(R.id.chapter21_linear);
		chap22_linear = (LinearLayout)findViewById(R.id.chapter22_linear);
		chap23_linear = (LinearLayout)findViewById(R.id.chapter23_linear);
		chap24_linear = (LinearLayout)findViewById(R.id.chapter24_linear);
		chap25_linear = (LinearLayout)findViewById(R.id.chapter25_linear);

		chap1_relative = (RelativeLayout)findViewById(R.id.chapter1text_relative);
		chap2_relative = (RelativeLayout)findViewById(R.id.chapter2text_relative);
		chap3_relative= (RelativeLayout)findViewById(R.id.chapter3text_relative);
		chap4_relative= (RelativeLayout)findViewById(R.id.chapter4text_relative);
		chap5_relative= (RelativeLayout)findViewById(R.id.chapter5text_relative);
		chap6_relative= (RelativeLayout)findViewById(R.id.chapter6text_relative);
		chap7_relative= (RelativeLayout)findViewById(R.id.chapter7text_relative);
		chap8_relative= (RelativeLayout)findViewById(R.id.chapter8text_relative);
		chap9_relative= (RelativeLayout)findViewById(R.id.chapter9text_relative);
		chap10_relative = (RelativeLayout)findViewById(R.id.chapter10text_relative);
		chap11_relative = (RelativeLayout)findViewById(R.id.chapter11text_relative);
		chap12_relative = (RelativeLayout)findViewById(R.id.chapter12text_relative);
		chap13_relative = (RelativeLayout)findViewById(R.id.chapter13text_relative);
		chap14_relative = (RelativeLayout)findViewById(R.id.chapter14text_relative);
		chap15_relative = (RelativeLayout)findViewById(R.id.chapter15text_relative);
		chap16_relative = (RelativeLayout)findViewById(R.id.chapter16text_relative);
		chap17_relative= (RelativeLayout)findViewById(R.id.chapter17text_relative);
		chap18_relative= (RelativeLayout)findViewById(R.id.chapter18text_relative);
		chap19_relative= (RelativeLayout)findViewById(R.id.chapter19text_relative);
		chap20_relative= (RelativeLayout)findViewById(R.id.chapter20text_relative);
		chap21_relative= (RelativeLayout)findViewById(R.id.chapter21text_relative);
		chap22_relative= (RelativeLayout)findViewById(R.id.chapter22text_relative);
		chap23_relative= (RelativeLayout)findViewById(R.id.chapter23text_relative);
		chap24_relative= (RelativeLayout)findViewById(R.id.chapter24text_relative);
		chap25_relative= (RelativeLayout)findViewById(R.id.chapter25text_relative);

		chap1_text = (TextView)findViewById(R.id.chapter1text);
		chap2_text = (TextView)findViewById(R.id.chapter2text);
		chap3_text = (TextView)findViewById(R.id.chapter3text);
		chap4_text = (TextView)findViewById(R.id.chapter4text);
		chap5_text = (TextView)findViewById(R.id.chapter5text);
		chap6_text = (TextView)findViewById(R.id.chapter6text);
		chap7_text = (TextView)findViewById(R.id.chapter7text);
		chap8_text = (TextView)findViewById(R.id.chapter8text);
		chap9_text = (TextView)findViewById(R.id.chapter9text);
		chap10_text = (TextView)findViewById(R.id.chapter10text);
		chap11_text = (TextView)findViewById(R.id.chapter11text);
		chap12_text = (TextView)findViewById(R.id.chapter12text);
		chap13_text = (TextView)findViewById(R.id.chapter13text);
		chap14_text = (TextView)findViewById(R.id.chapter14text);
		chap15_text = (TextView)findViewById(R.id.chapter15text);
		chap16_text = (TextView)findViewById(R.id.chapter16text);
		chap17_text = (TextView)findViewById(R.id.chapter17text);
		chap18_text = (TextView)findViewById(R.id.chapter18text);
		chap19_text = (TextView)findViewById(R.id.chapter19text);
		chap20_text = (TextView)findViewById(R.id.chapter20text);
		chap21_text = (TextView)findViewById(R.id.chapter21text);
		chap22_text = (TextView)findViewById(R.id.chapter22text);
		chap23_text = (TextView)findViewById(R.id.chapter23text);
		chap24_text = (TextView)findViewById(R.id.chapter24text);
		chap25_text = (TextView)findViewById(R.id.chapter25text);
		relativelayoutarr = new RelativeLayout[25];
		relativelayoutarr[0] = chap1_relative;
		relativelayoutarr[1] = chap2_relative;
		relativelayoutarr[2] = chap3_relative;
		relativelayoutarr[3] = chap4_relative;
		relativelayoutarr[4] = chap5_relative;
		relativelayoutarr[5] = chap6_relative;
		relativelayoutarr[6] = chap7_relative;
		relativelayoutarr[7] = chap8_relative;
		relativelayoutarr[8] = chap9_relative;
		relativelayoutarr[9] = chap10_relative;
		relativelayoutarr[10] = chap11_relative;
		relativelayoutarr[11] = chap12_relative;
		relativelayoutarr[12] = chap13_relative;
		relativelayoutarr[13] = chap14_relative;
		relativelayoutarr[14] = chap15_relative;
		relativelayoutarr[15] = chap16_relative;
		relativelayoutarr[16] = chap17_relative;
		relativelayoutarr[17] = chap18_relative;
		relativelayoutarr[18] = chap19_relative;
		relativelayoutarr[19] = chap20_relative;
		relativelayoutarr[20] = chap21_relative;
		relativelayoutarr[21] = chap22_relative;
		relativelayoutarr[22] = chap23_relative;
		relativelayoutarr[23] = chap24_relative;
		relativelayoutarr[24] = chap25_relative;
		linearlayoutarr = new LinearLayout[25];
		linearlayoutarr[0] = chap1_linear;
		linearlayoutarr[1] = chap2_linear;
		linearlayoutarr[2] =chap3_linear;
		linearlayoutarr[3] = chap4_linear;
		linearlayoutarr[4] =	chap5_linear;
		linearlayoutarr[5] =chap6_linear;
		linearlayoutarr[6] =chap7_linear;
		linearlayoutarr[7] =chap8_linear;
		linearlayoutarr[8] =chap9_linear;
		linearlayoutarr[9] =		chap10_linear;
		linearlayoutarr[10] =chap11_linear;
		linearlayoutarr[11] =chap12_linear;
		linearlayoutarr[12] =chap13_linear;
		linearlayoutarr[13] =chap14_linear;
		linearlayoutarr[14] =chap15_linear;
		linearlayoutarr[15] =chap16_linear;
		linearlayoutarr[16] =chap17_linear;
		linearlayoutarr[17] =chap18_linear;
		linearlayoutarr[18] =chap19_linear;
		linearlayoutarr[19] =chap20_linear;
		linearlayoutarr[20] =chap21_linear;
		linearlayoutarr[21] =chap22_linear;
		linearlayoutarr[22] =chap23_linear;
		linearlayoutarr[23] =chap24_linear;
		linearlayoutarr[24] = chap25_linear;
		textlayoutarr = new TextView[25];
		textlayoutarr[0] =chap1_text;
		textlayoutarr[1] =chap2_text;
		textlayoutarr[2] =chap3_text;
		textlayoutarr[3] =chap4_text;
		textlayoutarr[4] =chap5_text;
		textlayoutarr[5] =chap6_text;
		textlayoutarr[6] =chap7_text;
		textlayoutarr[7] =chap8_text;
		textlayoutarr[8] =chap9_text;
		textlayoutarr[9] =chap10_text;
		textlayoutarr[10] =chap11_text;
		textlayoutarr[11] =chap12_text;
		textlayoutarr[12] =chap13_text;
		textlayoutarr[13] =chap14_text;
		textlayoutarr[14] =chap15_text;
		textlayoutarr[15] =chap16_text;
		textlayoutarr[16] =chap17_text;
		textlayoutarr[17] =chap18_text;
		textlayoutarr[18] =chap19_text;
		textlayoutarr[19] =chap20_text;
		textlayoutarr[20] =chap21_text;
		textlayoutarr[21] =chap22_text;
		textlayoutarr[22] =chap23_text;
		textlayoutarr[23] =chap24_text;
		textlayoutarr[24] = chap25_text;


	}
	private void selectbiology() {
		for (int i =0;i< biology_chaptitle.length;i++)
		{
			textlayoutarr[i].setText(biology_chapdetail[i].toString());
			linearlayoutarr[i].setVisibility(View.VISIBLE);
		}	
	}
	@Override
	public void onClick(View v) {
		Intent in =null;
		switch (v.getId()) 
		{
		case R.id.chapter1text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "1");
				in.putExtra("check", check);	
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "1");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "1");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
			in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "1");
			in.putExtra("check", check);	
			}
			break;
		case R.id.chapter2text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("chapter", "2");
				in.putExtra("value", "1");
				in.putExtra("check", check);	
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "2");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "2");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
				in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "2");
			in.putExtra("check", check);
			}
			break;
		case R.id.chapter3text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("check", check);
				in.putExtra("chapter", "3");
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "3");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "3");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
				in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "3");
			in.putExtra("check", check); }
			break;
		case R.id.chapter4text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("check", check);
				in.putExtra("chapter", "4");
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "4");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "4");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "4");
			
			in.putExtra("check", check); }
			break;
		case R.id.chapter5text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "5");
				in.putExtra("check", check);	
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "5");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "5");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
					in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "5");
			in.putExtra("check", check);}
			break;
		case R.id.chapter6text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("check", check);
				in.putExtra("chapter", "6");
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "6");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "6");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
					in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "6");
			in.putExtra("check", check);}
			break;
		case R.id.chapter7text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "7");
				in.putExtra("check", check);	
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "7");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "7");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
					in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("check", check);
			in.putExtra("value", "7");}
			break;
		case R.id.chapter8text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("check", check);	
				in.putExtra("chapter", "8");
				}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "8");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "8");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
					in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "8");
			in.putExtra("check", check);}
			break;
		case R.id.chapter9text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "9");
				in.putExtra("check", check);	
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "9");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "9");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
					in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "9");
			in.putExtra("check", check);}
			break;
		case R.id.chapter10text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "10");
				in.putExtra("check", check);	
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "10");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "10");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
					in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "10");
			in.putExtra("check", check);}
			break;
		case R.id.chapter11text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "11");
				in.putExtra("check", check);	
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "11");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "11");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
					in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "11");
			in.putExtra("check", check); }
			break;
		case R.id.chapter12text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "12");
				
				in.putExtra("check", check);	
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "12");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "12");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
					in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "12");
			in.putExtra("check", check);
			}
			break;
		case R.id.chapter13text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("check", check);	
				in.putExtra("chapter", "13");
				}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "13");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "13");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
					in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "13");
			in.putExtra("check", check);}
			break;
		case R.id.chapter14text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("check", check);
				in.putExtra("chapter", "14");
				
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "14");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "14");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
					in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "14");
			in.putExtra("check", check);}
			break;
		case R.id.chapter15text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("check", check);
				in.putExtra("chapter", "15");
				
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "15");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "15");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
					in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "15");
			in.putExtra("check", check);}
			break;
		case R.id.chapter16text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("check", check);	
				in.putExtra("chapter", "16");
				
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "16");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "16");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
					in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "16");
			in.putExtra("check", check); }
			break;
		case R.id.chapter17text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("check", check);
				in.putExtra("chapter", "17");
				
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "17");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "17");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
					in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "17");
			in.putExtra("check", check); }
			break;
		case R.id.chapter18text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("check", check);
				in.putExtra("chapter", "18");
				
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "18");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "18");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
					in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "18");
			in.putExtra("check", check);}
			break;
		case R.id.chapter19text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "19");
				
				in.putExtra("check", check);	
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "19");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "19");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
					in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "19");
			in.putExtra("check", check);
			}break;
		case R.id.chapter20text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "20");
				
				in.putExtra("check", check);	
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "20");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "20");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
					in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("check", check);
			in.putExtra("value", "20");}
			break;
		case R.id.chapter21text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("check", check);
				in.putExtra("chapter", "21");
				
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "21");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "21");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
					in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "21");
			in.putExtra("check", check);}
			break;
		case R.id.chapter22text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "22");
				
				in.putExtra("check", check);	
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "22");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "22");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
					in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "22");
			in.putExtra("check", check);}
			break;
		case R.id.chapter23text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "23");
				
				in.putExtra("check", check);	
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "23");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "23");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
					in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "23");
			in.putExtra("check", check);}
			break;
		case R.id.chapter24text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "24");
				
				in.putExtra("check", check);	
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "24");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "24");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
					in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "24");
			in.putExtra("check", check);}
			break;
		case R.id.chapter25text_relative:
			if(check.equals("practice"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "25");
				
				in.putExtra("check", check);	
			}
			else if (type.equals("jamb"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "25");
				in.putExtra("type", "jamb");
				in.putExtra("check", check);	
				
			}
			else if (type.equals("waec"))
			{
				in = new Intent(Menu.this,Questions.class);
				in.putExtra("subject", subject);
				in.putExtra("value", "1");
				in.putExtra("chapter", "25");
				in.putExtra("type", "waec");
				in.putExtra("check", check);	
				
			}
			else {
					in = new Intent(Menu.this,AfterMenu.class);
			in.putExtra("subject", subject);
			in.putExtra("value", "25");
			in.putExtra("check", check);}
			break; 
		default:
			break;
		}
		startActivity(in);
	}
}
