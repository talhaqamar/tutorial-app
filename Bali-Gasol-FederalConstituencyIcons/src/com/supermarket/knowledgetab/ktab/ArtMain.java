package com.supermarket.knowledgetab.ktab;

import com.supermarket.knowledgetab.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

public class ArtMain extends Activity implements OnClickListener {
	LinearLayout biology,commerce,ccrs,economics,geography,englishlanguage,government
	,history,irk,englishliterature,calculator,yoruba;
	String check = "";

@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.artmain);

	
	Intent in = getIntent();
	if(!in.getStringExtra("check").isEmpty())
	{
	check = in.getStringExtra("check");	
	
	
}initialize();
setOnclicklistner(); }
private void setOnclicklistner() {
	 biology.setOnClickListener(this);
	 commerce.setOnClickListener(this);
	 ccrs.setOnClickListener(this);
	 economics.setOnClickListener(this);
	 geography.setOnClickListener(this);
	 englishlanguage.setOnClickListener(this);
	 government.setOnClickListener(this);
	 history.setOnClickListener(this);
	 irk.setOnClickListener(this);
	 englishliterature.setOnClickListener(this);
	 calculator.setOnClickListener(this);
	 yoruba.setOnClickListener(this);
	
	}
private void initialize() {
	 biology=(LinearLayout)findViewById(R.id.biology_linear);
	 commerce=(LinearLayout)findViewById(R.id.commerce_linear);
	 ccrs=(LinearLayout)findViewById(R.id.crs_linear);
	 economics=(LinearLayout)findViewById(R.id.economics_linear);
	 geography=(LinearLayout)findViewById(R.id.geography_linear);
	 englishlanguage=(LinearLayout)findViewById(R.id.englishlanguage_linear);
	 government=(LinearLayout)findViewById(R.id.government_linear);
	 history=(LinearLayout)findViewById(R.id.history_linear);
	 irk=(LinearLayout)findViewById(R.id.irk_linear);
	 englishliterature=(LinearLayout)findViewById(R.id.englishliterature_linear);
	 calculator=(LinearLayout)findViewById(R.id.mathematics_linear);
	 yoruba=(LinearLayout)findViewById(R.id.yoruba_linear);
	}
@Override
public void onClick(View v) {
	switch (v.getId()) {

	case R.id.biology_linear:
		Intent in = new Intent(ArtMain.this,Menu.class);
		in.putExtra("subject", "biology");
		in.putExtra("check", check);
		startActivity(in);
		break;
	case R.id.commerce_linear:
		Intent in1 = new Intent(ArtMain.this,Menu.class);
		in1.putExtra("subject", "commerce");
		in1.putExtra("check", check);
			startActivity(in1);
		break;
	case R.id.crs_linear:
		Intent in2 = new Intent(ArtMain.this,Menu.class);
		in2.putExtra("subject", "crs");
		in2.putExtra("check", check);
		startActivity(in2);
		break;
	case R.id.economics_linear:
		Intent in3 = new Intent(ArtMain.this,Menu.class);
		in3.putExtra("subject", "economics");
		in3.putExtra("check", check);
		startActivity(in3);
			break;
	case R.id.geography_linear:
		Intent in4 = new Intent(ArtMain.this,Menu.class);
		in4.putExtra("subject", "geography");
		in4.putExtra("check", check);
		startActivity(in4);
		break;
	case R.id.englishlanguage_linear:
		Intent in5 = new Intent(ArtMain.this,Menu.class);
		in5.putExtra("subject", "englishlanguage");
		in5.putExtra("check", check);
		startActivity(in5);
		break;
	case R.id.government_linear:
		Intent in6 = new Intent(ArtMain.this,Menu.class);
		in6.putExtra("subject", "government");
		in6.putExtra("check", check);
		
		startActivity(in6);
						break;
	case R.id.history_linear:
		Intent in66 = new Intent(ArtMain.this,Menu.class);
		in66.putExtra("subject", "history");
		in66.putExtra("check", check);
		
		startActivity(in66);
				break;
	case R.id.irk_linear:
		Intent in7 = new Intent(ArtMain.this,Menu.class);
		in7.putExtra("subject", "irk");
		in7.putExtra("check", check);
		
		startActivity(in7);						
	break;
	case R.id.englishliterature_linear:
		Intent in8 = new Intent(ArtMain.this,Menu.class);
		in8.putExtra("subject", "englishliterature");
		in8.putExtra("check", check);
		
		startActivity(in8);
		break;
case R.id.mathematics_linear:
	Intent in9 = new Intent(ArtMain.this,Menu.class);
	in9.putExtra("subject", "mathematics");
	in9.putExtra("check", check);
	
	startActivity(in9);
break;
case R.id.yoruba_linear:
	Intent in10 = new Intent(ArtMain.this,Menu.class);
	in10.putExtra("subject", "yoruba");
	
	in10.putExtra("check", check);
	startActivity(in10);		
break;
	default:
		break;
	}
}


}
