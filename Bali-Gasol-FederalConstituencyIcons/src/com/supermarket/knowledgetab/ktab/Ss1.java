package com.supermarket.knowledgetab.ktab;

import com.supermarket.knowledgetab.AfterExtraCurricular;
import com.supermarket.knowledgetab.MainActivity;
import com.supermarket.knowledgetab.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.Toast;

public class Ss1 extends Activity implements OnClickListener {
	LinearLayout art,science,social;
String check ="";
	@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.ss1);

	Intent in = getIntent();
	if(!in.getStringExtra("check").isEmpty())
	{
		check = in.getStringExtra("check");
	
	}
	initialize();
	setOnclicklistner();
}
private void setOnclicklistner() {
	art.setOnClickListener(this);
	science.setOnClickListener(this);
	social.setOnClickListener(this);
}
private void initialize() {
	art=(LinearLayout)findViewById(R.id.art_linear);
	science=(LinearLayout)findViewById(R.id.science_linear);
	social=(LinearLayout)findViewById(R.id.socialscience_linear);
	}
@Override
public void onClick(View v) {
	switch (v.getId()) {
	case R.id.art_linear:
		Intent in1 = new Intent(Ss1.this,ArtMain.class);
		in1.putExtra("check", check);
		startActivity(in1);
		break;
	case R.id.science_linear:
		Toast.makeText(Ss1.this, check.toString(), 5000).show();
		Intent in2 = new Intent(Ss1.this,ScienceMain.class);
		in2.putExtra("check", check);
		startActivity(in2);
		
		break;
	case R.id.socialscience_linear:
		Toast.makeText(Ss1.this, check.toString(), 5000).show();
		Intent in3 = new Intent(Ss1.this,SocialScienceMain.class);
		in3.putExtra("check", check);
		startActivity(in3);
		
		break;

	default:
		break;
	}
}


}
