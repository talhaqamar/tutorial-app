package com.supermarket.knowledgetab.ktab;

import com.supermarket.knowledgetab.R;
import com.supermarket.knowledgetab.R.id;
import com.supermarket.knowledgetab.R.layout;
import com.supermarket.knowledgetab.integrated.IntegratedMain;
import com.supermarket.knowledgetab.video.Video;
import com.supermarket.knowledgetab.video.VideoMain;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class KtabsMain extends Activity implements OnClickListener {
	LinearLayout ebook,video,integrated;
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.ktabsmain);

	initialize();
	setOnclicklistner();
}
private void setOnclicklistner() {
	ebook.setOnClickListener(this);
	video.setOnClickListener(this);
	integrated.setOnClickListener(this);
}
private void initialize() {
	ebook =(LinearLayout)findViewById(R.id.ebook_linear);
	video =(LinearLayout)findViewById(R.id.videotutorials_linear);
	integrated =(LinearLayout)findViewById(R.id.integrated_linear);
	}
@Override
public void onClick(View v) {
	switch (v.getId()) {
	case R.id.ebook_linear:
		Intent in1 = new Intent(KtabsMain.this,SelectionSS.class);
		in1.putExtra("check", "1");
		startActivity(in1);
		break;
	case R.id.videotutorials_linear:
		Intent in2 = new Intent(KtabsMain.this,VideoMain.class);
		//in2.putExtra("check", "2");
		startActivity(in2);
		break;
	case R.id.integrated_linear:
		Intent in3 = new Intent(KtabsMain.this,IntegratedMain.class);
		//in2.putExtra("check", "2");
		startActivity(in3);
		break;

	default:
		break;
	}
}


}
