package com.supermarket.knowledgetab;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class ExtraCurricularMain extends Activity implements OnClickListener {
	ImageView civil,sexual,computer,enterprise;
	
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.extracurricularlayout);

	civil = (ImageView)findViewById(R.id.civic);
	sexual = (ImageView)findViewById(R.id.sexual);
	computer = (ImageView)findViewById(R.id.computer);
	enterprise = (ImageView)findViewById(R.id.enterprise);
setonclicklistner();
}

private void setonclicklistner() {
	civil.setOnClickListener(this); 
	sexual.setOnClickListener(this);
	computer.setOnClickListener(this);
	enterprise.setOnClickListener(this);
}

@Override
public void onClick(View v) {
	Intent in = new Intent(ExtraCurricularMain.this,AfterExtraCurricular.class);
	switch (v.getId()) {
	case R.id.civic:
		in.putExtra("type", "civic");
		break;
	case R.id.sexual:
		in.putExtra("type", "sexual");		
		break;
	case R.id.computer:
		in.putExtra("type", "computer");
		break;
	case R.id.enterprise:
		in.putExtra("type", "enterprise");
		break;

	default:
		break;
	}
	startActivity(in);
}
}
