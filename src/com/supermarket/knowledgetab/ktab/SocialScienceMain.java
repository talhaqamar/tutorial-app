package com.supermarket.knowledgetab.ktab;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.supermarket.knowledgetab.R;

public class SocialScienceMain extends Activity  implements OnClickListener {
	LinearLayout  accounting,biology,commerce,crs,economics,englishlanguage,geography,
	government,history,irk,englishliterature,mathematics;
	
	String check = "";
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.socialscience);

	
	Intent in = getIntent();
	if(!in.getStringExtra("check").isEmpty())
	{
	check = in.getStringExtra("check");	
	

}
	initialize();
setOnclicklistner();	
}
private void setOnclicklistner() {
	 biology.setOnClickListener(this);
	 accounting.setOnClickListener(this);
	 biology.setOnClickListener(this);
	 commerce.setOnClickListener(this);
	 crs.setOnClickListener(this);
	 economics.setOnClickListener(this);
	 englishlanguage.setOnClickListener(this);
	 geography.setOnClickListener(this);
	 government.setOnClickListener(this);
	 history.setOnClickListener(this);
	 irk.setOnClickListener(this);
	 englishliterature.setOnClickListener(this);
	 mathematics.setOnClickListener(this);
	
	}
private void initialize() 
{
	 accounting = (LinearLayout)findViewById(R.id.accountingS_linear);
	 biology = (LinearLayout)findViewById(R.id.biologyS_linear);
	 commerce= (LinearLayout)findViewById(R.id.commerceS_linear);
	 crs= (LinearLayout)findViewById(R.id.crsS_linear);
	 economics= (LinearLayout)findViewById(R.id.economicsS_linear);
	 englishlanguage= (LinearLayout)findViewById(R.id.englishlanguageS_linear);
	 geography= (LinearLayout)findViewById(R.id.geographyS_linear);
	 government= (LinearLayout)findViewById(R.id.governmentS_linear);
	 history= (LinearLayout)findViewById(R.id.historyS_linear);
	 irk= (LinearLayout)findViewById(R.id.irkS_linear);
	 englishliterature= (LinearLayout)findViewById(R.id.englishliteratureS_linear);
	 mathematics= (LinearLayout)findViewById(R.id.mathematicsS_linear);
	
	}
@Override
public void onClick(View v) {
	Intent in10 = new Intent(SocialScienceMain.this,Menu.class);
	switch (v.getId()) {
	case  R.id.accountingS_linear:
		in10.putExtra("subject", "accounting");
		in10.putExtra("check", check);
	break;
	case  R.id.biologyS_linear:
		in10.putExtra("subject", "biology");
		in10.putExtra("check", check);
	break;
	case  R.id.commerceS_linear:
		in10.putExtra("subject", "commerce");
		in10.putExtra("check", check);
	break;
	case  R.id.crsS_linear:
		in10.putExtra("subject", "crs");
		in10.putExtra("check", check);
	break;
	case  R.id.economicsS_linear:
		in10.putExtra("subject", "economics");
		in10.putExtra("check", check);
	break;
	case  R.id.englishlanguageS_linear:
		in10.putExtra("subject", "englishlanguage");
		in10.putExtra("check", check);
	break;
	case  R.id.geographyS_linear:
		in10.putExtra("subject", "geography");
		in10.putExtra("check", check);
	break;
	case  R.id.governmentS_linear:
		in10.putExtra("subject", "government");
		in10.putExtra("check", check);
	break;
	case  R.id.historyS_linear:
		in10.putExtra("subject", "history");
		in10.putExtra("check", check);
	break;
	case  R.id.irkS_linear:
		in10.putExtra("subject", "irk");
		in10.putExtra("check", check);
	break;
	case  R.id.englishliteratureS_linear:
		in10.putExtra("subject", "englishliterature");
		in10.putExtra("check", check);
	break;
	case  R.id.mathematicsS_linear:
		in10.putExtra("subject", "mathematics");
		in10.putExtra("check", check);
	break;
	
	
	default:
		break;
	}
	startActivity(in10);		

}


}
