package com.supermarket.knowledgetab;

import com.supermarket.knowledgetab.ktab.KtabsMain;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
//	LinearLayout showinstallation,resetlinear,exitlinear;
	ImageView extra,ktab,hints;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	
		initialize();
		setOnclicklistner();
	}

	private void setOnclicklistner() {
		/*showinstallation.setOnClickListener(this);
		 resetlinear.setOnClickListener(this);
		 exitlinear.setOnClickListener(this);
		*/ extra.setOnClickListener(this);
		 ktab.setOnClickListener(this);
		 hints.setOnClickListener(this);
	}

	private void initialize() {
	/*	showinstallation = (LinearLayout)findViewById(R.id.showinstallation_linear);
		resetlinear  = (LinearLayout)findViewById(R.id.resetactivation_linear);
		exitlinear= (LinearLayout)findViewById(R.id.exit_linear);
	*/	extra = (ImageView)findViewById(R.id.extracurri_image);
		ktab = (ImageView)findViewById(R.id.ktab_image);
		hints = (ImageView)findViewById(R.id.hint_image);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settingsS1) {
			return true;
		}
		else if(id == R.id.action_settingsS2){return true;}
		else if(id == R.id.action_settingsS2){return true;}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		Toast t = Toast.makeText(getApplicationContext(),"asd",5000);
		
		switch (v.getId()) {
		/*case R.id.showinstallation_linear:
		t.setText("showinstallation");	
			break;
		case R.id.resetactivation_linear:
			t.setText("resetactivation_linear");	
			break;
		case R.id.exit_linear:
			t.setText("exit_linear");
			break;*/
		case R.id.extracurri_image:
			Intent in = new Intent(MainActivity.this,ExtraCurricularMain.class);
			startActivity(in);
			//t.setText("extracurri_image");
			break;
		case R.id.ktab_image:
			Intent in1 = new Intent(MainActivity.this,KtabsMain.class);
			startActivity(in1);
			t.setText("ktab_image");
			break;
		case R.id.hint_image:
			Intent in2 = new Intent(MainActivity.this,AfterExtraCurricular.class);
			in2.putExtra("type","hint");
			startActivity(in2);
			t.setText("hint_image");
			break;

		default:
			break;
		}
		t.setDuration(5000);
		t.show();
		
	}
}
