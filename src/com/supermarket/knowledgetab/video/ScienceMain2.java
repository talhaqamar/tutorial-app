package com.supermarket.knowledgetab.video;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.supermarket.knowledgetab.R;
import com.supermarket.knowlegdetab.mock.Menu1;
import com.supermarket.knowlegdetab.mock.ScienceMain1;

public class ScienceMain2 extends Activity implements OnClickListener {
	LinearLayout agriculture,biology,chemistry,library,economics,englishliterature,furthermath,geography,irk,mathematics,physics,yoruba;
	String type = "";
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.sciencemain2);

	
	
	initialize();
	setOnclicklistner();	
}
private void setOnclicklistner() {
	 biology.setOnClickListener(this);
	 agriculture.setOnClickListener(this);
	 chemistry.setOnClickListener(this);
	 library.setOnClickListener(this);
	 economics.setOnClickListener(this);
	 englishliterature.setOnClickListener(this);
	 furthermath.setOnClickListener(this);
	 geography.setOnClickListener(this);
	 irk.setOnClickListener(this);
	 mathematics.setOnClickListener(this);
	 physics.setOnClickListener(this);
	 yoruba.setOnClickListener(this);
	
	}
private void initialize() {
	 agriculture =(LinearLayout)findViewById(R.id.agriculture_linear);
	 biology  =(LinearLayout)findViewById(R.id.biology1_linear);
	 chemistry =(LinearLayout)findViewById(R.id.chemistry_linear);
	 library=(LinearLayout)findViewById(R.id.library1_linear);
	 economics=(LinearLayout)findViewById(R.id.economics1_linear);
	 englishliterature=(LinearLayout)findViewById(R.id.englishlanguage1_linear);
	 furthermath=(LinearLayout)findViewById(R.id.furthermath_linear);
	 geography=(LinearLayout)findViewById(R.id.geography1_linear);
	 irk=(LinearLayout)findViewById(R.id.irk1_linear);
	 mathematics=(LinearLayout)findViewById(R.id.mathematics1_linear);
	 physics=(LinearLayout)findViewById(R.id.physics1_linear);
	 yoruba=(LinearLayout)findViewById(R.id.yoruba1_linear);
	}
@Override
public void onClick(View v) {
	Intent in10 = new Intent(ScienceMain2.this,Menu2.class);
	switch (v.getId()) {
	case  R.id.agriculture_linear:
		
		in10.putExtra("subject", "agriculture");
		in10.putExtra("type", type);

		break;
	case R.id.biology1_linear:
		in10.putExtra("subject", "biology");
		in10.putExtra("type", type);
		break;
	case R.id.chemistry_linear:
		in10.putExtra("subject", "chemistry");
		in10.putExtra("type", type);
		break;
	case R.id.library1_linear:
		in10.putExtra("subject", "library");
		in10.putExtra("type", type);
		break;
		
	case R.id.economics1_linear:
		in10.putExtra("subject", "economics");
		in10.putExtra("type", type);
		break;
	case R.id.englishlanguage1_linear:
		in10.putExtra("subject", "englishlanguage");
		in10.putExtra("type", type);
		break;
		
	case R.id.furthermath_linear :
		in10.putExtra("subject", "furthermath");
		in10.putExtra("type", type);
		break;
	case R.id.geography1_linear:
		in10.putExtra("subject", "geography");
		in10.putExtra("type", type);
		break;
	case R.id.irk1_linear:
		in10.putExtra("subject", "irk");
		in10.putExtra("type", type);
		break;
	case R.id.mathematics1_linear:
		in10.putExtra("subject", "mathematics");
		in10.putExtra("type", type);
		
		break;
	case R.id.physics1_linear:
		in10.putExtra("subject", "physics");
		in10.putExtra("type", type);
		break;
	
case R.id.yoruba1_linear:
	in10.putExtra("subject", "yoruba");
	in10.putExtra("type", type);
	break;
	default:
		break;
	}
	startActivity(in10);		

}


}
