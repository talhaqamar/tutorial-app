package com.supermarket.knowledgetab.video;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.supermarket.knowledgetab.R;



public class ArtMain2 extends Activity implements OnClickListener {
	LinearLayout biology,commerce,ccrs,economics,geography,englishlanguage,government
	,history,irk,englishliterature,calculator,yoruba;
		String type = "";

@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.artmain2);

	Intent in = getIntent();
	type = in.getStringExtra("library");
	
	initialize();
	setOnclicklistner();	
	Toast.makeText(ArtMain2.this, "asd", 5000).show();
}
private void setOnclicklistner() {
	 biology.setOnClickListener(this);
	 commerce.setOnClickListener(this);
	 ccrs.setOnClickListener(this);
	 economics.setOnClickListener(this);
	 geography.setOnClickListener(this);
	 englishlanguage.setOnClickListener(this);
	 government.setOnClickListener(this);
	 history.setOnClickListener(this);
	 irk.setOnClickListener(this);
	 englishliterature.setOnClickListener(this);
	 calculator.setOnClickListener(this);
	 yoruba.setOnClickListener(this);
	
	}
private void initialize() {
	 biology=(LinearLayout)findViewById(R.id.biology_linear);
	 commerce=(LinearLayout)findViewById(R.id.commerce_linear);
	 ccrs=(LinearLayout)findViewById(R.id.crs_linear);
	 economics=(LinearLayout)findViewById(R.id.economics_linear);
	 geography=(LinearLayout)findViewById(R.id.geography_linear);
	 englishlanguage=(LinearLayout)findViewById(R.id.englishlanguage_linear);
	 government=(LinearLayout)findViewById(R.id.government_linear);
	 history=(LinearLayout)findViewById(R.id.history_linear);
	 irk=(LinearLayout)findViewById(R.id.irk_linear);
	 englishliterature=(LinearLayout)findViewById(R.id.englishliterature_linear);
	 calculator =(LinearLayout)findViewById(R.id.mathematics_linear);
	 yoruba =(LinearLayout)findViewById(R.id.yoruba_linear);
	}
@Override
public void onClick(View v) {
	switch (v.getId()) {

	case R.id.biology_linear:
		
		Intent in = new Intent(ArtMain2.this,Menu2.class);
		in.putExtra("subject", "biology");
		in.putExtra("type", type);
		
		startActivity(in);
		break;
	case R.id.commerce_linear:
		Intent in1 = new Intent(ArtMain2.this,Menu2.class);
		in1.putExtra("subject", "commerce");
		in1.putExtra("type", type);
			startActivity(in1);
		break;
	case R.id.crs_linear:
		Intent in2 = new Intent(ArtMain2.this,Menu2.class);
		in2.putExtra("subject", "crs");
		in2.putExtra("type", type);
		startActivity(in2);
		break;
	case R.id.economics_linear:
		Intent in3 = new Intent(ArtMain2.this,Menu2.class);
		in3.putExtra("subject", "economics");
		in3.putExtra("type", type);
		startActivity(in3);
			break;
	case R.id.geography_linear:
		Intent in4 = new Intent(ArtMain2.this,Menu2.class);
		in4.putExtra("subject", "geography");
		in4.putExtra("type", type);
		startActivity(in4);
		break;
	case R.id.englishlanguage_linear:
		Intent in5 = new Intent(ArtMain2.this,Menu2.class);
		in5.putExtra("subject", "englishlanguage");
		in5.putExtra("type", type);
		startActivity(in5);
		break;
	case R.id.government_linear:
		Intent in6 = new Intent(ArtMain2.this,Menu2.class);
		in6.putExtra("subject", "government");
		in6.putExtra("type", type);
		
		startActivity(in6);
						break;
	case R.id.history_linear:
		Intent in66 = new Intent(ArtMain2.this,Menu2.class);
		in66.putExtra("subject", "history");
		in66.putExtra("type", type);
		
		startActivity(in66);
				break;
	case R.id.irk_linear:
		Intent in7 = new Intent(ArtMain2.this,Menu2.class);
		in7.putExtra("subject", "irk");
		in7.putExtra("type", type);
		
		startActivity(in7);						
	break;
	case R.id.englishliterature_linear:
		Intent in8 = new Intent(ArtMain2.this,Menu2.class);
		in8.putExtra("subject", "englishliterature");
		in8.putExtra("type", type);
		
		startActivity(in8);
		break;
case R.id.mathematics_linear:
	Intent in9 = new Intent(ArtMain2.this,Menu2.class);
	in9.putExtra("subject", "mathematics");
	in9.putExtra("type", type);
	
	startActivity(in9);
break;
case R.id.yoruba_linear:
	Intent in10 = new Intent(ArtMain2.this,Menu2.class);
	in10.putExtra("subject", "yoruba");
	
	in10.putExtra("type", type);
	startActivity(in10);		
break;
	default:
		break;
	}
}


}
