package com.supermarket.knowledgetab;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SplashScreen extends Activity {
	protected boolean _active = true;
	protected int _splashTime = 3000;

	
	
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.splash);
	
	
		Thread splashTread = new Thread() {
			@Override
			public void run() {
				try {
					int waited = 0;
					while (_active && (waited < _splashTime)) {
						sleep(100);
						if (_active) {
							waited += 100;
						}
					}
				} catch (InterruptedException e) {
					e.toString();
				} finally {

					finish();
					Intent intent = new Intent(getApplicationContext(),
							MainActivity.class);
					startActivity(intent);
	//				overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
				}
			}
		};
		splashTread.start();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
	}
}