package com.supermarket.knowlegdetab.mock;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.supermarket.knowledgetab.R;
import com.supermarket.knowledgetab.ktab.ArtMain;
import com.supermarket.knowledgetab.ktab.ScienceMain;
import com.supermarket.knowledgetab.ktab.SocialScienceMain;
import com.supermarket.knowledgetab.ktab.Ss1;

public class JambMain extends Activity implements OnClickListener {
	LinearLayout art,science,social;
String check ="";
	@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.jambmain);

	
	initialize();
	setOnclicklistner();
}
private void setOnclicklistner() {
	art.setOnClickListener(this);
	science.setOnClickListener(this);
	social.setOnClickListener(this);
}
private void initialize() {
	art=(LinearLayout)findViewById(R.id.art_linear);
	science=(LinearLayout)findViewById(R.id.science_linear);
	social=(LinearLayout)findViewById(R.id.socialscience_linear);
	}
@Override
public void onClick(View v) {
	switch (v.getId()) {
	case R.id.art_linear:
		Intent in1 = new Intent(JambMain.this,ArtMain1.class);
		
		in1.putExtra("type", "jamb");
		
		startActivity(in1);
		break;
	case R.id.science_linear:
		Toast.makeText(JambMain.this, check.toString(), 5000).show();
		Intent in2 = new Intent(JambMain.this,ScienceMain1.class);
	
		in2.putExtra("type", "jamb");
		startActivity(in2);
		
		break;
	case R.id.socialscience_linear:
		Toast.makeText(JambMain.this, check.toString(), 5000).show();
		Intent in3 = new Intent(JambMain.this,SocialScienceMain1.class);
	
		in3.putExtra("type", "jamb");
		startActivity(in3);
		
		break;

	default:
		break;
	}
}


}
